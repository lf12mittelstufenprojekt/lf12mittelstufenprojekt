﻿Imports System.IO
Imports System.Net
Imports MySql.Data.MySqlClient

<DebuggerStepThrough>
Public Structure structDatenbank
    Dim Name As String
    Dim MySQLUser As String
    Dim MySQLPassword As String
    Dim MySQLServer As String
    Dim MySQLDatabase As String
End Structure

Public Class clsMySQL
    Implements IDisposable

    Private ReadOnly bPooling As Boolean = False

    Private ReadOnly _cnMySQL As MySqlConnection
    Private Shared _MySQLDatenbank As structDatenbank
    Private _DataReader As MySqlDataReader
    Private ReadOnly _Overwritedatabase As String = String.Empty

    Private Shared ReadOnly Property _Datenbank_Datei As String
        Get
            Return Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), Datenbank_Dateiname())
        End Get
    End Property

#Region "Default"

    Public Sub New(Server As String, Username As String, Password As String, Optional Database As String = "")
        _cnMySQL = New MySqlConnection(Get_Connection_String(Server, Username, Password, Database))
        Open()
    End Sub

    Public Sub New(Optional sDatabase As String = "")
        _Overwritedatabase = sDatabase

        init()
        _cnMySQL = New MySqlConnection(Get_Connection_String)
        Open()
    End Sub

    ' ReSharper disable once UnusedMember.Global
    Public Sub New(Pooling As Boolean)
        bPooling = Pooling
        init()
        _cnMySQL = New MySqlConnection(Get_Connection_String)
        Open()
    End Sub

    Protected Overrides Sub Finalize()
        Close()
        _cnMySQL.Dispose()
        MyBase.Finalize()
    End Sub

#End Region

#Region "Propertys"

    Private Shared ReadOnly Property Datenbank_Datei() As String
        Get
            Return _Datenbank_Datei
        End Get
    End Property

    Public ReadOnly Property DataReader As MySqlDataReader
        Get
            Return _DataReader
        End Get
    End Property

    Public Shared ReadOnly Property MySQL_Settings As structDatenbank
        Get
            Return _MySQLDatenbank
        End Get
    End Property
#End Region

#Region "Interne Funktionen"

    Private Sub Open()
        If _cnMySQL.State <> ConnectionState.Open Then
            Try
                _cnMySQL.Open()
                Console.WriteLine("MySQL Open")
                ExecuteWithParametersWithoutDatareader("SET SESSION wait_timeout = 30;")
            Catch ex As Exception
                Console.WriteLine(ex.ToString)
            End Try
        Else
            Try
                If Not _cnMySQL.Ping Then
                    Try
                        _cnMySQL.Close()
                        _cnMySQL.Open()
                        Console.WriteLine("MySQL Open")
                        ExecuteWithParametersWithoutDatareader("SET SESSION wait_timeout = 30;")
                    Catch ex As Exception
                        Console.WriteLine(ex.ToString)
                    End Try
                End If
            Catch
                Try
                    _cnMySQL.Close()
                    _cnMySQL.Open()
                    Console.WriteLine("MySQL Open")
                    ExecuteWithParametersWithoutDatareader("SET SESSION wait_timeout = 30;")
                Catch ex As Exception
                    Console.WriteLine(ex.ToString)
                End Try
            End Try
        End If
    End Sub

    Private Sub Close()
        Try
            _cnMySQL.Close()
            Console.WriteLine("MySQL Close")
        Catch ex As Exception
            Console.WriteLine(ex.ToString)
        End Try
    End Sub

    Private Sub Reset_DataReader()
        If Not IsNothing(_DataReader) Then
            Dim i As Integer = 0
            While Not _DataReader.IsClosed
                Try
                    _DataReader.Close()
                Catch ex As Exception
                    Close()
                    Open()
                    i += 1
                    Threading.Thread.Sleep(100)
                    Application.DoEvents()
                    If i >= 10 Then
                        Throw
                    End If
                End Try
            End While
        End If
    End Sub

    Private Function Get_Connection_String(Server As String, Username As String, Passwort As String, Optional Database As String = "") As String
        Dim ConnectionString As String
        ConnectionString = String.Format("Persist Security Info=False;server={0};user id={1};Password={2};Connection Timeout = 3000;ConvertZeroDateTime=true", Server, Username, Passwort)

        If Not bPooling Then
            ConnectionString &= ";Pooling=False"
        End If

        If Database.Trim.Length > 0 Then
            ConnectionString &= String.Format(";database={0}", Database)
        End If
        Return ConnectionString
    End Function

    Private Function Get_Connection_String() As String
        If _MySQLDatenbank.MySQLServer.StartsWith("89.0.") Then
            Dim Addresslist As IPAddress() = Dns.GetHostEntry(Dns.GetHostName()).AddressList
            Dim IPs As IPAddress
            ' alle IP-Adressen auflisten

            For Each IPs In Addresslist
                If IPs.ToString.StartsWith("172.31.") Then
                    _MySQLDatenbank.MySQLServer = "172.31.0.200"
                    Exit For
                End If
            Next IPs

        End If

        If _Overwritedatabase.Trim.Length > 0 Then
            Return Get_Connection_String(_MySQLDatenbank.MySQLServer, _MySQLDatenbank.MySQLUser, _MySQLDatenbank.MySQLPassword, _Overwritedatabase)
        Else
            Return Get_Connection_String(_MySQLDatenbank.MySQLServer, _MySQLDatenbank.MySQLUser, _MySQLDatenbank.MySQLPassword, _MySQLDatenbank.MySQLDatabase)
        End If
    End Function

    Private Sub init()
        Try
            Lade_Datenbankeinstellungen()
        Catch ex As Exception
            Dim formEinstellungen As frmEinstellungen
            formEinstellungen = New frmEinstellungen
            formEinstellungen.ShowDialog()
            Lade_Datenbankeinstellungen()
        End Try
    End Sub

    Private Sub Lade_Datenbankeinstellungen()
        Dim Stream As StreamReader
        Dim serializer As New Xml.Serialization.XmlSerializer(GetType(structDatenbank()))
        Stream = Crypt.decrypt(Datenbank_Datei)
        _MySQLDatenbank = CType(serializer.Deserialize(Stream), structDatenbank())(0)
    End Sub

    Private Sub ExecuteNonQuery(commandMySQL As MySqlCommand)
        Dim bFehler As Boolean
        Dim i As Integer
        Do
            bFehler = False
            Try
                commandMySQL.ExecuteNonQuery()
            Catch ex As Exception
                bFehler = True
                Close()
                Open()
                i += 1
                Threading.Thread.Sleep(100)
                Application.DoEvents()
                If i >= 10 Then
                    Throw
                End If
            End Try
        Loop While bFehler
    End Sub

    Private Function ExecuteReader(commandMySQL As MySqlCommand) As MySqlDataReader
        Dim oDataReader As MySqlDataReader = Nothing
        Dim bFehler As Boolean
        Dim i As Integer
        Do
            bFehler = False
            Try
                oDataReader = commandMySQL.ExecuteReader()
            Catch ex As Exception
                bFehler = True
                Close()
                Open()
                i += 1
                Threading.Thread.Sleep(100)
                Application.DoEvents()
                If i >= 10 Then
                    Throw
                End If
            End Try
        Loop While bFehler

        Return oDataReader
    End Function

#End Region

#Region "Öffentliche Funktionen"
    ' ReSharper disable once UnusedMember.Global 
    Public Shared Function MySQLDatumUhrzeit(dDate As Date) As String
        Return MySQLDatum(dDate) & " " & MySQLUhrzeit(dDate)
    End Function

    ' ReSharper disable once MemberCanBePrivate.Global
    Public Shared Function MySQLDatum(dDate As Date) As String
        Return Format(dDate, "yyyy-MM-dd")
    End Function

    ' ReSharper disable once MemberCanBePrivate.Global
    Public Shared Function MySQLUhrzeit(dDate As Date) As String
        Return Format(dDate, "HH:mm:ss")
    End Function

    ''' <summary>
    ''' Für den Selben Query mit unterschiedlichen Parametern aus 
    ''' </summary>
    ''' <param name="Query">SQL Query</param>
    ''' <param name="Parameter">Liste von Parametern, welche für den query relevant sind.</param>
    Public Sub ExecuteWithParametersWithoutDatareader(Query As String, Parameter As List(Of Dictionary(Of String, Object)))
        For Each dict As Dictionary(Of String, Object) In Parameter
            ExecuteWithParametersWithoutDatareader(Query, dict)
        Next
    End Sub

    ''' <summary>
    ''' Führe den Query aus.
    ''' </summary>
    ''' <param name="Query">SQL Query</param>
    ''' <param name="Parameter">Parameter, welche für den Query relevant sind</param>
    Public Sub ExecuteWithParametersWithoutDatareader(Query As String, Optional Parameter As Dictionary(Of String, Object) = Nothing)
        Reset_DataReader()
        Open()

        Dim commandMySQL As New MySqlCommand With {
            .Connection = _cnMySQL,
            .CommandText = Query,
            .CommandTimeout = 300
        }
        commandMySQL.Prepare()

        If Not IsNothing(Parameter) Then
            Dim pair As KeyValuePair(Of String, Object)
            For Each pair In Parameter
                commandMySQL.Parameters.AddWithValue(pair.Key, pair.Value)
            Next
        End If

        Try
            ExecuteNonQuery(commandMySQL)
        Catch ex As Exception
            Application.DoEvents()
            Throw
        End Try
    End Sub

    Public Sub ExecuteWithParametersAsDatareader(Query As String, Optional Parameter As Dictionary(Of String, Object) = Nothing)
        Reset_DataReader()
        Open()

        Dim commandMySQL As New MySqlCommand With {
            .Connection = _cnMySQL,
            .CommandText = Query,
            .CommandTimeout = 300
        }
        commandMySQL.Prepare()

        If Not IsNothing(Parameter) Then
            Dim pair As KeyValuePair(Of String, Object)
            For Each pair In Parameter
                commandMySQL.Parameters.AddWithValue(pair.Key, pair.Value)
            Next
        End If

        _DataReader = ExecuteReader(commandMySQL)
    End Sub

    ' ReSharper disable once UnusedMember.Global
    Public Sub ExecuteStorredProcedure(procedure As String, Optional ParameterIn As Dictionary(Of String, Object) = Nothing)
        Dim commandMySQL As New MySqlCommand
        Reset_DataReader()
        Open()
        commandMySQL.Connection = _cnMySQL

        commandMySQL.CommandText = procedure
        commandMySQL.CommandType = CommandType.StoredProcedure
        commandMySQL.CommandTimeout = 300

        If Not IsNothing(ParameterIn) Then
            Dim pair As KeyValuePair(Of String, Object)
            For Each pair In ParameterIn
                commandMySQL.Parameters.AddWithValue(pair.Key, pair.Value)
                commandMySQL.Parameters(pair.Key).Direction = ParameterDirection.Input
            Next
        End If

        ExecuteNonQuery(commandMySQL)
    End Sub


    ''' <summary>
    ''' Führt ein SQL Query aus
    ''' </summary>
    ''' <param name="query">SQL Query</param>
    ''' <param name="Parameter">Parameter, welche für den SQL Query relevant sind</param>
    ''' <returns>Liste der Datenbank IDs</returns>
    Public Function ExecuteInsertWithParameter(query As String, Parameter As List(Of Dictionary(Of String, Object))) As List(Of Integer)
        Dim rueckgabe As New List(Of Integer)
        For Each dict As Dictionary(Of String, Object) In Parameter
            rueckgabe.Add(ExecuteInsertWithParameter(query, dict))
        Next
        Return rueckgabe
    End Function

    ''' <summary>
    ''' Führt ein SQL Query aus
    ''' </summary>
    ''' <param name="query">SQL Query</param>
    ''' <param name="Parameter">Parameter, welche für den SQL Query relevant sind</param>
    ''' <returns>Die Datenbank ID</returns>
    Public Function ExecuteInsertWithParameter(query As String, Optional Parameter As Dictionary(Of String, Object) = Nothing) As Integer
        Dim commandMySQL As New MySqlCommand
        Open()
        commandMySQL.Connection = _cnMySQL
        commandMySQL.CommandText = query
        commandMySQL.CommandTimeout = 300
        commandMySQL.Prepare()

        If Not IsNothing(Parameter) Then
            Dim pair As KeyValuePair(Of String, Object)
            For Each pair In Parameter
                commandMySQL.Parameters.AddWithValue(pair.Key, pair.Value)
            Next
        End If

        ExecuteNonQuery(commandMySQL)
        Return commandMySQL.LastInsertedId
    End Function

    ' ReSharper disable once MemberCanBePrivate.Global
    Public Function DataAdapterWithParameters(query As String, Optional Parameter As Dictionary(Of String, Object) = Nothing) As MySqlDataAdapter
        Dim _DataAdapter As MySqlDataAdapter
        _DataAdapter = New MySqlDataAdapter(query, _cnMySQL)
        If Not IsNothing(Parameter) Then
            Dim pair As KeyValuePair(Of String, Object)
            For Each pair In Parameter
                _DataAdapter.SelectCommand.Parameters.AddWithValue(pair.Key, pair.Value)
            Next
        End If
        Return _DataAdapter
    End Function


    ''' <summary>
    ''' Fürt ein SQL Query aus und erzeugt ein Dataset 
    ''' </summary>
    ''' <param name="query">SQL Query</param>
    ''' <param name="Parameter">Parameter, die für den SQL Query Relevant sind</param>
    ''' <param name="Tabelle">Tabellenname des Datasets</param>
    ''' <returns>Dataset mit dem Ergebnis des SQL Querys</returns>
    Public Function FillDatasetWithQueryWithParameters(ByRef query As String, Optional Parameter As Dictionary(Of String, Object) = Nothing, Optional Tabelle As String = "Befehl") As DataSet
        Open()
        Dim myDataSet As New DataSet
        Dim myadapter As MySqlDataAdapter = DataAdapterWithParameters(query, Parameter)
        myadapter.SelectCommand.CommandTimeout = 600
        myadapter.Fill(myDataSet, Tabelle)
        Return myDataSet
    End Function

    Public Function FillDataTableWithQueryWithParameters(query As String, TableName As String, Optional Parameter As Dictionary(Of String, Object) = Nothing) As DataTable
        Open()
        Dim tempDA As MySqlDataAdapter = DataAdapterWithParameters(query, Parameter)
        Dim dt As New DataTable(TableName)
        tempDA.Fill(dt)
        Return dt
    End Function

    Public Shared Sub SchreibeTestVerbindung(ByVal sDatenbank As String, ByVal sServer As String, ByVal sUsername As String, ByVal sPasswort As String)
        Dim tempDatenbank As structDatenbank()
        Dim itempDatenbank As Integer = 0
        Dim formatter As Xml.Serialization.XmlSerializer
        Dim stream As MemoryStream
        Dim tempString As String

        ReDim Preserve tempDatenbank(itempDatenbank)
        tempDatenbank(itempDatenbank).Name = "ContainerPro"
        tempDatenbank(itempDatenbank).MySQLDatabase = sDatenbank
        tempDatenbank(itempDatenbank).MySQLServer = sServer
        tempDatenbank(itempDatenbank).MySQLUser = sUsername
        tempDatenbank(itempDatenbank).MySQLPassword = sPasswort
        itempDatenbank += 1

        ReDim Preserve tempDatenbank(itempDatenbank)
        tempDatenbank(itempDatenbank).Name = "ContainerProHamburg"
        tempDatenbank(itempDatenbank).MySQLDatabase = sDatenbank
        tempDatenbank(itempDatenbank).MySQLServer = sServer
        tempDatenbank(itempDatenbank).MySQLUser = sUsername
        tempDatenbank(itempDatenbank).MySQLPassword = sPasswort

        formatter = New Xml.Serialization.XmlSerializer(GetType(structDatenbank()))

        stream = New MemoryStream
        formatter.Serialize(stream, tempDatenbank)

        Dim r As StreamReader = New StreamReader(stream)
        r.BaseStream.Seek(0, SeekOrigin.Begin)
        tempString = r.ReadToEnd

        Crypt.crypt(Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), "Datenbank.dat"), tempString)
        stream.Close()
    End Sub

#Region "IDisposable Support"

    Private disposedValue As Boolean ' Dient zur Erkennung redundanter Aufrufe.

    ' IDisposable
    Private Sub Dispose(disposing As Boolean)
        If Not disposedValue _
            AndAlso disposing Then
            Close()
            _cnMySQL.Dispose()

            ' nicht verwaltete Ressourcen (nicht verwaltete Objekte) freigeben und Finalize() weiter unten überschreiben.
            ' große Felder auf Null setzen.
        End If
        disposedValue = True
    End Sub

    ' Finalize() nur überschreiben, wenn Dispose(disposing As Boolean) weiter oben Code zur Bereinigung nicht verwalteter Ressourcen enthält.
    'Protected Overrides Sub Finalize()
    '    ' Ändern Sie diesen Code nicht. Fügen Sie Bereinigungscode in Dispose(disposing As Boolean) weiter oben ein.
    '    Dispose(False)
    '    MyBase.Finalize()
    'End Sub

    ' Dieser Code wird von Visual Basic hinzugefügt, um das Dispose-Muster richtig zu implementieren.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Ändern Sie diesen Code nicht. Fügen Sie Bereinigungscode in Dispose(disposing As Boolean) weiter oben ein.
        Dispose(True)
        ' Auskommentierung der folgenden Zeile aufheben, wenn Finalize() oben überschrieben wird.
        ' GC.SuppressFinalize(Me)
    End Sub

#End Region

#End Region
End Class
