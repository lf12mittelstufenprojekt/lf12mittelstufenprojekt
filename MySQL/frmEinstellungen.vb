Imports System.IO
Public Class frmEinstellungen
    Dim bErlaubt As Boolean = False

    Private Sub btnTest_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnTest.Click
        Dim cnMySQL As MySql.Data.MySqlClient.MySqlConnection
        Dim commandMySQL As MySql.Data.MySqlClient.MySqlCommand
        Dim readMySQL As MySql.Data.MySqlClient.MySqlDataReader

        cboDatabase.Text = String.Empty
        cboDatabase.Items.Clear()

        Try
            cnMySQL = New MySql.Data.MySqlClient.MySqlConnection("Persist Security Info=False;server=" & txtServer.Text & ";user id=" & txtBenutzername.Text & ";Password=" & txtKennwort.Text & ";Connection Timeout = 300;")
            cnMySQL.Open()
            commandMySQL = New MySql.Data.MySqlClient.MySqlCommand("Show Databases;", cnMySQL)
            readMySQL = commandMySQL.ExecuteReader
            While readMySQL.Read
                If Not IsDBNull(readMySQL("Database")) Then
                    cboDatabase.Items.Add(readMySQL("Database"))
                End If
            End While
            readMySQL.Close()
            cnMySQL.Close()
        Catch ex As Exception
            MessageBox.Show("Verbindung fehlgeschlagen" & Environment.NewLine & ex.ToString, "Hinweis", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub frmEinstellungen_FormClosing(ByVal sender As Object, ByVal e As FormClosingEventArgs) Handles Me.FormClosing
        If Not bErlaubt Then
            Dim ownProcess As New Process

            For Each oProcess As Process In Process.GetProcessesByName("containerpro")
                ' Prozess-Infos ermitteln und im ListView anzeigen
                If oProcess.Id = Process.GetCurrentProcess.Id Then
                    ownProcess = oProcess
                Else
                    oProcess.Kill()
                End If

                ownProcess.Kill()
            Next
        End If
    End Sub

    Private Sub btnAbbruch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAbbruch.Click
        Close()
    End Sub

    Private Sub btnOK_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnOK.Click
        Dim tempDatenbank() As structDatenbank
        Dim itempDatenbank As Integer = 0
        Dim cnMySQL As MySql.Data.MySqlClient.MySqlConnection
        cboDatabase.Items.Clear()

        Dim formatter As Xml.Serialization.XmlSerializer
        Dim stream As MemoryStream

        Dim Titel As String = String.Empty
        Dim tempString As String
        'Dim dllCrypt As New Crypt

        Try
            Titel = "Standart"
            cnMySQL = New MySql.Data.MySqlClient.MySqlConnection("Persist Security Info=False;Database=" & cboDatabase.Text & ";server=" & txtServer.Text & ";user id=" & txtBenutzername.Text & ";Password=" & txtKennwort.Text & ";Connection Timeout = 300;Pooling=false;")
            cnMySQL.Open()
            cnMySQL.Close()

            Titel = "Siemens"
            cnMySQL = New MySql.Data.MySqlClient.MySqlConnection("Persist Security Info=False;Database=" & cboDatabase.Text & ";server=" & txtServer.Text & ";user id=" & txtBenutzername.Text & ";Password=" & txtKennwort.Text & ";Connection Timeout = 300;Pooling=false;")
            cnMySQL.Open()
            cnMySQL.Close()

            Titel = String.Empty

            ReDim Preserve tempDatenbank(itempDatenbank)
            tempDatenbank(itempDatenbank).Name = "ContainerPro"
            tempDatenbank(itempDatenbank).MySQLDatabase = cboDatabase.Text
            tempDatenbank(itempDatenbank).MySQLServer = txtServer.Text
            tempDatenbank(itempDatenbank).MySQLUser = txtBenutzername.Text
            tempDatenbank(itempDatenbank).MySQLPassword = txtKennwort.Text
            itempDatenbank += 1

            ReDim Preserve tempDatenbank(itempDatenbank)
            tempDatenbank(itempDatenbank).Name = "ContainerProBremen"
            tempDatenbank(itempDatenbank).MySQLDatabase = cboDatabase.Text
            tempDatenbank(itempDatenbank).MySQLServer = txtServer.Text
            tempDatenbank(itempDatenbank).MySQLUser = txtBenutzername.Text
            tempDatenbank(itempDatenbank).MySQLPassword = txtKennwort.Text

            formatter = New Xml.Serialization.XmlSerializer(GetType(structDatenbank()))

            stream = New MemoryStream
            formatter.Serialize(stream, tempDatenbank)

            Dim r As StreamReader = New StreamReader(stream)
            r.BaseStream.Seek(0, SeekOrigin.Begin)
            tempString = r.ReadToEnd

            Crypt.crypt(Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), Datenbank_Dateiname()), tempString)
            stream.Close()

            bErlaubt = True
            Close()
        Catch ex As Exception
            If Titel.Trim.Length > 0 Then
                MessageBox.Show("Verbindung des " & Titel & " Bereichs fehlgeschlagen", Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                MessageBox.Show("Verbindung fehlgeschlagen", Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        End Try
    End Sub

    Public Sub New()

        ' Dieser Aufruf ist f�r den Windows Form-Designer erforderlich.
        InitializeComponent()

        ' F�gen Sie Initialisierungen nach dem InitializeComponent()-Aufruf hinzu.

    End Sub
End Class