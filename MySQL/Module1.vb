Module mdlDatenbank
    Public Function Datenbank_Dateiname() As String
        Dim rueckgabe As String = "Datenbank.dat"

        For Each tempArgument As String In Environment.GetCommandLineArgs
            If tempArgument.ToLower.StartsWith("-standort=") Then
                If tempArgument.Split("=").Length >= 2 Then
                    rueckgabe = "Datenbank_" & tempArgument.Split("=")(1) & ".dat"
                    Exit For
                End If
            End If
        Next
        Return rueckgabe
    End Function
End Module