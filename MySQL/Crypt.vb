Imports System.Security.Cryptography
<DebuggerStepThrough>
Public Class Crypt

    Const sPasswort As String = "if3com56"

    Public Shared Sub crypt(ByVal Datei As String, ByVal daten As String)
        Dim DES As New DESCryptoServiceProvider()
        Dim fstream As FileStream
        Dim cstream As CryptoStream

        With DES
            ' Das Passwort muss als Bytearray gesetzt werden.
            .Key = Text.Encoding.ASCII.GetBytes(sPasswort)
            .IV = Text.Encoding.ASCII.GetBytes("12345678")
        End With
        ' Der Stream, der in die Datei schreibt.
        fstream = New FileStream(Datei, FileMode.Create)
        ' Der CryptoStream, der die Verschlüsselung
        ' übernimmt.

        cstream = New CryptoStream(fstream, DES.CreateEncryptor(), CryptoStreamMode.Write)
        ' Der CryptoStream bietet nur die Möglichkeit
        ' an Bytes zu schreiben. Deswegen machen wir uns
        ' das Ganze etwas leichter.
        Dim swriter As New StreamWriter(cstream)

        ' Text schreiben und Stream schließen
        swriter.Write(daten)
        swriter.Close()
    End Sub

    Public Shared Function decrypt(ByVal Datei As String) As StreamReader
        Dim DES As New DESCryptoServiceProvider()
        Dim fstream As FileStream
        Dim cstream As CryptoStream

        With DES
            ' Das Passwort muss als Bytearray gesetzt werden.
            .Key = Text.Encoding.ASCII.GetBytes(sPasswort)
            .IV = Text.Encoding.ASCII.GetBytes("12345678")
        End With

        ' -----------------------------------
        ' ------ Text wieder auslesen -------
        ' -----------------------------------
        fstream = New FileStream(Datei, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
        cstream = New CryptoStream(fstream,
          DES.CreateDecryptor,
          CryptoStreamMode.Read)

        Return New StreamReader(cstream)
    End Function
End Class