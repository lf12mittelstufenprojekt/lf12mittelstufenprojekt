﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AddPermissiongroup
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ButtonAbort = New System.Windows.Forms.Button()
        Me.ButtonSave = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TextBoxPermissiongroupname = New System.Windows.Forms.TextBox()
        Me.GroupBoxPermissions = New System.Windows.Forms.GroupBox()
        Me.CkBChangeAdminPW = New System.Windows.Forms.CheckBox()
        Me.CkBEditHolidayConfig = New System.Windows.Forms.CheckBox()
        Me.CkBCreateNewEmployee = New System.Windows.Forms.CheckBox()
        Me.CkBEditDocuments = New System.Windows.Forms.CheckBox()
        Me.CkBEditHolidays = New System.Windows.Forms.CheckBox()
        Me.CkBEditPermissiongroups = New System.Windows.Forms.CheckBox()
        Me.CkBEditFilter = New System.Windows.Forms.CheckBox()
        Me.CkBShowAllWorkspaces = New System.Windows.Forms.CheckBox()
        Me.CkBEditWorkspaces = New System.Windows.Forms.CheckBox()
        Me.CkBExportEmployeeData = New System.Windows.Forms.CheckBox()
        Me.CkBShowAllQualification = New System.Windows.Forms.CheckBox()
        Me.CkBEditQualifikation = New System.Windows.Forms.CheckBox()
        Me.CkBShowAllEmployee = New System.Windows.Forms.CheckBox()
        Me.CkBEditEmployee = New System.Windows.Forms.CheckBox()
        Me.GroupBoxPermissions.SuspendLayout()
        Me.SuspendLayout()
        '
        'ButtonAbort
        '
        Me.ButtonAbort.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonAbort.ForeColor = System.Drawing.Color.Blue
        Me.ButtonAbort.Location = New System.Drawing.Point(12, 412)
        Me.ButtonAbort.Name = "ButtonAbort"
        Me.ButtonAbort.Size = New System.Drawing.Size(95, 34)
        Me.ButtonAbort.TabIndex = 0
        Me.ButtonAbort.Text = "Abbrechen"
        Me.ButtonAbort.UseVisualStyleBackColor = True
        '
        'ButtonSave
        '
        Me.ButtonSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonSave.ForeColor = System.Drawing.Color.Blue
        Me.ButtonSave.Location = New System.Drawing.Point(178, 412)
        Me.ButtonSave.Name = "ButtonSave"
        Me.ButtonSave.Size = New System.Drawing.Size(94, 34)
        Me.ButtonSave.TabIndex = 1
        Me.ButtonSave.Text = "Hinzufügen"
        Me.ButtonSave.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(9, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(168, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Berechtigungsgruppenname:"
        '
        'TextBoxPermissiongroupname
        '
        Me.TextBoxPermissiongroupname.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.TextBoxPermissiongroupname.Location = New System.Drawing.Point(12, 35)
        Me.TextBoxPermissiongroupname.Name = "TextBoxPermissiongroupname"
        Me.TextBoxPermissiongroupname.Size = New System.Drawing.Size(260, 20)
        Me.TextBoxPermissiongroupname.TabIndex = 3
        '
        'GroupBoxPermissions
        '
        Me.GroupBoxPermissions.BackColor = System.Drawing.Color.LightSkyBlue
        Me.GroupBoxPermissions.Controls.Add(Me.CkBChangeAdminPW)
        Me.GroupBoxPermissions.Controls.Add(Me.CkBEditHolidayConfig)
        Me.GroupBoxPermissions.Controls.Add(Me.CkBCreateNewEmployee)
        Me.GroupBoxPermissions.Controls.Add(Me.CkBEditDocuments)
        Me.GroupBoxPermissions.Controls.Add(Me.CkBEditHolidays)
        Me.GroupBoxPermissions.Controls.Add(Me.CkBEditPermissiongroups)
        Me.GroupBoxPermissions.Controls.Add(Me.CkBEditFilter)
        Me.GroupBoxPermissions.Controls.Add(Me.CkBShowAllWorkspaces)
        Me.GroupBoxPermissions.Controls.Add(Me.CkBEditWorkspaces)
        Me.GroupBoxPermissions.Controls.Add(Me.CkBExportEmployeeData)
        Me.GroupBoxPermissions.Controls.Add(Me.CkBShowAllQualification)
        Me.GroupBoxPermissions.Controls.Add(Me.CkBEditQualifikation)
        Me.GroupBoxPermissions.Controls.Add(Me.CkBShowAllEmployee)
        Me.GroupBoxPermissions.Controls.Add(Me.CkBEditEmployee)
        Me.GroupBoxPermissions.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBoxPermissions.Location = New System.Drawing.Point(12, 61)
        Me.GroupBoxPermissions.Name = "GroupBoxPermissions"
        Me.GroupBoxPermissions.Size = New System.Drawing.Size(260, 345)
        Me.GroupBoxPermissions.TabIndex = 4
        Me.GroupBoxPermissions.TabStop = False
        Me.GroupBoxPermissions.Text = "Berechtigungen"
        '
        'CkBChangeAdminPW
        '
        Me.CkBChangeAdminPW.AutoSize = True
        Me.CkBChangeAdminPW.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CkBChangeAdminPW.Location = New System.Drawing.Point(6, 322)
        Me.CkBChangeAdminPW.Name = "CkBChangeAdminPW"
        Me.CkBChangeAdminPW.Size = New System.Drawing.Size(133, 17)
        Me.CkBChangeAdminPW.TabIndex = 25
        Me.CkBChangeAdminPW.Text = "Adminpasswort ändern"
        Me.CkBChangeAdminPW.UseVisualStyleBackColor = True
        '
        'CkBEditHolidayConfig
        '
        Me.CkBEditHolidayConfig.AutoSize = True
        Me.CkBEditHolidayConfig.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CkBEditHolidayConfig.Location = New System.Drawing.Point(6, 299)
        Me.CkBEditHolidayConfig.Name = "CkBEditHolidayConfig"
        Me.CkBEditHolidayConfig.Size = New System.Drawing.Size(172, 17)
        Me.CkBEditHolidayConfig.TabIndex = 24
        Me.CkBEditHolidayConfig.Text = "Urlaubskonfiguration verwalten"
        Me.CkBEditHolidayConfig.UseVisualStyleBackColor = True
        '
        'CkBCreateNewEmployee
        '
        Me.CkBCreateNewEmployee.AutoSize = True
        Me.CkBCreateNewEmployee.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CkBCreateNewEmployee.Location = New System.Drawing.Point(6, 275)
        Me.CkBCreateNewEmployee.Name = "CkBCreateNewEmployee"
        Me.CkBCreateNewEmployee.Size = New System.Drawing.Size(145, 17)
        Me.CkBCreateNewEmployee.TabIndex = 23
        Me.CkBCreateNewEmployee.Text = "Neue Mitarbeiter anlegen"
        Me.CkBCreateNewEmployee.UseVisualStyleBackColor = True
        '
        'CkBEditDocuments
        '
        Me.CkBEditDocuments.AutoSize = True
        Me.CkBEditDocuments.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CkBEditDocuments.Location = New System.Drawing.Point(6, 252)
        Me.CkBEditDocuments.Name = "CkBEditDocuments"
        Me.CkBEditDocuments.Size = New System.Drawing.Size(136, 17)
        Me.CkBEditDocuments.TabIndex = 22
        Me.CkBEditDocuments.Text = "Dokumente hinzufügen"
        Me.CkBEditDocuments.UseVisualStyleBackColor = True
        '
        'CkBEditHolidays
        '
        Me.CkBEditHolidays.AutoSize = True
        Me.CkBEditHolidays.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CkBEditHolidays.Location = New System.Drawing.Point(6, 229)
        Me.CkBEditHolidays.Name = "CkBEditHolidays"
        Me.CkBEditHolidays.Size = New System.Drawing.Size(174, 17)
        Me.CkBEditHolidays.TabIndex = 21
        Me.CkBEditHolidays.Text = "Urlaube Bearbeiten/hinzufügen"
        Me.CkBEditHolidays.UseVisualStyleBackColor = True
        '
        'CkBEditPermissiongroups
        '
        Me.CkBEditPermissiongroups.AutoSize = True
        Me.CkBEditPermissiongroups.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CkBEditPermissiongroups.Location = New System.Drawing.Point(6, 205)
        Me.CkBEditPermissiongroups.Name = "CkBEditPermissiongroups"
        Me.CkBEditPermissiongroups.Size = New System.Drawing.Size(151, 17)
        Me.CkBEditPermissiongroups.TabIndex = 20
        Me.CkBEditPermissiongroups.Text = "Berechtigungen Verwalten"
        Me.CkBEditPermissiongroups.UseVisualStyleBackColor = True
        '
        'CkBEditFilter
        '
        Me.CkBEditFilter.AutoSize = True
        Me.CkBEditFilter.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CkBEditFilter.Location = New System.Drawing.Point(6, 182)
        Me.CkBEditFilter.Name = "CkBEditFilter"
        Me.CkBEditFilter.Size = New System.Drawing.Size(103, 17)
        Me.CkBEditFilter.TabIndex = 19
        Me.CkBEditFilter.Text = "Filter verwaltung"
        Me.CkBEditFilter.UseVisualStyleBackColor = True
        '
        'CkBShowAllWorkspaces
        '
        Me.CkBShowAllWorkspaces.AutoSize = True
        Me.CkBShowAllWorkspaces.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CkBShowAllWorkspaces.Location = New System.Drawing.Point(6, 159)
        Me.CkBShowAllWorkspaces.Name = "CkBShowAllWorkspaces"
        Me.CkBShowAllWorkspaces.Size = New System.Drawing.Size(165, 17)
        Me.CkBShowAllWorkspaces.TabIndex = 18
        Me.CkBShowAllWorkspaces.Text = "Alle Arbeitsbereiche anzeigen"
        Me.CkBShowAllWorkspaces.UseVisualStyleBackColor = True
        '
        'CkBEditWorkspaces
        '
        Me.CkBEditWorkspaces.AutoSize = True
        Me.CkBEditWorkspaces.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CkBEditWorkspaces.Location = New System.Drawing.Point(6, 136)
        Me.CkBEditWorkspaces.Name = "CkBEditWorkspaces"
        Me.CkBEditWorkspaces.Size = New System.Drawing.Size(148, 17)
        Me.CkBEditWorkspaces.TabIndex = 17
        Me.CkBEditWorkspaces.Text = "Arbeitsbereiche verwalten"
        Me.CkBEditWorkspaces.UseVisualStyleBackColor = True
        '
        'CkBExportEmployeeData
        '
        Me.CkBExportEmployeeData.AutoSize = True
        Me.CkBExportEmployeeData.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CkBExportEmployeeData.Location = New System.Drawing.Point(6, 113)
        Me.CkBExportEmployeeData.Name = "CkBExportEmployeeData"
        Me.CkBExportEmployeeData.Size = New System.Drawing.Size(157, 17)
        Me.CkBExportEmployeeData.TabIndex = 16
        Me.CkBExportEmployeeData.Text = "Mitarbeiterdaten exportieren"
        Me.CkBExportEmployeeData.UseVisualStyleBackColor = True
        '
        'CkBShowAllQualification
        '
        Me.CkBShowAllQualification.AutoSize = True
        Me.CkBShowAllQualification.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CkBShowAllQualification.Location = New System.Drawing.Point(6, 90)
        Me.CkBShowAllQualification.Name = "CkBShowAllQualification"
        Me.CkBShowAllQualification.Size = New System.Drawing.Size(162, 17)
        Me.CkBShowAllQualification.TabIndex = 15
        Me.CkBShowAllQualification.Text = "Alle Qualifikationen anzeigen"
        Me.CkBShowAllQualification.UseVisualStyleBackColor = True
        '
        'CkBEditQualifikation
        '
        Me.CkBEditQualifikation.AutoSize = True
        Me.CkBEditQualifikation.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CkBEditQualifikation.Location = New System.Drawing.Point(6, 67)
        Me.CkBEditQualifikation.Name = "CkBEditQualifikation"
        Me.CkBEditQualifikation.Size = New System.Drawing.Size(145, 17)
        Me.CkBEditQualifikation.TabIndex = 14
        Me.CkBEditQualifikation.Text = "Qualifikationen verwalten"
        Me.CkBEditQualifikation.UseVisualStyleBackColor = True
        '
        'CkBShowAllEmployee
        '
        Me.CkBShowAllEmployee.AutoSize = True
        Me.CkBShowAllEmployee.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CkBShowAllEmployee.Location = New System.Drawing.Point(6, 44)
        Me.CkBShowAllEmployee.Name = "CkBShowAllEmployee"
        Me.CkBShowAllEmployee.Size = New System.Drawing.Size(141, 17)
        Me.CkBShowAllEmployee.TabIndex = 13
        Me.CkBShowAllEmployee.Text = "Alle Mitarbeiter anzeigen"
        Me.CkBShowAllEmployee.UseVisualStyleBackColor = True
        '
        'CkBEditEmployee
        '
        Me.CkBEditEmployee.AutoSize = True
        Me.CkBEditEmployee.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CkBEditEmployee.Location = New System.Drawing.Point(6, 21)
        Me.CkBEditEmployee.Name = "CkBEditEmployee"
        Me.CkBEditEmployee.Size = New System.Drawing.Size(124, 17)
        Me.CkBEditEmployee.TabIndex = 12
        Me.CkBEditEmployee.Text = "Mitarbeiter verwalten"
        Me.CkBEditEmployee.UseVisualStyleBackColor = True
        '
        'AddPermissiongroup
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(329, 458)
        Me.Controls.Add(Me.GroupBoxPermissions)
        Me.Controls.Add(Me.TextBoxPermissiongroupname)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ButtonSave)
        Me.Controls.Add(Me.ButtonAbort)
        Me.Name = "AddPermissiongroup"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Neue Berechtigungsgruppe"
        Me.GroupBoxPermissions.ResumeLayout(False)
        Me.GroupBoxPermissions.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ButtonAbort As Button
    Friend WithEvents ButtonSave As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents TextBoxPermissiongroupname As TextBox
    Friend WithEvents GroupBoxPermissions As GroupBox
    Friend WithEvents CkBEditFilter As CheckBox
    Friend WithEvents CkBShowAllWorkspaces As CheckBox
    Friend WithEvents CkBEditWorkspaces As CheckBox
    Friend WithEvents CkBExportEmployeeData As CheckBox
    Friend WithEvents CkBShowAllQualification As CheckBox
    Friend WithEvents CkBEditQualifikation As CheckBox
    Friend WithEvents CkBShowAllEmployee As CheckBox
    Friend WithEvents CkBEditEmployee As CheckBox
    Friend WithEvents CkBEditPermissiongroups As CheckBox
    Friend WithEvents CkBCreateNewEmployee As CheckBox
    Friend WithEvents CkBEditDocuments As CheckBox
    Friend WithEvents CkBEditHolidays As CheckBox
    Friend WithEvents CkBChangeAdminPW As CheckBox
    Friend WithEvents CkBEditHolidayConfig As CheckBox
End Class
