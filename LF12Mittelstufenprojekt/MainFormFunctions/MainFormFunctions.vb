﻿Module MainFormFunctions
    Private loginService As New LoginService()
    Private loginForm As New MainFormLogin(LoginService)
    Public adminLoggedIn As Boolean = False

    Public Sub Login()
        Dim thisWindowsUser As String = Environment.UserName
        Dim parameter As New Dictionary(Of String, Object)
        Dim showAdminLogin As Boolean = True
        Dim query As String = "SELECT WindowsBenutzer FROM mitarbeiter WHERE WindowsBenutzer = @WindowsBenutzer"
        parameter.Add("WindowsBenutzer", thisWindowsUser)

        Using mysql As New mysql_verbindung.clsMySQL
            mysql.ExecuteWithParametersAsDatareader(query, parameter)

            If mysql.DataReader.Read() Then showAdminLogin = False
        End Using

        If showAdminLogin Then loginForm.ShowDialog()
    End Sub

    Public Sub LoadOrReloadMainForm()
        Dim newCheck As New CheckPermissions
        'If Not logInService.IsLoggedIn() Then
        '    Login()
        'End If

        If adminLoggedIn Then
            MainForm.loggedUser = "Admin"
            ' Admin Berechtigung mit ID 1 laden
            MainForm.Perm = newCheck.CheckMyPermission(1)
        Else
            MainForm.loggedUser = Environment.UserName
            MainForm.Perm = newCheck.WhatsMyPermissiongroup(MainForm.loggedUser)
        End If


            LadeArbeitsbereiche()

        MainForm.lblUsername.Text = "User: " + Environment.UserName

        MainForm.DGVMitarbeiter.DataSource = LadeMitarbeiter()
        MainForm.DGVMitarbeiter.ClearSelection()

        If MainForm.Perm.ShowAllEmployee = "Y" Then
            MainForm.DGVMitarbeiter.Columns("ID").Visible = False
            ' IDs von Berechtigungen und Arbeitsbereich nicht anzeigen, nur die Bezeichnungen
            MainForm.DGVMitarbeiter.Columns("ArbeitsbereichID").Visible = False
            MainForm.DGVMitarbeiter.Columns("BerechtigungsGruppeID").Visible = False
        End If

        QualifikationenFilter()
    End Sub

    Private Sub LadeArbeitsbereiche()
        If MainForm.Perm.ShowAllWorkspaces <> "Y" Then
            Exit Sub
        End If
        MainForm.cbArbeitsbereich.Items.Clear()
        Dim query As String = "Select Bezeichnung from arbeitsbereich"
        MainForm.cbArbeitsbereich.Items.Add("")
        Using mysql As New mysql_verbindung.clsMySQL()
            mysql.ExecuteWithParametersAsDatareader(query)
            While mysql.DataReader.Read
                MainForm.cbArbeitsbereich.Items.Add(mysql.DataReader("Bezeichnung"))
            End While
        End Using

    End Sub

    Public Function LadeMitarbeiter() As List(Of Mitarbeiter)
        Dim query As String = "SELECT m.*, a.Bezeichnung AS ArbeitsbereichsBezeichnung, b.Bezeichnung AS BerechtigungsBezeichnung FROM mitarbeiter m
                               LEFT JOIN arbeitsbereich a
                               ON m.ArbeitsbereichID = a.ID
                               LEFT JOIN berechtigungsgruppen b
                               ON m.BerechtigungsGruppe = b.ID"

        Dim mitarbeiterListe As New List(Of Mitarbeiter)

        Using mysql As New mysql_verbindung.clsMySQL
            mysql.ExecuteWithParametersAsDatareader(query)

            While mysql.DataReader.Read()
                Dim mitarbeiter As New Mitarbeiter() With {
                        .ID = mysql.DataReader("ID"),
                        .Vorname = mysql.DataReader("Vorname"),
                        .Nachname = mysql.DataReader("Nachname"),
                        .Adresse = mysql.DataReader("Adresse"),
                        .PLZ = mysql.DataReader("PLZ"),
                        .Telefonnummer = mysql.DataReader("Telefonnummer"),
                        .Geburtsdatum = mysql.DataReader("Geburtsdatum"),
                        .SteuerID = mysql.DataReader("SteuerID"),
                        .IBAN = mysql.DataReader("IBAN"),
                        .ArbeitsEinstieg = mysql.DataReader("ArbeitsEinstieg"),
                        .ArbeitsEnde = mysql.DataReader("ArbeitsEnde"),
                        .WindowsBenutzer = mysql.DataReader("WindowsBenutzer"),
                        .BerechtigungsGruppeID = mysql.DataReader("BerechtigungsGruppe"),
                        .ArbeitsbereichID = mysql.DataReader("ArbeitsbereichID"),
                        .Personalnummer = mysql.DataReader("Personalnummer"),
                        .Ort = mysql.DataReader("Ort"),
                        .ArbeitsBereich = mysql.DataReader("ArbeitsbereichsBezeichnung"),
                        .BerechtigungsGruppe = mysql.DataReader("BerechtigungsBezeichnung")
                    }

                mitarbeiterListe.Add(mitarbeiter)

            End While
        End Using

        ' Wenn keine Berechtigung zum Anzeigen alle MA nur Daten vom angemeldeten MA anzeigen
        If MainForm.Perm.ShowAllEmployee <> "Y" Then
            ' Angemeldeten MA anhand Windowsbenutzer aus Mitarbeitern suchen
            Dim angemeldet As Mitarbeiter = mitarbeiterListe.FirstOrDefault(Function(f) f.WindowsBenutzer = Environment.UserName)

            If angemeldet Is Nothing Then
                ' Sollte nicht vorkommen, nur falls was schiefläuft!
                Dim nichtVorhanden As New DataGridViewTextBoxColumn With {
                  .HeaderText = "Info"
                }
                MainForm.DGVMitarbeiter.Rows.Clear()
                MainForm.DGVMitarbeiter.Columns.Add(nichtVorhanden)
                MainForm.DGVMitarbeiter.Rows.Add("Keine Berechtigungen zum anzeigen der Mitarbeiter")
                MainForm.DGVMitarbeiter.AutoSizeColumnsMode = DataGridViewAutoSizeColumnMode.Fill
                MainForm.DGVMitarbeiter.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                MainForm.DGVMitarbeiter.DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 16)
                MainForm.DGVMitarbeiter.DefaultCellStyle.ForeColor = Color.Red
                MainForm.DGVMitarbeiter.Rows(0).Height = 50
                MainForm.DGVMitarbeiter.ClearSelection()
                Exit Function

            Else
                ' Nur angemldten MA anzeigen!
                mitarbeiterListe.Clear()
                mitarbeiterListe.Add(angemeldet)
            End If
        End If

        If Not MainForm.DGVMitarbeiter.Columns.Contains("ColumnExport") Then


            Dim checkBoxColumn As New DataGridViewCheckBoxColumn() With {
                .HeaderText = "Exportieren",
                .Name = "ColumnExport",
                .Width = 100
            }
            MainForm.DGVMitarbeiter.Columns.Insert(0, checkBoxColumn)
        End If

        Return mitarbeiterListe
    End Function


    Private Sub QualifikationenFilter()
        Dim qualifikationen As List(Of Qualifikation) = LadeAlleQualifikationen()
        MainForm.DGVQualifikationenFilter.Columns.Clear()
        MainForm.DGVQualifikationenFilter.DataSource = qualifikationen
        MainForm.DGVQualifikationenFilter.ClearSelection()

        Dim vorhanden As New DataGridViewCheckBoxColumn With {
            .HeaderText = "Vorhanden",
            .Width = 100
        }

        Dim nichtVorhanden As New DataGridViewCheckBoxColumn With {
            .HeaderText = "Nicht Vorhanden",
            .Width = 100
        }

        MainForm.DGVQualifikationenFilter.Columns.Add(vorhanden)
        MainForm.DGVQualifikationenFilter.Columns.Add(nichtVorhanden)
    End Sub

    Private Function LadeAlleQualifikationen() As List(Of Qualifikation)
        Dim query As String = "Select * from qualifikationen"
        Dim qualifikationen As New List(Of Qualifikation)

        Using mysql As New mysql_verbindung.clsMySQL
            mysql.ExecuteWithParametersAsDatareader(query)

            While mysql.DataReader.Read()
                Dim qualifikation As New Qualifikation() With {
                    .ID = mysql.DataReader("ID"),
                    .Bezeichnung = mysql.DataReader("Bezeichnung")
                }

                qualifikationen.Add(qualifikation)
            End While
        End Using

        Return qualifikationen
    End Function

    Private Function getMitarbeiterQulifikationen() As Dictionary(Of Integer, List(Of Integer))
        Dim qualifikationenListe As New List(Of MitarbeiterQualifikation)
        Dim query As String = "Select * from  mitarbeiterqualifikationen"

        Using mysql As New mysql_verbindung.clsMySQL
            mysql.ExecuteWithParametersAsDatareader(query)

            While mysql.DataReader.Read()
                Dim qualifikation As New MitarbeiterQualifikation() With {
                    .ID = mysql.DataReader("ID"),
                    .MitarbeiterID = mysql.DataReader("MitarbeiterID"),
                    .QualifikationID = mysql.DataReader("QualifikationenID")
                }

                qualifikationenListe.Add(qualifikation)

            End While
        End Using

        Dim d As New Dictionary(Of Integer, List(Of Integer))
        For Each q As MitarbeiterQualifikation In qualifikationenListe
            If d.ContainsKey(q.MitarbeiterID) Then
                d(q.MitarbeiterID).Add(q.QualifikationID)
            Else
                d(q.MitarbeiterID) = New List(Of Integer) From {q.QualifikationID}
            End If
        Next

        Return d
    End Function

    Public Sub SetVisibility(mypanel As Panel)
        If mypanel.Visible = False Then
            mypanel.Visible = True
        Else
            mypanel.Visible = False
        End If
        If MainForm.PanelEmployee.Visible = True And mypanel IsNot MainForm.PanelEmployee Then
            MainForm.PanelEmployee.Visible = False
        End If
        If MainForm.PanelQualification.Visible = True And mypanel IsNot MainForm.PanelQualification Then
            MainForm.PanelQualification.Visible = False
        End If
        If MainForm.PanelWorkspaces.Visible = True And mypanel IsNot MainForm.PanelWorkspaces Then
            MainForm.PanelWorkspaces.Visible = False
        End If
        If MainForm.PanelPermissions.Visible = True And mypanel IsNot MainForm.PanelPermissions Then
            MainForm.PanelPermissions.Visible = False
        End If
        If MainForm.PanelOptions.Visible = True And mypanel IsNot MainForm.PanelOptions Then
            MainForm.PanelOptions.Visible = False
        End If
    End Sub

    Public Function CheckPermissionInMainForm(Berechtigungsgruppe As String, unequal As Boolean, resultOfCheck As String) As Boolean
        If unequal = True Then
            If Berechtigungsgruppe <> resultOfCheck Then
                MessageBox.Show("Sie besitzen nicht die benötigten Berechtigungen für diese Aktion.", "Info zu Berechtigungen", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return True
            End If
        Else
            If Berechtigungsgruppe = resultOfCheck Then
                MessageBox.Show("Sie besitzen nicht die benötigten Berechtigungen für diese Aktion.", "Info zu Berechtigungen", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return True
            End If
        End If
        Return Nothing
    End Function
End Module
