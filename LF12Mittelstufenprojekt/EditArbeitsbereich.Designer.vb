﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EditArbeitsbereich
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnAbbrechen = New System.Windows.Forms.Button()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.dgvArbeitsbereich = New System.Windows.Forms.DataGridView()
        Me.ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Bezeichnung = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bearbeiten = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.löschen = New System.Windows.Forms.DataGridViewButtonColumn()
        CType(Me.dgvArbeitsbereich, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnAbbrechen
        '
        Me.btnAbbrechen.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAbbrechen.ForeColor = System.Drawing.Color.Blue
        Me.btnAbbrechen.Location = New System.Drawing.Point(463, 325)
        Me.btnAbbrechen.Name = "btnAbbrechen"
        Me.btnAbbrechen.Size = New System.Drawing.Size(95, 41)
        Me.btnAbbrechen.TabIndex = 5
        Me.btnAbbrechen.Text = "Abbrechen"
        Me.btnAbbrechen.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.Blue
        Me.btnAdd.Location = New System.Drawing.Point(463, 12)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(95, 103)
        Me.btnAdd.TabIndex = 4
        Me.btnAdd.Text = "Hinzufügen"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'dgvArbeitsbereich
        '
        Me.dgvArbeitsbereich.AllowUserToAddRows = False
        Me.dgvArbeitsbereich.AllowUserToDeleteRows = False
        Me.dgvArbeitsbereich.AllowUserToResizeRows = False
        Me.dgvArbeitsbereich.BackgroundColor = System.Drawing.Color.LightSkyBlue
        Me.dgvArbeitsbereich.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvArbeitsbereich.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ID, Me.Bezeichnung, Me.bearbeiten, Me.löschen})
        Me.dgvArbeitsbereich.Location = New System.Drawing.Point(12, 12)
        Me.dgvArbeitsbereich.Name = "dgvArbeitsbereich"
        Me.dgvArbeitsbereich.ShowCellErrors = False
        Me.dgvArbeitsbereich.ShowCellToolTips = False
        Me.dgvArbeitsbereich.ShowEditingIcon = False
        Me.dgvArbeitsbereich.ShowRowErrors = False
        Me.dgvArbeitsbereich.Size = New System.Drawing.Size(445, 354)
        Me.dgvArbeitsbereich.TabIndex = 3
        '
        'ID
        '
        Me.ID.HeaderText = "ID"
        Me.ID.Name = "ID"
        Me.ID.Visible = False
        '
        'Bezeichnung
        '
        Me.Bezeichnung.HeaderText = "Bezeichnung"
        Me.Bezeichnung.Name = "Bezeichnung"
        Me.Bezeichnung.Width = 200
        '
        'bearbeiten
        '
        Me.bearbeiten.HeaderText = "bearbeiten"
        Me.bearbeiten.Name = "bearbeiten"
        '
        'löschen
        '
        Me.löschen.HeaderText = "löschen"
        Me.löschen.Name = "löschen"
        '
        'EditArbeitsbereich
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(567, 377)
        Me.Controls.Add(Me.btnAbbrechen)
        Me.Controls.Add(Me.btnAdd)
        Me.Controls.Add(Me.dgvArbeitsbereich)
        Me.Name = "EditArbeitsbereich"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Arbeitsbereiche bearbeiten"
        CType(Me.dgvArbeitsbereich, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents btnAbbrechen As Button
    Friend WithEvents btnAdd As Button
    Friend WithEvents dgvArbeitsbereich As DataGridView
    Friend WithEvents ID As DataGridViewTextBoxColumn
    Friend WithEvents Bezeichnung As DataGridViewTextBoxColumn
    Friend WithEvents bearbeiten As DataGridViewButtonColumn
    Friend WithEvents löschen As DataGridViewButtonColumn
End Class
