﻿Imports System.ComponentModel

Public Class EditPermissiongroup
    Dim wasChanged As Boolean = False
    Public Property [Result] As DialogResult
    Public mode As Integer = 0
    Public selectedRow As Integer
    Public tempArray() As String
    Public myIndex As Integer
    Private Sub AddPermissiongroup_Load() Handles MyBase.Load
        DGVPermissiongroup.Rows.Clear()
        Dim i As Integer = 0
        Dim firstRun As Boolean = True

        ' Liest alle Rechte der einzenen Berechtigungsgruppe aus
        For Each b As BerechtigungsGruppe In BerechtigungsGruppe.ReadPermissions

            DGVPermissiongroup.Rows.Add()

            With DGVPermissiongroup.Rows(i)
                .Cells(0).Value = b.ID
                .Cells(1).Value = b.Bezeichnung

                If b.EditEmployee = "Y" Then
                    .Cells(2).Value = "J"
                Else
                    .Cells(2).Value = "N"
                End If

                If b.ShowAllEmployee = "Y" Then
                    .Cells(3).Value = "J"
                Else
                    .Cells(3).Value = "N"
                End If

                If b.EditQualifications = "Y" Then
                    .Cells(4).Value = "J"
                Else
                    .Cells(4).Value = "N"
                End If

                If b.ShowAllQualification = "Y" Then
                    .Cells(5).Value = "J"
                Else
                    .Cells(5).Value = "N"
                End If

                If b.ExportEmployeeData = "Y" Then
                    .Cells(6).Value = "J"
                Else
                    .Cells(6).Value = "N"
                End If

                If b.EditWorkspaces = "Y" Then
                    .Cells(7).Value = "J"
                Else
                    .Cells(7).Value = "N"
                End If

                If b.ShowAllWorkspaces = "Y" Then
                    .Cells(8).Value = "J"
                Else
                    .Cells(8).Value = "N"
                End If

                If b.EditFilter = "Y" Then
                    .Cells(9).Value = "J"
                Else
                    .Cells(9).Value = "N"
                End If

                If b.EditPermissiongroup = "Y" Then
                    .Cells(10).Value = "J"
                Else
                    .Cells(10).Value = "N"
                End If

                If b.EditHolidays = "Y" Then
                    .Cells(11).Value = "J"
                Else
                    .Cells(11).Value = "N"
                End If

                If b.EditDocuments = "Y" Then
                    .Cells(12).Value = "J"
                Else
                    .Cells(12).Value = "N"
                End If

                If b.CreateEmployee = "Y" Then
                    .Cells(13).Value = "J"
                Else
                    .Cells(13).Value = "N"
                End If

                If b.EditHolidayConfig = "Y" Then
                    .Cells(14).Value = "J"
                Else
                    .Cells(14).Value = "N"
                End If

                If b.ChangeAdminPW = "Y" Then
                    .Cells(15).Value = "J"
                Else
                    .Cells(15).Value = "N"
                End If
            End With
            i += 1
        Next

        ' Verbirgt die Spalte "ID"
        DGVPermissiongroup.Columns(0).Visible = False

        'If ignoreTimer = False Then
        Timer1.Start()
        'End If
    End Sub

    'Private Function LoadPermissiongroups() As List(Of BerechtigungsGruppe)
    '    Dim query As String = "Select * from  berechtigungsgruppen"
    '    Dim berechtigungsListe As New List(Of BerechtigungsGruppe)

    '    Using mysql As New mysql_verbindung.clsMySQL
    '        mysql.ExecuteWithParametersAsDatareader(query)

    '        While mysql.DataReader.Read()
    '            Dim berechtigungsgruppe As New BerechtigungsGruppe() With {
    '                .ID = mysql.DataReader("ID"),
    '                .Bezeichnung = mysql.DataReader("Bezeichnung"),
    '                .EditEmployee = mysql.DataReader("MitarbeiterVerwalten"),
    '                .EditFilter = mysql.DataReader("FilterVerwalten"),
    '                .EditQualifications = mysql.DataReader("QualifikationenVerwalten"),
    '                .EditWorkspaces = mysql.DataReader("ArbeitsbereicheVerwalten"),
    '                .ExportEmployeeData = mysql.DataReader("MitarbeiterdatenExportieren"),
    '                .ShowAllWorkspaces = mysql.DataReader("AlleArbeitsbereicheAnzeigen"),
    '                .ShowAllEmployee = mysql.DataReader("AlleMitarbeiterAnzeigen"),
    '                .ShowAllQualification = mysql.DataReader("AlleQualifikationenAnzeigen"),
    '                .EditPermissiongroup = mysql.DataReader("BerechtigungenVerwalten"),
    '                .EditHolidays = mysql.DataReader("UrlaubeVerwalten"),
    '                .EditDocuments = mysql.DataReader("DokumenteVerwalten"),
    '                .CreateEmployee = mysql.DataReader("MitarbeiterErstellen")
    '            }
    '            berechtigungsListe.Add(berechtigungsgruppe)
    '        End While
    '    End Using

    '    Return berechtigungsListe
    'End Function

    Private Sub ButtonAbort_Click(sender As Object, e As EventArgs) Handles ButtonAbort.Click
        Close()
    End Sub

    Private Sub ButtonAddPermissiongroup_Click(sender As Object, e As EventArgs) Handles ButtonAddPermissiongroup.Click
        ' Startet die Editorrmaske der Berechtigungen
        ReDim tempArray(DGVPermissiongroup.Rows.Count - 1)
        For i = 0 To DGVPermissiongroup.Rows.Count - 1
            tempArray(i) = DGVPermissiongroup.Item(1, i).Value
        Next i
        Dim addPerm As New AddPermissiongroup
        addPerm.dgvCount = DGVPermissiongroup.Rows.Count
        addPerm.CkBEditPermissiongroups.Enabled = False
        addPerm.CkBChangeAdminPW.Enabled = False
        addPerm.tempArray = tempArray
        result = addPerm.ShowDialog()
        If result = DialogResult.OK Then
            AddPermissiongroup_Load()
            wasChanged = True
        End If
    End Sub

    Private Sub DGVPermissiongroup_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGVPermissiongroup.CellDoubleClick
        LoadPermissionEditor(e.RowIndex)
    End Sub

    Public Sub LoadPermissionEditor(myRowIndex As Integer, Optional ignoreTimer As Boolean = False) 'e As DataGridViewCellEventArgs,
        If myRowIndex = -1 Then Exit Sub
        ' Fängt den bearbeitungsmodus ab, wenn es sich um den Admin handelt
        If DGVPermissiongroup.Item(1, myRowIndex).Value = "Admin" Then
            MessageBox.Show("Die Berechtigungsgruppe 'Admin' wurde vom System vordefiniert, sie kann nicht bearbeitet werden.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        ' Setzt die Rechte in dem Datagridview, die aus der DB kommen
        If myRowIndex <> -1 Then
            Dim EditPerm As New AddPermissiongroup

            If DGVPermissiongroup.Item(2, myRowIndex).Value = "J" Then
                EditPerm.CkBEditEmployee.CheckState = CheckState.Checked
            End If
            If DGVPermissiongroup.Item(3, myRowIndex).Value = "J" Then
                EditPerm.CkBShowAllEmployee.CheckState = CheckState.Checked
            End If
            If DGVPermissiongroup.Item(4, myRowIndex).Value = "J" Then
                EditPerm.CkBEditQualifikation.CheckState = CheckState.Checked
            End If
            If DGVPermissiongroup.Item(5, myRowIndex).Value = "J" Then
                EditPerm.CkBShowAllQualification.CheckState = CheckState.Checked
            End If
            If DGVPermissiongroup.Item(6, myRowIndex).Value = "J" Then
                EditPerm.CkBExportEmployeeData.CheckState = CheckState.Checked
            End If
            If DGVPermissiongroup.Item(7, myRowIndex).Value = "J" Then
                EditPerm.CkBEditWorkspaces.CheckState = CheckState.Checked
            End If
            If DGVPermissiongroup.Item(8, myRowIndex).Value = "J" Then
                EditPerm.CkBShowAllWorkspaces.CheckState = CheckState.Checked
            End If
            If DGVPermissiongroup.Item(9, myRowIndex).Value = "J" Then
                EditPerm.CkBEditFilter.CheckState = CheckState.Checked
            End If
            If DGVPermissiongroup.Item(10, myRowIndex).Value = "J" Then
                EditPerm.CkBEditPermissiongroups.CheckState = CheckState.Checked
            End If
            If DGVPermissiongroup.Item(11, myRowIndex).Value = "J" Then
                EditPerm.CkBEditHolidays.CheckState = CheckState.Checked
            End If
            If DGVPermissiongroup.Item(12, myRowIndex).Value = "J" Then
                EditPerm.CkBEditDocuments.CheckState = CheckState.Checked
            End If
            If DGVPermissiongroup.Item(13, myRowIndex).Value = "J" Then
                EditPerm.CkBCreateNewEmployee.CheckState = CheckState.Checked
            End If
            If DGVPermissiongroup.Item(14, myRowIndex).Value = "J" Then
                EditPerm.CkBEditHolidayConfig.CheckState = CheckState.Checked
            End If
            If DGVPermissiongroup.Item(15, myRowIndex).Value = "J" Then
                EditPerm.CkBChangeAdminPW.CheckState = CheckState.Checked
            End If

            EditPerm.Text = "Berichtigung bearbeiten"
            EditPerm.TextBoxPermissiongroupname.Text = DGVPermissiongroup.Item(1, myRowIndex).Value
            EditPerm.mode = 1
            EditPerm.mySelectedRow = DGVPermissiongroup.Rows(myRowIndex).Cells(0).Value
            EditPerm.CkBEditPermissiongroups.Enabled = False
            EditPerm.CkBChangeAdminPW.Enabled = False
            EditPerm.ButtonSave.Text = "Ändern"
            If DGVPermissiongroup.CurrentRow.Cells(1).Value = "User" Then
                EditPerm.TextBoxPermissiongroupname.Enabled = False
            End If
            EditPerm.ShowDialog()
            If ignoreTimer = False Then
                AddPermissiongroup_Load()

            End If
        End If
    End Sub

    Private Sub EditPermissiongroup_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        ' Wenn etwas geändert wurde, dann werden alle Berechtigungen neu geladen
        If wasChanged = True Then
            Dim newCheck As New CheckPermissions
            MainForm.Perm = newCheck.WhatsMyPermissiongroup(MainForm.loggedUser)
            'MainForm.LoadOrReload()
            MessageBox.Show("Da die Berechtigungen angepasst wurden, muss das Programm neugestartet werden.", "Berechtigungen", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Application.Restart()
        End If

    End Sub

    Private Sub Button1DeletePermissiongroup_Click(sender As Object, e As EventArgs) Handles ButtonDeletePermissiongroup.Click
        Dim mitarbeiterListe As New List(Of Mitarbeiter)
        Dim parameter As New Dictionary(Of String, Object)
        Dim mitarbeiter As New Mitarbeiter()
        Dim query As String
        Dim result As String
        Dim PermissiongroupInUse As String = ""

        ' Wenn die selection das DGV ungleich leer ist dann weiter
        If DGVPermissiongroup.CurrentCell.Selected <> False Then
            query = "Select Vorname, Nachname from  mitarbeiter Where BerechtigungsGruppe = @BerechtigungsGruppe"
            parameter.Add("BerechtigungsGruppe", DGVPermissiongroup.CurrentRow.Cells(0).Value)

            ' Mitarbeiter auslesen die die ausgewählte Berechtigungsgruppe angehören
            Using mysql As New mysql_verbindung.clsMySQL
                mysql.ExecuteWithParametersAsDatareader(query, parameter)

                While mysql.DataReader.Read()
                    mitarbeiter = New Mitarbeiter() With {
                        .Vorname = mysql.DataReader("Vorname"),
                        .Nachname = mysql.DataReader("Nachname")
                    }
                    mitarbeiterListe.Add(mitarbeiter)
                End While
            End Using
        End If

        ' Wenn Berechtigungsgruppe ist gleich Admin dann abbrechen
        If DGVPermissiongroup.CurrentRow.Cells(1).Value = "Admin" Then
            MessageBox.Show("Die Berechtigungsgruppe 'Admin' wurde vom System vordefiniert, sie kann nicht gelöscht werden.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        ' Wenn Berechtigungsgruppe ist gleich User dann abbrechen
        If DGVPermissiongroup.CurrentRow.Cells(1).Value = "User" Then
            MessageBox.Show("Die Berechtigungsgruppe 'User' wurde vom System vordefiniert, sie kann bearbeitet, aber nicht gelöscht werden.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        ' Wenn keine zugewiesenden Mitarbeiter gefunden wurde dann löschen
        If mitarbeiterListe.Count = 0 Then

            ' Abfrage vor dem Löschnvorgang
            result = InputBox("Bitte folgenden Bestätigungscode eingeben: " + vbCrLf + DGVPermissiongroup.CurrentRow.Cells(1).Value, "Berechtigung " + DGVPermissiongroup.CurrentRow.Cells(1).Value + " löschen")

            If result = DGVPermissiongroup.CurrentRow.Cells(1).Value Then
                query = "DELETE FROM berechtigungsgruppen Where ID = @ID"
                parameter.Add("ID", DGVPermissiongroup.CurrentRow.Cells(0).Value)
                Using mysql As New mysql_verbindung.clsMySQL
                    mysql.ExecuteWithParametersWithoutDatareader(query, parameter)
                End Using
                'MessageBox.Show("Berechtigungsgruppe '" + DGVPermissiongroup.CurrentRow.Cells(1).Value + "' wurde gelöscht", "Berechtigungsgruppe '" + DGVPermissiongroup.CurrentRow.Cells(1).Value + "' löschen", MessageBoxButtons.OK, MessageBoxIcon.Information)
                mode = 0
                AddPermissiongroup_Load()
                wasChanged = True

            ElseIf result <> DGVPermissiongroup.CurrentRow.Cells(1).Value And result <> "" Then ' Wenn bestätigungscode falschen dann Message ausgeben
                MessageBox.Show("Berechtigungsgruppe wurde nicht gelöscht, da der bestätigungscode nicht korrekt war.", "Berechtigung löschen", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If

        ElseIf mitarbeiterListe.Count <> 0 Then
            For Each mitarbeiter In mitarbeiterListe ' Sind noch mitarbeiter der gruppe zugewiesen werden sie angezeigt und der Löschvorgang wird beendet
                If mitarbeiterListe.Count = 1 Then
                    PermissiongroupInUse += mitarbeiter.Nachname + ", " + mitarbeiter.Vorname
                ElseIf mitarbeiterListe.Count > 1 Then
                    PermissiongroupInUse += mitarbeiter.Nachname + ", " + mitarbeiter.Vorname + vbCrLf
                End If
            Next
            MessageBox.Show("Die Berechtigungsgruppe kann nicht gelöscht werden, da sie noch Mitarbeitern zugewiesen ist. Die betroffenen Personen bitte eine andere Berechtigungsgruppe zuweisen. " + vbCrLf + vbCrLf + "Personen die dieser Gruppe noch zugewiesen sind: " + vbCrLf + PermissiongroupInUse, "Berechtigungsgruppe löschen", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If

    End Sub

    Private Sub DGVPermissiongroup_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGVPermissiongroup.CellClick
        selectedRow = e.RowIndex
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        ReDim tempArray(DGVPermissiongroup.Rows.Count - 1)
        For i = 0 To DGVPermissiongroup.Rows.Count - 1
            tempArray(i) = DGVPermissiongroup.Item(1, i).Value
        Next i
        Timer1.Stop()

        If mode = 1 Then
            AddPermissiongroup.dgvCount = DGVPermissiongroup.Rows.Count
            AddPermissiongroup.tempArray = tempArray
            ChoosePermissiongroupForEdit.dgvCount = DGVPermissiongroup.Rows.Count
            ChoosePermissiongroupForEdit.tempArray = tempArray
            ChoosePermissiongroupForEdit.ShowDialog()
            If Result = DialogResult.OK Then
                mode = 0
                AddPermissiongroup_Load()
                wasChanged = True
            Else
                Close()
            End If
        ElseIf mode = 2 Then

            ' Startet die Editormaske der Berechtigungen
            Dim addPerm As New AddPermissiongroup
            addPerm.tempArray = tempArray
            addPerm.CkBEditPermissiongroups.Enabled = False
            addPerm.CkBChangeAdminPW.Enabled = False
            addPerm.dgvCount = DGVPermissiongroup.Rows.Count
            addPerm.ShowDialog()

            If result = DialogResult.OK Then
                AddPermissiongroup_Load()
                wasChanged = True
            Else
                Close()
            End If
        End If
    End Sub
End Class