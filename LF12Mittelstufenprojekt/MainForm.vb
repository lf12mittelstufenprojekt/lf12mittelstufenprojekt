﻿Public Class MainForm

    Private mitarbeiter As List(Of Mitarbeiter)
    Private loginService As New LoginService()
    Private changePasswordForm As New PassWortChange(loginService)
    Private datenExportService As New DatenExportService()
    Public Perm As BerechtigungsGruppe
    Public loggedUser As String
    Dim DGVcurrentRow As DataGridViewCellEventArgs
    Public editPerm As New EditPermissiongroup
    Private mitarbeiterFilterService As New MitarbeiterFilterService()

    Private Sub MainForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ' Wenn windowsuser bekannt dann Programm start sonst Adminlogin
        Login()

        ' Mainform laden
        LoadOrReloadMainForm()

        ' Selektierung des Datagrid aufheben
        DGVMitarbeiter.ClearSelection()

        ' Timer für die Uhrzeit starten
        MyTimer.Start()

        ' Datum im unteren Label anzeigen lassen
        lblDateData.Text = Date.Now.ToLongDateString
    End Sub

    Private Sub DGVQualifikationenFilter_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGVQualifikationenFilter.CellContentClick
        mitarbeiter = LadeMitarbeiter()
        Dim arbeitsBereich As String = cbArbeitsbereich.Text
        mitarbeiter = mitarbeiterFilterService.Filter(arbeitsBereich, mitarbeiter, DGVQualifikationenFilter)
        DGVMitarbeiter.DataSource = mitarbeiter
        DGVMitarbeiter.ClearSelection()
    End Sub

    Private Sub DGVMitarbeiter_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGVMitarbeiter.CellDoubleClick
        Editemployee(e)
    End Sub

    Private Sub DGVMitarbeiter_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGVMitarbeiter.CellClick
        If CheckPermissionInMainForm(Perm.EditEmployee, True, "Y") Then Exit Sub

        'Klick auf Überschriftzeile abfangen
        If e.RowIndex = -1 Then
            Return
        End If

        If e.ColumnIndex = 0 Then
            Dim row As DataGridViewRow = DGVMitarbeiter.Rows(e.RowIndex)
            Dim checkBox As DataGridViewCell = row.Cells.Item(0)

            If checkBox.Value Then
                checkBox.Value = False
            Else
                checkBox.Value = True
            End If
        End If
        DGVcurrentRow = e
    End Sub
    Private Sub Editemployee(e As DataGridViewCellEventArgs)
        'Klick auf Überschriftzeile abfangen
        If e.RowIndex = -1 OrElse e.RowIndex >= DGVMitarbeiter.Rows.Count Then
            'Absturz verhindern wenn z.B. auf Mitarbeiter geklickt wurde, gefiltert wurde und dann auf bearbeiten geklickt wurde
            Return
        End If

        Dim newEmployee As New AddEmployee With {
            .id = DGVMitarbeiter.Rows(e.RowIndex).Cells("ID").Value,
            .mode = 1 ' 1=Mitarbeiter bearbeiten
        }
        newEmployee.Text = "Vorhandene Mitarbeiterdaten bearbeiten"
        newEmployee.ShowDialog()
        LoadOrReloadMainForm()
    End Sub

    Private Sub cbArbeitsbereich_Enter(sender As Object, e As EventArgs) Handles cbArbeitsbereich.Enter
        If CheckPermissionInMainForm(Perm.ShowAllWorkspaces, True, "Y") Then Exit Sub
    End Sub

    Private Sub ButtonEmployee_Click(sender As Object, e As EventArgs) Handles ButtonEmployee.Click
        SetVisibility(PanelEmployee)
    End Sub

    Private Sub ButtonQualification_Click(sender As Object, e As EventArgs) Handles ButtonQualification.Click
        SetVisibility(PanelQualification)
    End Sub

    Private Sub ButtonWorkspaces_Click(sender As Object, e As EventArgs) Handles ButtonWorkspaces.Click
        SetVisibility(PanelWorkspaces)
    End Sub

    Private Sub ButtonPermissions_Click(sender As Object, e As EventArgs) Handles ButtonPermissions.Click
        SetVisibility(PanelPermissions)
    End Sub

    Private Sub ButtonOptions_Click(sender As Object, e As EventArgs) Handles ButtonOptions.Click
        SetVisibility(PanelOptions)
    End Sub

    Private Sub ButtonExit_Click(sender As Object, e As EventArgs) Handles ButtonExit.Click
        Close()
    End Sub

    Private Sub ButtonAddEmployee_Click(sender As Object, e As EventArgs) Handles ButtonAddEmployee.Click
        If CheckPermissionInMainForm(Perm.CreateEmployee, True, "Y") Then Exit Sub
        Dim newEmployee As New AddEmployee With {
            .mode = 2, ' 2= Mitarbeiter hinzufügen
            .Text = "Neuen Mitarbeiter anlegen"
        }
        newEmployee.BtnMaDelete.Enabled = False
        newEmployee.ShowDialog()
        LoadOrReloadMainForm()
    End Sub

    Private Sub ButtonEditEmployee_Click(sender As Object, e As EventArgs) Handles ButtonEditEmployee.Click
        If DGVcurrentRow IsNot Nothing Then
            Editemployee(DGVcurrentRow)
        Else
            MessageBox.Show("Bitte einen mitarbeiter auswählen", "Mitarbeiter bearbeiten", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub ButtonAddQualification_Click(sender As Object, e As EventArgs) Handles ButtonAddQualification.Click
        If CheckPermissionInMainForm(Perm.EditQualifications, True, "Y") Then Exit Sub
        Dim EditQuali As New EditQualifikation
        EditQuali.ShowDialog()
        Me.MainForm_Load(sender, e)
    End Sub

    Private Sub ButtonAddWorkspace_Click(sender As Object, e As EventArgs) Handles ButtonAddWorkspace.Click
        If CheckPermissionInMainForm(Perm.EditWorkspaces, True, "Y") Then Exit Sub
        Dim EditArbeitsbereich As New EditArbeitsbereich
        EditArbeitsbereich.ShowDialog()
        Me.MainForm_Load(sender, e)
    End Sub

    Private Sub ButtonShowPermissions_Click(sender As Object, e As EventArgs) Handles ButtonShowPermissions.Click
        If CheckPermissionInMainForm(Perm.EditPermissiongroup, True, "Y") Then Exit Sub
        Dim editPerm As New EditPermissiongroup
        editPerm.mode = 0 ' Berechtigungen anzeigen
        editPerm.ShowDialog()
    End Sub

    Private Sub ButtonAddPermission_Click(sender As Object, e As EventArgs) Handles ButtonAddPermission.Click
        If CheckPermissionInMainForm(Perm.EditPermissiongroup, True, "Y") Then Exit Sub
        editPerm.mode = 2 ' Berechtigungen hinzufügen
        editPerm.ShowDialog()
    End Sub

    Private Sub ButtonEditPermissions_Click(sender As Object, e As EventArgs) Handles ButtonEditPermissions.Click
        If CheckPermissionInMainForm(Perm.EditPermissiongroup, True, "Y") Then Exit Sub
        editPerm.mode = 1 ' Berechtigungen bearbeiten
        editPerm.ShowDialog()
    End Sub

    Private Sub BtnResetFilter_Click(sender As Object, e As EventArgs) Handles BtnResetFilter.Click
        mitarbeiterFilterService.ResetFilter(cbArbeitsbereich, DGVQualifikationenFilter)
        LoadOrReloadMainForm()
    End Sub

    Private Sub cbArbeitsbereich_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbArbeitsbereich.SelectedIndexChanged
        mitarbeiter = LadeMitarbeiter()
        Dim arbeitsBereich As String = cbArbeitsbereich.Text
        mitarbeiter = mitarbeiterFilterService.Filter(arbeitsBereich, mitarbeiter, DGVQualifikationenFilter)
        DGVMitarbeiter.DataSource = mitarbeiter
        DGVMitarbeiter.ClearSelection()
    End Sub

    Private Sub ButtonExportEmployees_Click(sender As Object, e As EventArgs) Handles ButtonExportEmployees.Click
        If CheckPermissionInMainForm(Perm.ExportEmployeeData, True, "Y") Then Exit Sub

        Dim exportIds As New List(Of Integer)

        For Each row As DataGridViewRow In DGVMitarbeiter.Rows

            Dim ausgewaehlt As Boolean = row.Cells.Item(0).Value

            ' Wenn ausgewählt Mitarbeiterid holen
            If ausgewaehlt Then
                Dim id As Integer = row.Cells.Item(1).Value
                exportIds.Add(id)
            End If
        Next

        ' Wenn keine Mitarbeiter ausgewählt exportieren abbrechen
        If exportIds.Count = 0 Then
            MessageBox.Show("Bitte Mitarbeiter zum Exportieren auswählen.", "Exportieren", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return
        End If

        Dim mitarbeiter As List(Of Mitarbeiter) = LadeMitarbeiter()
        Dim exportList As New List(Of Mitarbeiter)
        ' Von allen Mitarbeitern bleiben nur ausgewählte in Liste
        For Each m As Mitarbeiter In mitarbeiter
            If exportIds.Contains(m.ID) Then
                exportList.Add(m)
            End If
        Next
        datenExportService.ExportMitarbeiterDaten(exportList)
    End Sub

    Private Sub ButtonConfigHolidays_Click(sender As Object, e As EventArgs) Handles ButtonConfigHolidays.Click
        If CheckPermissionInMainForm(Perm.EditHolidayConfig, True, "Y") Then Exit Sub
        Dim urlaubConfig As New UrlaubConfig()
        ' öffnet die Urlaubskonfuguration
        urlaubConfig.ShowDialog()
    End Sub

    Private Sub ButtonChangeAdminPW_Click(sender As Object, e As EventArgs) Handles ButtonChangeAdminPW.Click
        If CheckPermissionInMainForm(Perm.ChangeAdminPW, True, "Y") Then Exit Sub
        ' Öffnet ein Dialog um das <Adminpasswort zu ändern
        changePasswordForm.ShowDialog()
    End Sub

    Private Sub MyTimer_Tick(sender As Object, e As EventArgs) Handles MyTimer.Tick
        lblSystemTime.Text = Date.Now.ToLongTimeString
    End Sub
End Class
