﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PassWortChange
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblCapsLock = New System.Windows.Forms.Label()
        Me.ChkShowPW = New System.Windows.Forms.CheckBox()
        Me.BtnCancel = New System.Windows.Forms.Button()
        Me.BtnÄndern = New System.Windows.Forms.Button()
        Me.lblNeuPasswort = New System.Windows.Forms.Label()
        Me.lblPasswortWiederholen = New System.Windows.Forms.Label()
        Me.TxtWiederholung = New System.Windows.Forms.TextBox()
        Me.TxtNeuPasswort = New System.Windows.Forms.TextBox()
        Me.TxtAltesPasswort = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'lblCapsLock
        '
        Me.lblCapsLock.AutoSize = True
        Me.lblCapsLock.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCapsLock.ForeColor = System.Drawing.Color.Red
        Me.lblCapsLock.Location = New System.Drawing.Point(36, 192)
        Me.lblCapsLock.Name = "lblCapsLock"
        Me.lblCapsLock.Size = New System.Drawing.Size(173, 15)
        Me.lblCapsLock.TabIndex = 15
        Me.lblCapsLock.Text = "Feststelltaste ist aktiviert !"
        Me.lblCapsLock.Visible = False
        '
        'ChkShowPW
        '
        Me.ChkShowPW.AutoSize = True
        Me.ChkShowPW.Location = New System.Drawing.Point(39, 162)
        Me.ChkShowPW.Name = "ChkShowPW"
        Me.ChkShowPW.Size = New System.Drawing.Size(115, 17)
        Me.ChkShowPW.TabIndex = 10
        Me.ChkShowPW.Text = "Passwort anzeigen"
        Me.ChkShowPW.UseVisualStyleBackColor = True
        '
        'BtnCancel
        '
        Me.BtnCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnCancel.ForeColor = System.Drawing.Color.Blue
        Me.BtnCancel.Location = New System.Drawing.Point(145, 219)
        Me.BtnCancel.Name = "BtnCancel"
        Me.BtnCancel.Size = New System.Drawing.Size(95, 30)
        Me.BtnCancel.TabIndex = 14
        Me.BtnCancel.Text = "Abbrechen"
        Me.BtnCancel.UseVisualStyleBackColor = True
        '
        'BtnÄndern
        '
        Me.BtnÄndern.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnÄndern.ForeColor = System.Drawing.Color.Blue
        Me.BtnÄndern.Location = New System.Drawing.Point(12, 219)
        Me.BtnÄndern.Name = "BtnÄndern"
        Me.BtnÄndern.Size = New System.Drawing.Size(95, 30)
        Me.BtnÄndern.TabIndex = 12
        Me.BtnÄndern.Text = "Ändern"
        Me.BtnÄndern.UseVisualStyleBackColor = True
        '
        'lblNeuPasswort
        '
        Me.lblNeuPasswort.AutoSize = True
        Me.lblNeuPasswort.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNeuPasswort.Location = New System.Drawing.Point(36, 67)
        Me.lblNeuPasswort.Name = "lblNeuPasswort"
        Me.lblNeuPasswort.Size = New System.Drawing.Size(102, 13)
        Me.lblNeuPasswort.TabIndex = 13
        Me.lblNeuPasswort.Text = "Neues Passwort:"
        '
        'lblPasswortWiederholen
        '
        Me.lblPasswortWiederholen.AutoSize = True
        Me.lblPasswortWiederholen.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPasswortWiederholen.Location = New System.Drawing.Point(36, 119)
        Me.lblPasswortWiederholen.Name = "lblPasswortWiederholen"
        Me.lblPasswortWiederholen.Size = New System.Drawing.Size(137, 13)
        Me.lblPasswortWiederholen.TabIndex = 11
        Me.lblPasswortWiederholen.Text = "Passwort Wiederholen:"
        '
        'TxtWiederholung
        '
        Me.TxtWiederholung.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.TxtWiederholung.Location = New System.Drawing.Point(39, 135)
        Me.TxtWiederholung.Name = "TxtWiederholung"
        Me.TxtWiederholung.Size = New System.Drawing.Size(134, 20)
        Me.TxtWiederholung.TabIndex = 9
        Me.TxtWiederholung.UseSystemPasswordChar = True
        '
        'TxtNeuPasswort
        '
        Me.TxtNeuPasswort.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.TxtNeuPasswort.Location = New System.Drawing.Point(39, 83)
        Me.TxtNeuPasswort.Name = "TxtNeuPasswort"
        Me.TxtNeuPasswort.Size = New System.Drawing.Size(134, 20)
        Me.TxtNeuPasswort.TabIndex = 8
        Me.TxtNeuPasswort.UseSystemPasswordChar = True
        '
        'TxtAltesPasswort
        '
        Me.TxtAltesPasswort.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.TxtAltesPasswort.Location = New System.Drawing.Point(39, 29)
        Me.TxtAltesPasswort.Name = "TxtAltesPasswort"
        Me.TxtAltesPasswort.Size = New System.Drawing.Size(134, 20)
        Me.TxtAltesPasswort.TabIndex = 16
        Me.TxtAltesPasswort.UseSystemPasswordChar = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(36, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(94, 13)
        Me.Label1.TabIndex = 17
        Me.Label1.Text = "Altes Passwort:"
        '
        'PassWortChange
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(252, 261)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TxtAltesPasswort)
        Me.Controls.Add(Me.lblCapsLock)
        Me.Controls.Add(Me.ChkShowPW)
        Me.Controls.Add(Me.BtnCancel)
        Me.Controls.Add(Me.BtnÄndern)
        Me.Controls.Add(Me.lblNeuPasswort)
        Me.Controls.Add(Me.lblPasswortWiederholen)
        Me.Controls.Add(Me.TxtWiederholung)
        Me.Controls.Add(Me.TxtNeuPasswort)
        Me.Name = "PassWortChange"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Passwort Ändern"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lblCapsLock As Label
    Friend WithEvents ChkShowPW As CheckBox
    Friend WithEvents BtnCancel As Button
    Friend WithEvents BtnÄndern As Button
    Friend WithEvents lblNeuPasswort As Label
    Friend WithEvents lblPasswortWiederholen As Label
    Friend WithEvents TxtWiederholung As TextBox
    Friend WithEvents TxtNeuPasswort As TextBox
    Friend WithEvents TxtAltesPasswort As TextBox
    Friend WithEvents Label1 As Label
End Class
