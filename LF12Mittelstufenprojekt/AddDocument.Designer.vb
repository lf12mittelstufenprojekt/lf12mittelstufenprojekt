﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AddDocument
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtBeschreibung = New System.Windows.Forms.TextBox()
        Me.lblDragAndDrop = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnDateiAuswählen = New System.Windows.Forms.Button()
        Me.btnHochladen = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtBeschreibung)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(297, 55)
        Me.GroupBox1.TabIndex = 29
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Beschreibung"
        '
        'txtBeschreibung
        '
        Me.txtBeschreibung.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.txtBeschreibung.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBeschreibung.Location = New System.Drawing.Point(6, 19)
        Me.txtBeschreibung.MaxLength = 100
        Me.txtBeschreibung.Name = "txtBeschreibung"
        Me.txtBeschreibung.Size = New System.Drawing.Size(285, 20)
        Me.txtBeschreibung.TabIndex = 0
        '
        'lblDragAndDrop
        '
        Me.lblDragAndDrop.AllowDrop = True
        Me.lblDragAndDrop.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.lblDragAndDrop.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDragAndDrop.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblDragAndDrop.Location = New System.Drawing.Point(12, 151)
        Me.lblDragAndDrop.Name = "lblDragAndDrop"
        Me.lblDragAndDrop.Size = New System.Drawing.Size(298, 138)
        Me.lblDragAndDrop.TabIndex = 28
        Me.lblDragAndDrop.Text = "Hier hinziehen"
        Me.lblDragAndDrop.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.DarkGreen
        Me.Label2.Location = New System.Drawing.Point(12, 120)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(295, 31)
        Me.Label2.TabIndex = 27
        Me.Label2.Text = "- oder -"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnDateiAuswählen
        '
        Me.btnDateiAuswählen.BackColor = System.Drawing.SystemColors.Control
        Me.btnDateiAuswählen.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDateiAuswählen.ForeColor = System.Drawing.Color.Blue
        Me.btnDateiAuswählen.Location = New System.Drawing.Point(12, 73)
        Me.btnDateiAuswählen.Name = "btnDateiAuswählen"
        Me.btnDateiAuswählen.Size = New System.Drawing.Size(298, 36)
        Me.btnDateiAuswählen.TabIndex = 26
        Me.btnDateiAuswählen.Text = "Datei auswählen"
        Me.btnDateiAuswählen.UseVisualStyleBackColor = False
        '
        'btnHochladen
        '
        Me.btnHochladen.BackColor = System.Drawing.SystemColors.Control
        Me.btnHochladen.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHochladen.ForeColor = System.Drawing.Color.Blue
        Me.btnHochladen.Location = New System.Drawing.Point(12, 319)
        Me.btnHochladen.Name = "btnHochladen"
        Me.btnHochladen.Size = New System.Drawing.Size(298, 56)
        Me.btnHochladen.TabIndex = 35
        Me.btnHochladen.Text = "Hochladen"
        Me.btnHochladen.UseVisualStyleBackColor = False
        '
        'AddDocument
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(326, 389)
        Me.Controls.Add(Me.btnHochladen)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.lblDragAndDrop)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btnDateiAuswählen)
        Me.Name = "AddDocument"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Neues Dokument hochladen"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents txtBeschreibung As TextBox
    Friend WithEvents lblDragAndDrop As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents btnDateiAuswählen As Button
    Friend WithEvents btnHochladen As Button
End Class
