﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class UrlaubConfig
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.BtnSpeichern = New System.Windows.Forms.Button()
        Me.ComboboxTage = New System.Windows.Forms.CheckedListBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.DGVFeiertage = New System.Windows.Forms.DataGridView()
        Me.Bezeichnung = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Datum = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.DGVFeiertage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BtnSpeichern
        '
        Me.BtnSpeichern.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnSpeichern.ForeColor = System.Drawing.Color.Blue
        Me.BtnSpeichern.Location = New System.Drawing.Point(12, 198)
        Me.BtnSpeichern.Name = "BtnSpeichern"
        Me.BtnSpeichern.Size = New System.Drawing.Size(95, 30)
        Me.BtnSpeichern.TabIndex = 1
        Me.BtnSpeichern.Text = "Speichern"
        Me.BtnSpeichern.UseVisualStyleBackColor = True
        '
        'ComboboxTage
        '
        Me.ComboboxTage.BackColor = System.Drawing.Color.LightSkyBlue
        Me.ComboboxTage.CheckOnClick = True
        Me.ComboboxTage.ForeColor = System.Drawing.Color.Black
        Me.ComboboxTage.FormattingEnabled = True
        Me.ComboboxTage.Items.AddRange(New Object() {"Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag", "Sonntag"})
        Me.ComboboxTage.Location = New System.Drawing.Point(420, 28)
        Me.ComboboxTage.Name = "ComboboxTage"
        Me.ComboboxTage.Size = New System.Drawing.Size(120, 109)
        Me.ComboboxTage.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(417, 12)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(191, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Nicht als Urlaub gezählte Tage: "
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(13, 11)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(337, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Feiertage im aktuellen Jahr (zählen nicht als Urlaubstage):" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'DGVFeiertage
        '
        Me.DGVFeiertage.BackgroundColor = System.Drawing.Color.LightSkyBlue
        Me.DGVFeiertage.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVFeiertage.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Bezeichnung, Me.Datum, Me.ID})
        Me.DGVFeiertage.Location = New System.Drawing.Point(16, 28)
        Me.DGVFeiertage.MultiSelect = False
        Me.DGVFeiertage.Name = "DGVFeiertage"
        Me.DGVFeiertage.Size = New System.Drawing.Size(398, 164)
        Me.DGVFeiertage.TabIndex = 5
        '
        'Bezeichnung
        '
        Me.Bezeichnung.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Bezeichnung.HeaderText = "Bezeichnung"
        Me.Bezeichnung.Name = "Bezeichnung"
        '
        'Datum
        '
        Me.Datum.HeaderText = "Datum"
        Me.Datum.Name = "Datum"
        '
        'ID
        '
        Me.ID.HeaderText = "ID"
        Me.ID.Name = "ID"
        Me.ID.Visible = False
        '
        'UrlaubConfig
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(644, 239)
        Me.Controls.Add(Me.DGVFeiertage)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ComboboxTage)
        Me.Controls.Add(Me.BtnSpeichern)
        Me.Name = "UrlaubConfig"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Eistellungen zu den Urlaubstagen"
        CType(Me.DGVFeiertage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents BtnSpeichern As Button
    Friend WithEvents ComboboxTage As CheckedListBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents DGVFeiertage As DataGridView
    Friend WithEvents Bezeichnung As DataGridViewTextBoxColumn
    Friend WithEvents Datum As DataGridViewTextBoxColumn
    Friend WithEvents ID As DataGridViewTextBoxColumn
End Class
