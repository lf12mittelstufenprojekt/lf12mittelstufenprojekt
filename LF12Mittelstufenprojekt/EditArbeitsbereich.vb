﻿Public Class EditArbeitsbereich

    Private Sub Load() Handles MyBase.Load
        dgvArbeitsbereich.Rows.Clear()
        Dim query As String = "SELECT * FROM arbeitsbereich"
        Using mysql As New mysql_verbindung.clsMySQL
            mysql.ExecuteWithParametersAsDatareader(query)
            While mysql.DataReader.Read()
                dgvArbeitsbereich.Rows.Add(mysql.DataReader("ID"), mysql.DataReader("Bezeichnung"), "bearbeiten", "löschen")
            End While
        End Using
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Dim AddArbeitsbereich As New AddArbeitsbereich
        AddArbeitsbereich.ShowDialog()
        Load()
    End Sub

        Private Sub btnAbbrechen_Click(sender As Object, e As EventArgs) Handles btnAbbrechen.Click
            Me.Close()
        End Sub

    Private Sub dgvArbeitsbereich_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvArbeitsbereich.CellContentClick
        Select Case dgvArbeitsbereich.Columns(e.ColumnIndex).Name
            Case "bearbeiten"
                Dim EditArbeitsbereich As New AddArbeitsbereich()
                EditArbeitsbereich.Text = "Arbeitsbereich bearbeiten"
                EditArbeitsbereich.ArbeitsbereichID = dgvArbeitsbereich.Rows(e.RowIndex).Cells("ID").Value
                EditArbeitsbereich.ShowDialog()
                Load()
            Case "löschen"
                Dim query As String = "Delete from arbeitsbereich Where ID = " & dgvArbeitsbereich.Rows(e.RowIndex).Cells("ID").Value
                Using mysql As New mysql_verbindung.clsMySQL
                    mysql.ExecuteWithParametersWithoutDatareader(query)
                End Using
                Load()
                MsgBox("Erfolgreich gelöscht")
        End Select
    End Sub
End Class
