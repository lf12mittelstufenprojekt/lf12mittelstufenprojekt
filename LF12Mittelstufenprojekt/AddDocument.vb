﻿Public Class AddDocument
    Public MitarbeiterID As Integer
    Dim _file As String
    Private Property file As String
        Get
            Return _file
        End Get
        Set(value As String)
            _file = value
            If _file.Trim.Length > 0 Then
                lblDragAndDrop.Text = "1 Datei ausgewählt"
            Else
                lblDragAndDrop.Text = "Hier hinziehen"
            End If

        End Set

    End Property

    Private Sub btnHochladen_Click(sender As Object, e As EventArgs) Handles btnHochladen.Click
        If Not txtBeschreibung.Text = "" Then
            Dim Dokumentname As String = txtBeschreibung.Text
            btnHochladen.Enabled = False

            If String.IsNullOrEmpty(file) OrElse Not IO.File.Exists(file) OrElse file.Trim = "" Then
                MessageBox.Show("Error: File can´t be found.", "Dokument hochladen", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
            Dokument.SpeichereDateiUnterMitarbeiter(Dokumentname, MitarbeiterID, file)
            MessageBox.Show("Erfolgreich hochgeladen", "Dokument hochladen", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Me.Close()
        Else
            MessageBox.Show("Bitte fügen Sie eine Beschreibung hinzu.", "Dokument hochladen", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub btnDateiAuswählen_Click(sender As Object, e As EventArgs) Handles btnDateiAuswählen.Click
        Dim ofdDateien As New OpenFileDialog
        If ofdDateien.ShowDialog() = DialogResult.OK Then
            For Each tempFile In ofdDateien.FileNames()
                file = tempFile
            Next
        End If
    End Sub

    Private Sub lblDragAndDrop_DragLeave(sender As Object, e As EventArgs) Handles lblDragAndDrop.DragLeave

        lblDragAndDrop.BackColor = SystemColors.GradientActiveCaption

    End Sub

    Private Sub lblDragAndDrop_DragDrop(sender As Object, e As DragEventArgs) Handles lblDragAndDrop.DragDrop

        For Each tempFile In e.Data.GetData(DataFormats.FileDrop)
            file = tempFile
        Next

    End Sub

    Private Sub lblDragAndDrop_DragEnter(sender As Object, e As DragEventArgs) Handles lblDragAndDrop.DragEnter

        If e.Data.GetDataPresent(DataFormats.FileDrop) Then

            lblDragAndDrop.BackColor = SystemColors.MenuHighlight
            e.Effect = DragDropEffects.Copy
        End If
    End Sub
End Class