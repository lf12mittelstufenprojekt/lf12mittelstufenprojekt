﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class AddEmployee
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ButtonAbort = New System.Windows.Forms.Button()
        Me.ButtonSave = New System.Windows.Forms.Button()
        Me.DGVQualification = New System.Windows.Forms.DataGridView()
        Me.LabelQualification = New System.Windows.Forms.Label()
        Me.DTPHolidaysForm = New System.Windows.Forms.DateTimePicker()
        Me.DTPHolidaysTo = New System.Windows.Forms.DateTimePicker()
        Me.DGVHolidays = New System.Windows.Forms.DataGridView()
        Me.ButtonAddHolidays = New System.Windows.Forms.Button()
        Me.LabelWorkspace = New System.Windows.Forms.Label()
        Me.LabelPermissiongroup = New System.Windows.Forms.Label()
        Me.LabelWindowsUser = New System.Windows.Forms.Label()
        Me.TextBoxWindowsUser = New System.Windows.Forms.TextBox()
        Me.LabelEndOfWork = New System.Windows.Forms.Label()
        Me.LabelStartOfWork = New System.Windows.Forms.Label()
        Me.LabelIBAN = New System.Windows.Forms.Label()
        Me.TextBoxIBAN = New System.Windows.Forms.TextBox()
        Me.LabelTaxID = New System.Windows.Forms.Label()
        Me.TextBoxTaxID = New System.Windows.Forms.TextBox()
        Me.LabelPhone = New System.Windows.Forms.Label()
        Me.TextBoxPhone = New System.Windows.Forms.TextBox()
        Me.LabelBirthday = New System.Windows.Forms.Label()
        Me.LabelLastname = New System.Windows.Forms.Label()
        Me.TextBoxLastname = New System.Windows.Forms.TextBox()
        Me.LabelPostcode = New System.Windows.Forms.Label()
        Me.TextBoxPostcode = New System.Windows.Forms.TextBox()
        Me.LabelAddress = New System.Windows.Forms.Label()
        Me.TextBoxAddress = New System.Windows.Forms.TextBox()
        Me.LabelFirstname = New System.Windows.Forms.Label()
        Me.TextBoxFirstname = New System.Windows.Forms.TextBox()
        Me.LabelLocation = New System.Windows.Forms.Label()
        Me.TextBoxLocation = New System.Windows.Forms.TextBox()
        Me.LabelDocuments = New System.Windows.Forms.Label()
        Me.DGVDocuments = New System.Windows.Forms.DataGridView()
        Me.DokuID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Bezeichnung = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Datum = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.öffnen = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.löschen = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TextBoxHolidaysTotalDays = New System.Windows.Forms.TextBox()
        Me.BtnDeleteUrlaub = New System.Windows.Forms.Button()
        Me.MyToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnAddDokument = New System.Windows.Forms.Button()
        Me.DTPStartOfWork = New System.Windows.Forms.DateTimePicker()
        Me.DTPEndOfWork = New System.Windows.Forms.DateTimePicker()
        Me.DTPBirthday = New System.Windows.Forms.DateTimePicker()
        Me.LabelPersonalnumber = New System.Windows.Forms.Label()
        Me.TextBoxPersonalnumber = New System.Windows.Forms.TextBox()
        Me.BtnMaDelete = New System.Windows.Forms.Button()
        Me.ArbeitsbereichCombobox = New System.Windows.Forms.ComboBox()
        Me.BerechtigungsGruppeCombobox = New System.Windows.Forms.ComboBox()
        Me.ColumnHolidaysFrom = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnHolidaysTo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnHolidaysTotalDaysPerSet = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UrlaubID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.DGVQualification, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGVHolidays, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGVDocuments, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ButtonAbort
        '
        Me.ButtonAbort.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonAbort.ForeColor = System.Drawing.Color.Blue
        Me.ButtonAbort.Location = New System.Drawing.Point(914, 632)
        Me.ButtonAbort.Name = "ButtonAbort"
        Me.ButtonAbort.Size = New System.Drawing.Size(95, 30)
        Me.ButtonAbort.TabIndex = 2
        Me.ButtonAbort.Text = "Abbrechen"
        Me.ButtonAbort.UseVisualStyleBackColor = True
        '
        'ButtonSave
        '
        Me.ButtonSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonSave.ForeColor = System.Drawing.Color.Blue
        Me.ButtonSave.Location = New System.Drawing.Point(813, 632)
        Me.ButtonSave.Name = "ButtonSave"
        Me.ButtonSave.Size = New System.Drawing.Size(95, 30)
        Me.ButtonSave.TabIndex = 3
        Me.ButtonSave.Text = "Speichern"
        Me.ButtonSave.UseVisualStyleBackColor = True
        '
        'DGVQualification
        '
        Me.DGVQualification.AllowUserToOrderColumns = True
        Me.DGVQualification.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DGVQualification.BackgroundColor = System.Drawing.Color.LightSkyBlue
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGVQualification.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DGVQualification.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVQualification.Location = New System.Drawing.Point(12, 179)
        Me.DGVQualification.MultiSelect = False
        Me.DGVQualification.Name = "DGVQualification"
        Me.DGVQualification.ReadOnly = True
        Me.DGVQualification.Size = New System.Drawing.Size(450, 209)
        Me.DGVQualification.TabIndex = 18
        '
        'LabelQualification
        '
        Me.LabelQualification.AutoSize = True
        Me.LabelQualification.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelQualification.Location = New System.Drawing.Point(12, 163)
        Me.LabelQualification.Name = "LabelQualification"
        Me.LabelQualification.Size = New System.Drawing.Size(96, 13)
        Me.LabelQualification.TabIndex = 19
        Me.LabelQualification.Text = "Qualifikationen:"
        '
        'DTPHolidaysForm
        '
        Me.DTPHolidaysForm.CalendarFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DTPHolidaysForm.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DTPHolidaysForm.Location = New System.Drawing.Point(12, 22)
        Me.DTPHolidaysForm.Name = "DTPHolidaysForm"
        Me.DTPHolidaysForm.Size = New System.Drawing.Size(199, 20)
        Me.DTPHolidaysForm.TabIndex = 20
        '
        'DTPHolidaysTo
        '
        Me.DTPHolidaysTo.CalendarFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DTPHolidaysTo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DTPHolidaysTo.Location = New System.Drawing.Point(259, 23)
        Me.DTPHolidaysTo.Name = "DTPHolidaysTo"
        Me.DTPHolidaysTo.Size = New System.Drawing.Size(200, 20)
        Me.DTPHolidaysTo.TabIndex = 21
        '
        'DGVHolidays
        '
        Me.DGVHolidays.AllowUserToAddRows = False
        Me.DGVHolidays.AllowUserToDeleteRows = False
        Me.DGVHolidays.BackgroundColor = System.Drawing.Color.LightSkyBlue
        Me.DGVHolidays.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVHolidays.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ColumnHolidaysFrom, Me.ColumnHolidaysTo, Me.ColumnHolidaysTotalDaysPerSet, Me.UrlaubID})
        Me.DGVHolidays.Location = New System.Drawing.Point(12, 55)
        Me.DGVHolidays.MultiSelect = False
        Me.DGVHolidays.Name = "DGVHolidays"
        Me.DGVHolidays.ReadOnly = True
        Me.DGVHolidays.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGVHolidays.Size = New System.Drawing.Size(517, 145)
        Me.DGVHolidays.TabIndex = 22
        '
        'ButtonAddHolidays
        '
        Me.ButtonAddHolidays.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonAddHolidays.ForeColor = System.Drawing.Color.MediumVioletRed
        Me.ButtonAddHolidays.Location = New System.Drawing.Point(478, 19)
        Me.ButtonAddHolidays.Name = "ButtonAddHolidays"
        Me.ButtonAddHolidays.Size = New System.Drawing.Size(95, 30)
        Me.ButtonAddHolidays.TabIndex = 23
        Me.ButtonAddHolidays.Text = "Hinzufügen"
        Me.MyToolTip.SetToolTip(Me.ButtonAddHolidays, "Urlaub hinzufügen")
        Me.ButtonAddHolidays.UseVisualStyleBackColor = True
        '
        'LabelWorkspace
        '
        Me.LabelWorkspace.AutoSize = True
        Me.LabelWorkspace.Location = New System.Drawing.Point(9, 112)
        Me.LabelWorkspace.Name = "LabelWorkspace"
        Me.LabelWorkspace.Size = New System.Drawing.Size(71, 13)
        Me.LabelWorkspace.TabIndex = 65
        Me.LabelWorkspace.Text = "Arbeitsbreich:"
        '
        'LabelPermissiongroup
        '
        Me.LabelPermissiongroup.AutoSize = True
        Me.LabelPermissiongroup.Location = New System.Drawing.Point(763, 11)
        Me.LabelPermissiongroup.Name = "LabelPermissiongroup"
        Me.LabelPermissiongroup.Size = New System.Drawing.Size(111, 13)
        Me.LabelPermissiongroup.TabIndex = 63
        Me.LabelPermissiongroup.Text = "Berechtigungsgruppe:"
        '
        'LabelWindowsUser
        '
        Me.LabelWindowsUser.AutoSize = True
        Me.LabelWindowsUser.Location = New System.Drawing.Point(763, 112)
        Me.LabelWindowsUser.Name = "LabelWindowsUser"
        Me.LabelWindowsUser.Size = New System.Drawing.Size(95, 13)
        Me.LabelWindowsUser.TabIndex = 61
        Me.LabelWindowsUser.Text = "Windowsbenutzer:"
        '
        'TextBoxWindowsUser
        '
        Me.TextBoxWindowsUser.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.TextBoxWindowsUser.Location = New System.Drawing.Point(766, 128)
        Me.TextBoxWindowsUser.Name = "TextBoxWindowsUser"
        Me.TextBoxWindowsUser.Size = New System.Drawing.Size(151, 20)
        Me.TextBoxWindowsUser.TabIndex = 60
        '
        'LabelEndOfWork
        '
        Me.LabelEndOfWork.AutoSize = True
        Me.LabelEndOfWork.Location = New System.Drawing.Point(575, 112)
        Me.LabelEndOfWork.Name = "LabelEndOfWork"
        Me.LabelEndOfWork.Size = New System.Drawing.Size(66, 13)
        Me.LabelEndOfWork.TabIndex = 59
        Me.LabelEndOfWork.Text = "Arbeitsende:"
        '
        'LabelStartOfWork
        '
        Me.LabelStartOfWork.AutoSize = True
        Me.LabelStartOfWork.Location = New System.Drawing.Point(387, 112)
        Me.LabelStartOfWork.Name = "LabelStartOfWork"
        Me.LabelStartOfWork.Size = New System.Drawing.Size(68, 13)
        Me.LabelStartOfWork.TabIndex = 57
        Me.LabelStartOfWork.Text = "Arbeitsbegin:"
        '
        'LabelIBAN
        '
        Me.LabelIBAN.AutoSize = True
        Me.LabelIBAN.Location = New System.Drawing.Point(199, 112)
        Me.LabelIBAN.Name = "LabelIBAN"
        Me.LabelIBAN.Size = New System.Drawing.Size(35, 13)
        Me.LabelIBAN.TabIndex = 55
        Me.LabelIBAN.Text = "IBAN:"
        '
        'TextBoxIBAN
        '
        Me.TextBoxIBAN.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.TextBoxIBAN.Location = New System.Drawing.Point(202, 128)
        Me.TextBoxIBAN.Name = "TextBoxIBAN"
        Me.TextBoxIBAN.Size = New System.Drawing.Size(151, 20)
        Me.TextBoxIBAN.TabIndex = 54
        '
        'LabelTaxID
        '
        Me.LabelTaxID.AutoSize = True
        Me.LabelTaxID.Location = New System.Drawing.Point(763, 60)
        Me.LabelTaxID.Name = "LabelTaxID"
        Me.LabelTaxID.Size = New System.Drawing.Size(52, 13)
        Me.LabelTaxID.TabIndex = 52
        Me.LabelTaxID.Text = "SteuerID:"
        '
        'TextBoxTaxID
        '
        Me.TextBoxTaxID.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.TextBoxTaxID.Location = New System.Drawing.Point(766, 76)
        Me.TextBoxTaxID.Name = "TextBoxTaxID"
        Me.TextBoxTaxID.Size = New System.Drawing.Size(151, 20)
        Me.TextBoxTaxID.TabIndex = 51
        '
        'LabelPhone
        '
        Me.LabelPhone.AutoSize = True
        Me.LabelPhone.Location = New System.Drawing.Point(575, 60)
        Me.LabelPhone.Name = "LabelPhone"
        Me.LabelPhone.Size = New System.Drawing.Size(83, 13)
        Me.LabelPhone.TabIndex = 50
        Me.LabelPhone.Text = "Telefonnummer:"
        '
        'TextBoxPhone
        '
        Me.TextBoxPhone.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.TextBoxPhone.Location = New System.Drawing.Point(578, 76)
        Me.TextBoxPhone.Name = "TextBoxPhone"
        Me.TextBoxPhone.Size = New System.Drawing.Size(151, 20)
        Me.TextBoxPhone.TabIndex = 49
        '
        'LabelBirthday
        '
        Me.LabelBirthday.AutoSize = True
        Me.LabelBirthday.Location = New System.Drawing.Point(575, 11)
        Me.LabelBirthday.Name = "LabelBirthday"
        Me.LabelBirthday.Size = New System.Drawing.Size(76, 13)
        Me.LabelBirthday.TabIndex = 48
        Me.LabelBirthday.Text = "Geburtsdatum:"
        '
        'LabelLastname
        '
        Me.LabelLastname.AutoSize = True
        Me.LabelLastname.Location = New System.Drawing.Point(387, 11)
        Me.LabelLastname.Name = "LabelLastname"
        Me.LabelLastname.Size = New System.Drawing.Size(62, 13)
        Me.LabelLastname.TabIndex = 46
        Me.LabelLastname.Text = "Nachname:"
        '
        'TextBoxLastname
        '
        Me.TextBoxLastname.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.TextBoxLastname.Location = New System.Drawing.Point(390, 27)
        Me.TextBoxLastname.Name = "TextBoxLastname"
        Me.TextBoxLastname.Size = New System.Drawing.Size(151, 20)
        Me.TextBoxLastname.TabIndex = 45
        '
        'LabelPostcode
        '
        Me.LabelPostcode.AutoSize = True
        Me.LabelPostcode.Location = New System.Drawing.Point(199, 60)
        Me.LabelPostcode.Name = "LabelPostcode"
        Me.LabelPostcode.Size = New System.Drawing.Size(63, 13)
        Me.LabelPostcode.TabIndex = 44
        Me.LabelPostcode.Text = "Postleitzahl:"
        '
        'TextBoxPostcode
        '
        Me.TextBoxPostcode.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.TextBoxPostcode.Location = New System.Drawing.Point(202, 76)
        Me.TextBoxPostcode.Name = "TextBoxPostcode"
        Me.TextBoxPostcode.Size = New System.Drawing.Size(151, 20)
        Me.TextBoxPostcode.TabIndex = 43
        '
        'LabelAddress
        '
        Me.LabelAddress.AutoSize = True
        Me.LabelAddress.Location = New System.Drawing.Point(11, 60)
        Me.LabelAddress.Name = "LabelAddress"
        Me.LabelAddress.Size = New System.Drawing.Size(48, 13)
        Me.LabelAddress.TabIndex = 42
        Me.LabelAddress.Text = "Adresse:"
        '
        'TextBoxAddress
        '
        Me.TextBoxAddress.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.TextBoxAddress.Location = New System.Drawing.Point(14, 76)
        Me.TextBoxAddress.Name = "TextBoxAddress"
        Me.TextBoxAddress.Size = New System.Drawing.Size(151, 20)
        Me.TextBoxAddress.TabIndex = 41
        '
        'LabelFirstname
        '
        Me.LabelFirstname.AutoSize = True
        Me.LabelFirstname.Location = New System.Drawing.Point(199, 11)
        Me.LabelFirstname.Name = "LabelFirstname"
        Me.LabelFirstname.Size = New System.Drawing.Size(52, 13)
        Me.LabelFirstname.TabIndex = 40
        Me.LabelFirstname.Text = "Vorname:"
        '
        'TextBoxFirstname
        '
        Me.TextBoxFirstname.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.TextBoxFirstname.Location = New System.Drawing.Point(202, 27)
        Me.TextBoxFirstname.Name = "TextBoxFirstname"
        Me.TextBoxFirstname.Size = New System.Drawing.Size(151, 20)
        Me.TextBoxFirstname.TabIndex = 39
        '
        'LabelLocation
        '
        Me.LabelLocation.AutoSize = True
        Me.LabelLocation.Location = New System.Drawing.Point(387, 60)
        Me.LabelLocation.Name = "LabelLocation"
        Me.LabelLocation.Size = New System.Drawing.Size(24, 13)
        Me.LabelLocation.TabIndex = 67
        Me.LabelLocation.Text = "Ort:"
        '
        'TextBoxLocation
        '
        Me.TextBoxLocation.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.TextBoxLocation.Location = New System.Drawing.Point(390, 76)
        Me.TextBoxLocation.Name = "TextBoxLocation"
        Me.TextBoxLocation.Size = New System.Drawing.Size(151, 20)
        Me.TextBoxLocation.TabIndex = 66
        '
        'LabelDocuments
        '
        Me.LabelDocuments.AutoSize = True
        Me.LabelDocuments.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelDocuments.Location = New System.Drawing.Point(476, 163)
        Me.LabelDocuments.Name = "LabelDocuments"
        Me.LabelDocuments.Size = New System.Drawing.Size(75, 13)
        Me.LabelDocuments.TabIndex = 69
        Me.LabelDocuments.Text = "Dokumente:"
        '
        'DGVDocuments
        '
        Me.DGVDocuments.AllowUserToAddRows = False
        Me.DGVDocuments.AllowUserToDeleteRows = False
        Me.DGVDocuments.AllowUserToResizeColumns = False
        Me.DGVDocuments.AllowUserToResizeRows = False
        Me.DGVDocuments.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DGVDocuments.BackgroundColor = System.Drawing.Color.LightSkyBlue
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGVDocuments.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.DGVDocuments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVDocuments.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DokuID, Me.Bezeichnung, Me.Datum, Me.öffnen, Me.löschen})
        Me.DGVDocuments.Location = New System.Drawing.Point(476, 179)
        Me.DGVDocuments.MultiSelect = False
        Me.DGVDocuments.Name = "DGVDocuments"
        Me.DGVDocuments.RowHeadersVisible = False
        Me.DGVDocuments.Size = New System.Drawing.Size(450, 209)
        Me.DGVDocuments.TabIndex = 68
        '
        'DokuID
        '
        Me.DokuID.HeaderText = "DokuID"
        Me.DokuID.Name = "DokuID"
        Me.DokuID.Visible = False
        '
        'Bezeichnung
        '
        Me.Bezeichnung.HeaderText = "Bezeichnung"
        Me.Bezeichnung.Name = "Bezeichnung"
        '
        'Datum
        '
        Me.Datum.HeaderText = "Datum"
        Me.Datum.Name = "Datum"
        '
        'öffnen
        '
        Me.öffnen.HeaderText = "öffnen"
        Me.öffnen.Name = "öffnen"
        '
        'löschen
        '
        Me.löschen.HeaderText = "löschen"
        Me.löschen.Name = "löschen"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.TextBoxHolidaysTotalDays)
        Me.GroupBox1.Controls.Add(Me.DGVHolidays)
        Me.GroupBox1.Controls.Add(Me.DTPHolidaysForm)
        Me.GroupBox1.Controls.Add(Me.DTPHolidaysTo)
        Me.GroupBox1.Controls.Add(Me.BtnDeleteUrlaub)
        Me.GroupBox1.Controls.Add(Me.ButtonAddHolidays)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(12, 411)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(680, 255)
        Me.GroupBox1.TabIndex = 71
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Urlaub hinzufügen"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(133, 222)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(223, 13)
        Me.Label1.TabIndex = 25
        Me.Label1.Text = "Gesamtanzahl benötigter Urlaubstage:"
        '
        'TextBoxHolidaysTotalDays
        '
        Me.TextBoxHolidaysTotalDays.BackColor = System.Drawing.SystemColors.ControlLight
        Me.TextBoxHolidaysTotalDays.Location = New System.Drawing.Point(356, 219)
        Me.TextBoxHolidaysTotalDays.Name = "TextBoxHolidaysTotalDays"
        Me.TextBoxHolidaysTotalDays.ReadOnly = True
        Me.TextBoxHolidaysTotalDays.Size = New System.Drawing.Size(92, 20)
        Me.TextBoxHolidaysTotalDays.TabIndex = 24
        '
        'BtnDeleteUrlaub
        '
        Me.BtnDeleteUrlaub.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnDeleteUrlaub.ForeColor = System.Drawing.Color.MediumVioletRed
        Me.BtnDeleteUrlaub.Location = New System.Drawing.Point(579, 19)
        Me.BtnDeleteUrlaub.Name = "BtnDeleteUrlaub"
        Me.BtnDeleteUrlaub.Size = New System.Drawing.Size(95, 30)
        Me.BtnDeleteUrlaub.TabIndex = 76
        Me.BtnDeleteUrlaub.Text = "Löschen"
        Me.MyToolTip.SetToolTip(Me.BtnDeleteUrlaub, "Urlaub löschen")
        Me.BtnDeleteUrlaub.UseVisualStyleBackColor = True
        '
        'MyToolTip
        '
        Me.MyToolTip.AutomaticDelay = 0
        Me.MyToolTip.AutoPopDelay = 5000
        Me.MyToolTip.InitialDelay = 0
        Me.MyToolTip.ReshowDelay = 0
        Me.MyToolTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info
        Me.MyToolTip.ToolTipTitle = "Info:"
        '
        'btnAddDokument
        '
        Me.btnAddDokument.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddDokument.ForeColor = System.Drawing.Color.MediumVioletRed
        Me.btnAddDokument.Location = New System.Drawing.Point(831, 394)
        Me.btnAddDokument.Name = "btnAddDokument"
        Me.btnAddDokument.Size = New System.Drawing.Size(95, 30)
        Me.btnAddDokument.TabIndex = 77
        Me.btnAddDokument.Text = "Hinzufügen"
        Me.MyToolTip.SetToolTip(Me.btnAddDokument, "Urlaub hinzufügen")
        Me.btnAddDokument.UseVisualStyleBackColor = True
        '
        'DTPStartOfWork
        '
        Me.DTPStartOfWork.CalendarMonthBackground = System.Drawing.SystemColors.GradientInactiveCaption
        Me.DTPStartOfWork.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DTPStartOfWork.Location = New System.Drawing.Point(390, 128)
        Me.DTPStartOfWork.MaxDate = New Date(2099, 12, 31, 0, 0, 0, 0)
        Me.DTPStartOfWork.MinDate = New Date(1900, 1, 1, 0, 0, 0, 0)
        Me.DTPStartOfWork.Name = "DTPStartOfWork"
        Me.DTPStartOfWork.Size = New System.Drawing.Size(151, 20)
        Me.DTPStartOfWork.TabIndex = 72
        '
        'DTPEndOfWork
        '
        Me.DTPEndOfWork.CalendarMonthBackground = System.Drawing.SystemColors.ActiveCaption
        Me.DTPEndOfWork.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DTPEndOfWork.Location = New System.Drawing.Point(578, 128)
        Me.DTPEndOfWork.MaxDate = New Date(2099, 12, 31, 0, 0, 0, 0)
        Me.DTPEndOfWork.MinDate = New Date(1900, 1, 1, 0, 0, 0, 0)
        Me.DTPEndOfWork.Name = "DTPEndOfWork"
        Me.DTPEndOfWork.Size = New System.Drawing.Size(151, 20)
        Me.DTPEndOfWork.TabIndex = 73
        '
        'DTPBirthday
        '
        Me.DTPBirthday.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DTPBirthday.Location = New System.Drawing.Point(578, 27)
        Me.DTPBirthday.MaxDate = New Date(2099, 12, 31, 0, 0, 0, 0)
        Me.DTPBirthday.MinDate = New Date(1900, 1, 1, 0, 0, 0, 0)
        Me.DTPBirthday.Name = "DTPBirthday"
        Me.DTPBirthday.Size = New System.Drawing.Size(151, 20)
        Me.DTPBirthday.TabIndex = 74
        '
        'LabelPersonalnumber
        '
        Me.LabelPersonalnumber.AutoSize = True
        Me.LabelPersonalnumber.Location = New System.Drawing.Point(12, 11)
        Me.LabelPersonalnumber.Name = "LabelPersonalnumber"
        Me.LabelPersonalnumber.Size = New System.Drawing.Size(88, 13)
        Me.LabelPersonalnumber.TabIndex = 79
        Me.LabelPersonalnumber.Text = "Personalnummer:"
        '
        'TextBoxPersonalnumber
        '
        Me.TextBoxPersonalnumber.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.TextBoxPersonalnumber.Location = New System.Drawing.Point(15, 27)
        Me.TextBoxPersonalnumber.Name = "TextBoxPersonalnumber"
        Me.TextBoxPersonalnumber.Size = New System.Drawing.Size(151, 20)
        Me.TextBoxPersonalnumber.TabIndex = 78
        '
        'BtnMaDelete
        '
        Me.BtnMaDelete.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnMaDelete.ForeColor = System.Drawing.Color.Blue
        Me.BtnMaDelete.Location = New System.Drawing.Point(698, 632)
        Me.BtnMaDelete.Name = "BtnMaDelete"
        Me.BtnMaDelete.Size = New System.Drawing.Size(95, 30)
        Me.BtnMaDelete.TabIndex = 80
        Me.BtnMaDelete.Text = "MA Löschen"
        Me.BtnMaDelete.UseVisualStyleBackColor = True
        '
        'ArbeitsbereichCombobox
        '
        Me.ArbeitsbereichCombobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ArbeitsbereichCombobox.FormattingEnabled = True
        Me.ArbeitsbereichCombobox.Location = New System.Drawing.Point(12, 127)
        Me.ArbeitsbereichCombobox.Name = "ArbeitsbereichCombobox"
        Me.ArbeitsbereichCombobox.Size = New System.Drawing.Size(151, 21)
        Me.ArbeitsbereichCombobox.TabIndex = 82
        '
        'BerechtigungsGruppeCombobox
        '
        Me.BerechtigungsGruppeCombobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.BerechtigungsGruppeCombobox.FormattingEnabled = True
        Me.BerechtigungsGruppeCombobox.Location = New System.Drawing.Point(766, 26)
        Me.BerechtigungsGruppeCombobox.Name = "BerechtigungsGruppeCombobox"
        Me.BerechtigungsGruppeCombobox.Size = New System.Drawing.Size(151, 21)
        Me.BerechtigungsGruppeCombobox.TabIndex = 83
        '
        'ColumnHolidaysFrom
        '
        Me.ColumnHolidaysFrom.HeaderText = "Urlaub von"
        Me.ColumnHolidaysFrom.Name = "ColumnHolidaysFrom"
        Me.ColumnHolidaysFrom.ReadOnly = True
        Me.ColumnHolidaysFrom.Width = 150
        '
        'ColumnHolidaysTo
        '
        Me.ColumnHolidaysTo.HeaderText = "Urlaub bis"
        Me.ColumnHolidaysTo.Name = "ColumnHolidaysTo"
        Me.ColumnHolidaysTo.ReadOnly = True
        Me.ColumnHolidaysTo.Width = 150
        '
        'ColumnHolidaysTotalDaysPerSet
        '
        Me.ColumnHolidaysTotalDaysPerSet.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.ColumnHolidaysTotalDaysPerSet.HeaderText = "Anzahl Tage pro Urlaub"
        Me.ColumnHolidaysTotalDaysPerSet.Name = "ColumnHolidaysTotalDaysPerSet"
        Me.ColumnHolidaysTotalDaysPerSet.ReadOnly = True
        '
        'UrlaubID
        '
        Me.UrlaubID.HeaderText = "UrlaubID"
        Me.UrlaubID.Name = "UrlaubID"
        Me.UrlaubID.ReadOnly = True
        Me.UrlaubID.Visible = False
        '
        'AddEmployee
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1021, 674)
        Me.Controls.Add(Me.BerechtigungsGruppeCombobox)
        Me.Controls.Add(Me.ArbeitsbereichCombobox)
        Me.Controls.Add(Me.BtnMaDelete)
        Me.Controls.Add(Me.LabelPersonalnumber)
        Me.Controls.Add(Me.TextBoxPersonalnumber)
        Me.Controls.Add(Me.btnAddDokument)
        Me.Controls.Add(Me.DTPBirthday)
        Me.Controls.Add(Me.DTPEndOfWork)
        Me.Controls.Add(Me.DTPStartOfWork)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.LabelDocuments)
        Me.Controls.Add(Me.DGVDocuments)
        Me.Controls.Add(Me.LabelLocation)
        Me.Controls.Add(Me.TextBoxLocation)
        Me.Controls.Add(Me.LabelWorkspace)
        Me.Controls.Add(Me.LabelPermissiongroup)
        Me.Controls.Add(Me.LabelWindowsUser)
        Me.Controls.Add(Me.TextBoxWindowsUser)
        Me.Controls.Add(Me.LabelEndOfWork)
        Me.Controls.Add(Me.LabelStartOfWork)
        Me.Controls.Add(Me.LabelIBAN)
        Me.Controls.Add(Me.TextBoxIBAN)
        Me.Controls.Add(Me.LabelTaxID)
        Me.Controls.Add(Me.TextBoxTaxID)
        Me.Controls.Add(Me.LabelPhone)
        Me.Controls.Add(Me.TextBoxPhone)
        Me.Controls.Add(Me.LabelBirthday)
        Me.Controls.Add(Me.LabelLastname)
        Me.Controls.Add(Me.TextBoxLastname)
        Me.Controls.Add(Me.LabelPostcode)
        Me.Controls.Add(Me.TextBoxPostcode)
        Me.Controls.Add(Me.LabelAddress)
        Me.Controls.Add(Me.TextBoxAddress)
        Me.Controls.Add(Me.LabelFirstname)
        Me.Controls.Add(Me.TextBoxFirstname)
        Me.Controls.Add(Me.LabelQualification)
        Me.Controls.Add(Me.DGVQualification)
        Me.Controls.Add(Me.ButtonSave)
        Me.Controls.Add(Me.ButtonAbort)
        Me.Name = "AddEmployee"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Mitarbeiter Hinzufügen"
        CType(Me.DGVQualification, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGVHolidays, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGVDocuments, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ButtonAbort As Button
    Friend WithEvents ButtonSave As Button
    Friend WithEvents DGVQualification As DataGridView
    Friend WithEvents LabelQualification As Label
    Friend WithEvents DTPHolidaysForm As DateTimePicker
    Friend WithEvents DTPHolidaysTo As DateTimePicker
    Friend WithEvents DGVHolidays As DataGridView
    Friend WithEvents ButtonAddHolidays As Button
    Friend WithEvents LabelWorkspace As Label
    Friend WithEvents LabelPermissiongroup As Label
    Friend WithEvents LabelWindowsUser As Label
    Friend WithEvents TextBoxWindowsUser As TextBox
    Friend WithEvents LabelEndOfWork As Label
    Friend WithEvents LabelStartOfWork As Label
    Friend WithEvents LabelIBAN As Label
    Friend WithEvents TextBoxIBAN As TextBox
    Friend WithEvents LabelTaxID As Label
    Friend WithEvents TextBoxTaxID As TextBox
    Friend WithEvents LabelPhone As Label
    Friend WithEvents TextBoxPhone As TextBox
    Friend WithEvents LabelBirthday As Label
    Friend WithEvents LabelLastname As Label
    Friend WithEvents TextBoxLastname As TextBox
    Friend WithEvents LabelPostcode As Label
    Friend WithEvents TextBoxPostcode As TextBox
    Friend WithEvents LabelAddress As Label
    Friend WithEvents TextBoxAddress As TextBox
    Friend WithEvents LabelFirstname As Label
    Friend WithEvents TextBoxFirstname As TextBox
    Friend WithEvents LabelLocation As Label
    Friend WithEvents TextBoxLocation As TextBox
    Friend WithEvents LabelDocuments As Label
    Friend WithEvents DGVDocuments As DataGridView
    Friend WithEvents MyToolTip As ToolTip
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents DTPStartOfWork As DateTimePicker
    Friend WithEvents DTPEndOfWork As DateTimePicker
    Friend WithEvents DTPBirthday As DateTimePicker
    Friend WithEvents Label1 As Label
    Friend WithEvents TextBoxHolidaysTotalDays As TextBox
    Friend WithEvents BtnDeleteUrlaub As Button
    Friend WithEvents DokuID As DataGridViewTextBoxColumn
    Friend WithEvents Bezeichnung As DataGridViewTextBoxColumn
    Friend WithEvents Datum As DataGridViewTextBoxColumn
    Friend WithEvents öffnen As DataGridViewButtonColumn
    Friend WithEvents löschen As DataGridViewButtonColumn
    Friend WithEvents btnAddDokument As Button
    Friend WithEvents LabelPersonalnumber As Label
    Friend WithEvents TextBoxPersonalnumber As TextBox
    Friend WithEvents BtnMaDelete As Button
    Friend WithEvents ArbeitsbereichCombobox As ComboBox
    Friend WithEvents BerechtigungsGruppeCombobox As ComboBox
    Friend WithEvents ColumnHolidaysFrom As DataGridViewTextBoxColumn
    Friend WithEvents ColumnHolidaysTo As DataGridViewTextBoxColumn
    Friend WithEvents ColumnHolidaysTotalDaysPerSet As DataGridViewTextBoxColumn
    Friend WithEvents UrlaubID As DataGridViewTextBoxColumn
End Class
