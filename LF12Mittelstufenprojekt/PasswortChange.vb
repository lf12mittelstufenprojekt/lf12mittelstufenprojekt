﻿
Public Class PassWortChange

    Private _loginService As LoginService

    Public Sub New(loginService As LoginService)
        InitializeComponent()
        _loginService = loginService
    End Sub

    Private Sub BtnÄndern_Click(sender As Object, e As EventArgs) Handles BtnÄndern.Click
        Dim altPasswort As String = TxtAltesPasswort.Text
        Dim neuPasswort As String = TxtNeuPasswort.Text
        Dim wiederholungPasswort As String = TxtWiederholung.Text

        If Not _loginService.IstPasswortValid(altPasswort) Then
            MessageBox.Show("Altes Passwort nicht korrekt!", "Passwort Änderung", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return
        End If

        Dim falschesPasswortFormatMsg As String = _loginService.CheckPasswortFormat(neuPasswort)

        If Not neuPasswort.Equals(wiederholungPasswort) Then
            MessageBox.Show("Eingegebene Passwörter stimmen nicht überein!", "Passwort Änderung", MessageBoxButtons.OK, MessageBoxIcon.Information)
        ElseIf Not String.IsNullOrWhiteSpace(falschesPasswortFormatMsg) Then
            MessageBox.Show(falschesPasswortFormatMsg, "Passwort Änderung", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            _loginService.UpdatePasswort(neuPasswort)
            MessageBox.Show("Passwort wurde erfolgreich geändert!", "Passwort Änderung", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Close()
        End If
    End Sub


    Private Sub ChkShowPW_CheckedChanged(sender As Object, e As EventArgs) Handles ChkShowPW.CheckedChanged
        If ChkShowPW.CheckState = CheckState.Checked Then
            TxtNeuPasswort.UseSystemPasswordChar = False
            TxtWiederholung.UseSystemPasswordChar = False
            TxtAltesPasswort.UseSystemPasswordChar = False
        Else
            TxtNeuPasswort.UseSystemPasswordChar = True
            TxtWiederholung.UseSystemPasswordChar = True
            TxtAltesPasswort.UseSystemPasswordChar = True
        End If
    End Sub

    Private Sub BtnCancel_Click(sender As Object, e As EventArgs) Handles BtnCancel.Click
        Close()
    End Sub
End Class