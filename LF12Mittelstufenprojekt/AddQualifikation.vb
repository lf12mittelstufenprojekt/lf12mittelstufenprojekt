﻿Public Class AddQualifikation
    Public QualifikationsID As Integer = 0
    Private Sub btnAbbrechen_Click(sender As Object, e As EventArgs) Handles btnAbbrechen.Click
        Me.Close()
    End Sub

    Private Sub btnSpeichern_Click(sender As Object, e As EventArgs) Handles btnSpeichern.Click
        If QualifikationsID = 0 Then
            If txtQualifikation.Text = "" Then
                MsgBox("Bitte geben sie eine gültige Bezeichnung für die Qualifikation ein.")
            Else
                Dim query As String = "INSERT INTO qualifikationen (Bezeichnung) VALUES ('" & txtQualifikation.Text & "')"
                Using mysql As New mysql_verbindung.clsMySQL()
                    mysql.ExecuteInsertWithParameter(query)
                End Using
                MsgBox("Erfolgreich hinzugefügt.")
                Me.Close()
            End If
        Else
            If txtQualifikation.Text = "" Then
                MsgBox("Bitte geben sie eine gültige Bezeichnung für die Qualifikation ein.")
            Else
                Dim query As String = "Update qualifikationen Set Bezeichnung = '" & txtQualifikation.Text & "' Where ID = " & QualifikationsID
                Using mysql As New mysql_verbindung.clsMySQL()
                    mysql.ExecuteInsertWithParameter(query)
                End Using
                MsgBox("Erfolgreich geändert.")
                Me.Close()
            End If
        End If

    End Sub

    Private Sub AddQualifikation_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If QualifikationsID = 0 Then

        Else
            Dim query As String = "Select * from qualifikationen where ID = " & QualifikationsID
            Using mysql As New mysql_verbindung.clsMySQL
                mysql.ExecuteWithParametersAsDatareader(query)
                While mysql.DataReader.Read()
                    txtQualifikation.Text = mysql.DataReader("Bezeichnung")
                End While
            End Using
        End If
    End Sub
End Class