﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainFormLogin
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TxtPassword = New System.Windows.Forms.TextBox()
        Me.lblPw = New System.Windows.Forms.Label()
        Me.BtnLogin = New System.Windows.Forms.Button()
        Me.PageSetupDialog1 = New System.Windows.Forms.PageSetupDialog()
        Me.BtnCancel = New System.Windows.Forms.Button()
        Me.ChkShowPW = New System.Windows.Forms.CheckBox()
        Me.lblCapsLock = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.SuspendLayout()
        '
        'TxtPassword
        '
        Me.TxtPassword.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.TxtPassword.Location = New System.Drawing.Point(29, 33)
        Me.TxtPassword.Name = "TxtPassword"
        Me.TxtPassword.Size = New System.Drawing.Size(134, 20)
        Me.TxtPassword.TabIndex = 1
        Me.TxtPassword.UseSystemPasswordChar = True
        '
        'lblPw
        '
        Me.lblPw.AutoSize = True
        Me.lblPw.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPw.Location = New System.Drawing.Point(26, 17)
        Me.lblPw.Name = "lblPw"
        Me.lblPw.Size = New System.Drawing.Size(62, 13)
        Me.lblPw.TabIndex = 2
        Me.lblPw.Text = "Passwort:"
        '
        'BtnLogin
        '
        Me.BtnLogin.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnLogin.ForeColor = System.Drawing.Color.Blue
        Me.BtnLogin.Location = New System.Drawing.Point(12, 131)
        Me.BtnLogin.Name = "BtnLogin"
        Me.BtnLogin.Size = New System.Drawing.Size(95, 30)
        Me.BtnLogin.TabIndex = 3
        Me.BtnLogin.Text = "Anmelden"
        Me.BtnLogin.UseVisualStyleBackColor = True
        '
        'BtnCancel
        '
        Me.BtnCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnCancel.ForeColor = System.Drawing.Color.Blue
        Me.BtnCancel.Location = New System.Drawing.Point(136, 131)
        Me.BtnCancel.Name = "BtnCancel"
        Me.BtnCancel.Size = New System.Drawing.Size(95, 30)
        Me.BtnCancel.TabIndex = 4
        Me.BtnCancel.Text = "Abbrechen"
        Me.BtnCancel.UseVisualStyleBackColor = True
        '
        'ChkShowPW
        '
        Me.ChkShowPW.AutoSize = True
        Me.ChkShowPW.Location = New System.Drawing.Point(29, 60)
        Me.ChkShowPW.Name = "ChkShowPW"
        Me.ChkShowPW.Size = New System.Drawing.Size(115, 17)
        Me.ChkShowPW.TabIndex = 2
        Me.ChkShowPW.Text = "Passwort anzeigen"
        Me.ChkShowPW.UseVisualStyleBackColor = True
        '
        'lblCapsLock
        '
        Me.lblCapsLock.AutoSize = True
        Me.lblCapsLock.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCapsLock.ForeColor = System.Drawing.Color.Red
        Me.lblCapsLock.Location = New System.Drawing.Point(26, 90)
        Me.lblCapsLock.Name = "lblCapsLock"
        Me.lblCapsLock.Size = New System.Drawing.Size(173, 15)
        Me.lblCapsLock.TabIndex = 7
        Me.lblCapsLock.Text = "Feststelltaste ist aktiviert !"
        Me.lblCapsLock.Visible = False
        '
        'Timer1
        '
        Me.Timer1.Interval = 5000
        '
        'MainFormLogin
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(243, 173)
        Me.Controls.Add(Me.lblCapsLock)
        Me.Controls.Add(Me.ChkShowPW)
        Me.Controls.Add(Me.BtnCancel)
        Me.Controls.Add(Me.BtnLogin)
        Me.Controls.Add(Me.lblPw)
        Me.Controls.Add(Me.TxtPassword)
        Me.Name = "MainFormLogin"
        Me.Text = "Admin Login"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TxtPassword As TextBox
    Friend WithEvents lblPw As Label
    Friend WithEvents BtnLogin As Button
    Friend WithEvents PageSetupDialog1 As PageSetupDialog
    Friend WithEvents BtnCancel As Button
    Friend WithEvents ChkShowPW As CheckBox
    Friend WithEvents lblCapsLock As Label
    Friend WithEvents Timer1 As Timer
End Class
