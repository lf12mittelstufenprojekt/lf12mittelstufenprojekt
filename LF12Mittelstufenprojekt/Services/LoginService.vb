﻿Imports System.Security.Cryptography
Imports System.Text

Public Class LoginService

    Private loggedIn As Boolean = False

    Public Sub UpdatePasswort(neuPasswort As String)
        Dim hashPasswort As String = getHash(neuPasswort)

        ' Es gibt nur ein Admin Passwort in der Tabelle
        Dim query As String = "update login set Passwort = '" & hashPasswort & "'"

        Using mysql As New mysql_verbindung.clsMySQL
            mysql.ExecuteWithParametersAsDatareader(query)
        End Using

    End Sub

    Public Function IstPasswortValid(passwort As String) As Boolean
        Dim hashPasswort As String = getHash(passwort)

        Dim query As String = "select Passwort from login where Passwort = '" & hashPasswort & "'"
        Using mysql As New mysql_verbindung.clsMySQL
            mysql.ExecuteWithParametersAsDatareader(query)

            If Not mysql.DataReader.Read() Then
                Return False
            End If
        End Using

        loggedIn = True
        Return True
    End Function

    Public Function CheckPasswortFormat(passwort As String) As String
        If passwort.Length < 8 Then
            Return "Das Passwort muss mindestens 8-stellig sein!"
        End If
        'Weitere Überprüfungen

        Return ""
    End Function

    Public Function IsLoggedIn() As Boolean
        Return loggedIn
    End Function

    Private Function getHash(passwort As String) As String
        Dim sha As New SHA512Managed()
        Dim p As Byte() = Encoding.UTF8.GetBytes(passwort)

        Dim sb As New StringBuilder()
        Dim hashedBytes As Byte() = sha.ComputeHash(p)

        For Each b As Byte In hashedBytes
            sb.Append(Hex(b))
        Next

        Dim hash As String = sb.ToString()

        ' In Tabelle auf 200 Zeichen beschränkt
        If hash.Length > 200 Then
            hash = hash.Substring(0, 200)
        End If

        Return hash
    End Function

End Class
