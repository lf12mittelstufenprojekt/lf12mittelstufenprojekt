﻿Imports System.Text.RegularExpressions

Public Class MitarbeiterValidationService

    Private datumFormate As New List(Of String) From
        {"9.9.9999", "99.99.9999", "9.99.9999", "99.9.9999"}

    ' Vorname, Nachname, Ort, Adresse, Telefonnr. SteuerID, IBAN, Arbeitsbereich, Berechtigungsgruppe prüfen
    Public Function BasisCheck(str As String) As Boolean
        Return Not String.IsNullOrWhiteSpace(str)
    End Function

    ' PLZ muss 5-stellig und Integer
    Public Function CheckPLZ(plz As String) As Boolean
        Dim i As Integer = -1

        If plz.Length = 5 AndAlso Integer.TryParse(plz, i) AndAlso i > 0 Then
            Return True
        End If

        Return False
    End Function

    Public Function CheckPersonalNr(nr As String) As Boolean
        Dim p As Integer = 0
        Return Integer.TryParse(nr, p) AndAlso p > 0
    End Function

    ' Geburtsdatum, ArbeitsEinstieg, ArbeitsEnde überprüfen
    Public Function CheckDatum(datum As String) As Boolean
        Dim datumFormat As String = Regex.Replace(datum, "[0-9]", "9")
        If Not datumFormate.Contains(datumFormat) Then
            Return False
        End If

        Dim d As Date
        If Not DateTime.TryParse(datum, d) Then
            Return False
        End If

        If d.Year < 1900 OrElse d.Year > 2100 Then
            Return False
        End If

        Return True
    End Function

End Class
