﻿Public Class UrlaubService

    ' Feiertag werden nicht von Urlaubstagen abgezogen
    Private feiertage As List(Of Date)
    ' Tage die nicht als Urlaub gezählt werden Mo: 0, Di: 1 usw. (z.B. Sa\So)
    Private keineUrlaubstage As List(Of Integer)
    Public Const EndeDatumVorAnfangMsg As String = "Fehler, das Enddatum darf nicht vor dem Anfangsdatum liegen."
    Public Const UrlaubUeberschneidungMsg As String = "Der eingetragene Urlaub überschneidet sich mit einem anderen Urlaub in dieser Liste."

    Public Function LadeUrlaube(dgvUrlaub As DataGridView, mitarbeiterId As Integer) As List(Of Urlaub)
        LadeFeiertage()
        LadeKeineUrlaubsTage()

        Dim urlaube As List(Of Urlaub) = GetUrlaube(mitarbeiterId)
        AddUrlaubeInGridView(dgvUrlaub, urlaube)

        Return urlaube
    End Function

    Public Sub SpeicherUrlaube(dgvUrlaub As DataGridView, mitarbeiterId As Integer)
        Dim urlaube As List(Of Urlaub) = GetUrlaubeFromGrid(dgvUrlaub, mitarbeiterId)

        For Each urlaub As Urlaub In urlaube
            ' Urlaub wurde noch nicht gespeichert
            If urlaub.ID = -1 Then
                Dim urlaubsBeginn As String = urlaub.UrlaubsBeginn.ToString("yyyy-MM-dd")
                Dim urlaubsEnde As String = urlaub.UrlaubsEnde.ToString("yyyy-MM-dd")

                Dim query As String = "INSERT INTO urlaub (MitarbeiterID, UrlaubsBeginn, UrlaubsEnde) VALUES('" & mitarbeiterId.ToString() & "', '" & urlaubsBeginn & "', '" & urlaubsEnde & "')"

                Using mysql As New mysql_verbindung.clsMySQL
                    mysql.ExecuteWithParametersAsDatareader(query)
                End Using
            End If
        Next
    End Sub

    ' Urlaub in GridView hinzufügen, noch nicht in Datenbank speichern
    Public Function AddUrlaub(dgvUrlaub As DataGridView, mitarbeiterId As Integer, beginn As Date, ende As Date) As Integer
        ' Uhrzeit von Datum entfernen
        beginn = New Date(beginn.Year, beginn.Month, beginn.Day)
        ende = New Date(ende.Year, ende.Month, ende.Day)

        If feiertage Is Nothing OrElse keineUrlaubstage Is Nothing Then
            ' Wenn Listen noh nicht geladen, aus DB laden.
            LadeFeiertage()
            LadeKeineUrlaubsTage()
        End If

        Dim urlaube As List(Of Urlaub) = GetUrlaubeFromGrid(dgvUrlaub, mitarbeiterId)
        Dim msg As String = CheckUrlaub(urlaube, beginn, ende)
        Dim urlaubsTageAnzahl = 0

        If msg = "" Then
            urlaubsTageAnzahl = CalcUrlaubsTageFuerZeitraum(beginn, ende)
            ' Neu hinzugefügter Urlaub hat noch ID -1 da noch nicht gespeichert
            dgvUrlaub.Rows.Insert(dgvUrlaub.Rows.Count, beginn.ToLongDateString(), ende.ToLongDateString(), urlaubsTageAnzahl, -1)
        Else
            MessageBox.Show(msg, "Urlaub hinzufügen", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If

        Return urlaubsTageAnzahl
    End Function

    Public Function GetTotalUrlaubstage(urlaube As List(Of Urlaub)) As Integer
        Dim total As Integer = 0
        For Each urlaub As Urlaub In urlaube
            total += urlaub.Tage
        Next

        Return total
    End Function

    Public Function GetTotalUrlaubstage(dgvUrlaub As DataGridView, mitarbeiterId As Integer) As Integer
        Dim urlaube As List(Of Urlaub) = GetUrlaubeFromGrid(dgvUrlaub, mitarbeiterId)
        Return GetTotalUrlaubstage(urlaube)
    End Function

    Public Function CheckUrlaub(urlaube As List(Of Urlaub), beginn As Date, ende As Date) As String
        ' Endedatum liegt vor Beginn
        If ende < beginn Then
            Return endeDatumVorAnfangMsg
        End If

        For Each urlaub As Urlaub In urlaube
            Dim von As Date = urlaub.UrlaubsBeginn
            Dim bis As Date = urlaub.UrlaubsEnde

            ' Überprüfen, ob neuer Urlaub sich mit irgendeinem bereits vorhanden überschneidet
            If IsDatumInZeitraum(von, bis, beginn) OrElse 'Ist Anfang oder Ende in bereits vorhandenem Zeitraum
               IsDatumInZeitraum(von, bis, ende) OrElse
               beginn < von AndAlso ende > bis Then ' Neuer Urlaub überlappt anderen Urlaub komplett
                Return UrlaubUeberschneidungMsg
            End If
        Next

        Return ""
    End Function

    ' Überprüfen ob ein Datum in einem bestimmten Zeitraum liegt
    Public Function IsDatumInZeitraum(beginn As Date, ende As Date, pruefDatum As Date) As Boolean
        If pruefDatum >= beginn AndAlso pruefDatum <= ende Then
            Return True
        End If

        Return False
    End Function

    Public Sub DeleteUrlaub(dgvUrlaub As DataGridView, mitarbeiterId As Integer)
        Dim rowIndex As Integer = dgvUrlaub.SelectedCells.Item(0).RowIndex

        If dgvUrlaub.SelectedCells.Count = 0 OrElse rowIndex < 0 Then
            MessageBox.Show("Bitte Urlaub zuzm Löschen auswählen.", "Urlaub löschen", MessageBoxButtons.OK, MessageBoxIcon.Information)
            ' Kein Urlaubeintrag ausgewählt
            Return
        End If

        Dim urlaubId As Integer = dgvUrlaub.Rows.Item(rowIndex).Cells(3).Value

        If urlaubId <> -1 Then
            ' Nur wenn Urlaub eine ID > -1 wurde überhaupt in DB gespeichert und muss gelöscht werden
            Dim query As String = "DELETE FROM urlaub WHERE MitarbeiterID = " & mitarbeiterId & " AND ID = " & urlaubId

            Using mysql As New mysql_verbindung.clsMySQL
                mysql.ExecuteWithParametersAsDatareader(query)
            End Using
        End If

        dgvUrlaub.Rows.RemoveAt(rowIndex)
    End Sub

    Private Sub LadeFeiertage()
        feiertage = New List(Of Date)
        Dim query As String = "SELECT Datum FROM Feiertage"

        Using mysql As New mysql_verbindung.clsMySQL
            mysql.ExecuteWithParametersAsDatareader(query)

            While mysql.DataReader.Read()
                Dim datum As Date = CDate(mysql.DataReader("Datum"))
                feiertage.Add(datum)
            End While
        End Using
    End Sub

    Private Sub LadeKeineUrlaubsTage()
        keineUrlaubstage = New List(Of Integer)
        ' Mo: 0, Di: 1 usw.
        Dim query As String = "SELECT config FROM urlaubconfig"

        Using mysql As New mysql_verbindung.clsMySQL
            mysql.ExecuteWithParametersAsDatareader(query)

            If mysql.DataReader.Read() Then
                Dim config As String = mysql.DataReader("config")

                For i As Integer = 0 To 6
                    If config.Substring(i, 1) = "1" Then
                        ' Wochentag mit Index wird nicht als Urlaubstag gezählt
                        ' Index umrechnen, im DayOfWeek Enum ist Sonntag 0 und Mo: 1, Di: 2 usw.
                        Dim tagIndex As Integer = (i + 1) Mod 7
                        keineUrlaubstage.Add(tagIndex)
                    End If
                Next
            End If
        End Using
    End Sub

    ' Urlaube für MitarbeiterId aus Datenbank laden
    Private Function GetUrlaube(mitarbeiterId As Integer) As List(Of Urlaub)
        Dim urlaubList As New List(Of Urlaub)
        Dim query As String = "SELECT * FROM urlaub WHERE MitarbeiterID = " & mitarbeiterId
        Using mysql As New mysql_verbindung.clsMySQL
            mysql.ExecuteWithParametersAsDatareader(query)

            While mysql.DataReader.Read()
                Dim urlaubsBeginn As Date = mysql.DataReader("UrlaubsBeginn")
                Dim urlaubsEnde As Date = mysql.DataReader("UrlaubsEnde")
                Dim id As Integer = mysql.DataReader("ID")
                Dim anzahlTage As Integer = CalcUrlaubsTageFuerZeitraum(urlaubsBeginn, urlaubsEnde)

                Dim urlaub As New Urlaub(id, mitarbeiterId, urlaubsBeginn, urlaubsEnde, anzahlTage)
                urlaubList.Add(urlaub)
            End While
        End Using

        Return urlaubList
    End Function

    Public Function GetUrlaubeFromGrid(dgvUrlaub As DataGridView, mitarbeiterId As Integer) As List(Of Urlaub)
        Dim urlaubList As New List(Of Urlaub)

        For Each row As DataGridViewRow In dgvUrlaub.Rows
            Dim urlaubsBeginn As Date = row.Cells.Item(0).Value
            Dim urlaubsEnde As Date = row.Cells.Item(1).Value
            Dim anzahlTage As Integer = CalcUrlaubsTageFuerZeitraum(urlaubsBeginn, urlaubsEnde)
            Dim id As Integer = -1
            If Not String.IsNullOrWhiteSpace(row.Cells.Item(3)?.Value) Then
                id = row.Cells.Item(3).Value
            End If

            Dim urlaub As New Urlaub(id, mitarbeiterId, urlaubsBeginn, urlaubsEnde, anzahlTage)
            urlaubList.Add(urlaub)
        Next

        Return urlaubList
    End Function

    Private Sub AddUrlaubeInGridView(dgvUrlaub As DataGridView, urlaube As List(Of Urlaub))
        For Each urlaub As Urlaub In urlaube
            Dim beginn As String = urlaub.UrlaubsBeginn.ToLongDateString()
            Dim ende As String = urlaub.UrlaubsEnde.ToLongDateString()
            dgvUrlaub.Rows.Add(beginn, ende, urlaub.Tage, urlaub.ID)
        Next
    End Sub

    Public Function CalcUrlaubsTageFuerZeitraum(start As Date, ende As Date) As Integer
        Dim tage As Integer = 0
        ' Uhrzeiten von Datum zurücksetzen, da es Probleme mit Datediff gab
        Dim von As Date = New Date(start.Year, start.Month, start.Day)
        Dim bis As Date = New Date(ende.Year, ende.Month, ende.Day)
        Dim diff As Integer = DateDiff("d", von, bis)

        For i As Integer = 0 To diff Step 1
            Dim d As Date = von.AddDays(i)

            ' Wenn Feiertage oder andere Wochentage die nicht als Urlaub gelten im Urlaubszeitraum sind nicht als Urlaubstage zählen
            If feiertage.Contains(d) OrElse
                keineUrlaubstage.Contains(d.DayOfWeek) Then
                Continue For
            End If

            tage += 1
        Next

        Return tage
    End Function

    Public Sub SetFeiertage(feiertage As List(Of Date))
        Me.feiertage = feiertage
    End Sub

    ' 
    Public Sub SetKeineUrlaubstage(keineUrlaubstage As List(Of Integer))
        Me.keineUrlaubstage = keineUrlaubstage
    End Sub


End Class
