﻿Imports System.IO
Imports System.Text

' Mitarbeiterdaten in CSV Datei exportieren.
' Die Datei wird unter ..\Documents\Mitarbeiterdaten.csv gespeichert und kann z.B. mit Excel geöffnet werden.
Public Class DatenExportService

    Private exportPfad As String = ""
    Private fileName As String = ""
    Private ReadOnly trennZeichen As String = ";"

    Public Sub ExportMitarbeiterDaten(mitarbeiter As List(Of Mitarbeiter))
        If mitarbeiter.Count = 0 Then
            Return
        End If

        exportPfad = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)

        Dim zeitStempel As String = Now.ToString("dd.MM.yyyy__hh-mm-ss")
        If mitarbeiter.Count = 1 Then
            Dim ma As Mitarbeiter = mitarbeiter(0)
            ' Name + Zeitstempel als Dateiname
            fileName = ma.Nachname & ", " & ma.Vorname & "__" & zeitStempel & ".csv"
        Else
            ' Bei mehreren Mitarbeiter Zeitstempel als Dateiname
            fileName = "Mitarbeiterdaten_" & zeitStempel & ".csv"
        End If

        exportPfad &= "\" & fileName

        Dim fehler As Boolean = False
        Dim daten As New StringBuilder()
        daten.AppendLine(getUeberschriftzeile())

        For Each ma As Mitarbeiter In mitarbeiter
            daten.AppendLine(mitabeiterToCSVString(ma))
        Next

        Try
            File.WriteAllText(exportPfad, daten.ToString())
        Catch ex As Exception
            fehler = True
            MessageBox.Show("Es ist ein Fehler beim Export aufgetreten.", "Export", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        If Not fehler Then
            MessageBox.Show("Mitarbeiterdaten wurden erfolgreich nach: " & exportPfad & " exportiert.", "Export", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Function getUeberschriftzeile() As String
        Dim sb As New StringBuilder()
        sb.Append("Personalnr." & trennZeichen)
        sb.Append("Vorname" & trennZeichen)
        sb.Append("Nachname" & trennZeichen)
        sb.Append("Ort" & trennZeichen)
        sb.Append("Adresse" & trennZeichen)
        sb.Append("PLZ" & trennZeichen)
        sb.Append("ArbeitsBereich" & trennZeichen)
        sb.Append("Telefonnummer" & trennZeichen)
        sb.Append("Geburtsdatum" & trennZeichen)
        sb.Append("SteuerID" & trennZeichen)
        sb.Append("IBAN" & trennZeichen)
        sb.Append("ArbeitsEinstieg" & trennZeichen)
        sb.Append("ArbeitsEnde")

        Return sb.ToString()
    End Function

    Private Function mitabeiterToCSVString(mitarbeiter As Mitarbeiter) As String
        Dim sb As New StringBuilder()
        sb.Append(mitarbeiter.Personalnummer & trennZeichen)
        sb.Append(mitarbeiter.Vorname & trennZeichen)
        sb.Append(mitarbeiter.Nachname & trennZeichen)
        sb.Append(mitarbeiter.Ort & trennZeichen)
        sb.Append(mitarbeiter.Adresse & trennZeichen)
        sb.Append(mitarbeiter.PLZ & trennZeichen)
        sb.Append(mitarbeiter.ArbeitsBereich & trennZeichen)
        sb.Append(mitarbeiter.Telefonnummer & trennZeichen)
        sb.Append(mitarbeiter.Geburtsdatum.ToString("dd.MM.yyyy") & trennZeichen)
        sb.Append(mitarbeiter.SteuerID & trennZeichen)
        sb.Append(mitarbeiter.IBAN & trennZeichen)
        sb.Append(mitarbeiter.ArbeitsEinstieg.ToString("dd.MM.yyyy"))

        If Not mitarbeiter.ArbeitsEnde = Date.MinValue Then
            ' Mitarbeiter hat Austrittsdatum
            sb.Append(trennZeichen & mitarbeiter.ArbeitsEnde.ToString("dd.MM.yyyy"))
        End If

        Return sb.ToString()
    End Function

End Class
