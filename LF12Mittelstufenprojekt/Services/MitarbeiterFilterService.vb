﻿Public Class MitarbeiterFilterService

    Public Function Filter(arbeitsBereich As String, mitarbeiter As List(Of Mitarbeiter),
                                      dgvQualifikationenFilter As DataGridView) As List(Of Mitarbeiter)

        If Not String.IsNullOrWhiteSpace(arbeitsBereich) Then
            'Nur Mitarbbeiter sammeln, die im ausgewählten Arbeitsbereich sind       
            mitarbeiter = mitarbeiter.Where(Function(f) f.ArbeitsBereich = arbeitsBereich).ToList()
        End If

        Dim qualifFilter As QualifikationFilter = GetQualifikationenFilter(dgvQualifikationenFilter)
        Dim mitarbeiterQualifikationen As Dictionary(Of Integer, List(Of Integer)) = getMitarbeiterQulifikationen(mitarbeiter)

        For Each k As KeyValuePair(Of Integer, List(Of Integer)) In mitarbeiterQualifikationen
            Dim mitarbeiterId As Integer = k.Key
            Dim qualifikationenIds As List(Of Integer) = k.Value
            Dim removedMitarbeiter As Boolean = False

            For Each qualifikationId As Integer In qualifFilter.Vorhanden
                If Not qualifikationenIds.Contains(qualifikationId) Then
                    Dim m As Mitarbeiter = mitarbeiter.FirstOrDefault(Function(f) f.ID = mitarbeiterId)

                    If Not IsNothing(m) Then
                        mitarbeiter.Remove(m)
                        removedMitarbeiter = True
                        Exit For
                    End If
                End If
            Next

            If removedMitarbeiter Then
                Continue For
            End If

            For Each qualifikationId As Integer In qualifFilter.NichtVorhanden
                If qualifikationenIds.Contains(qualifikationId) Then
                    Dim m As Mitarbeiter = mitarbeiter.FirstOrDefault(Function(f) f.ID = mitarbeiterId)

                    If Not IsNothing(m) Then
                        mitarbeiter.Remove(m)
                        Exit For
                    End If
                End If
            Next
        Next

        Return mitarbeiter
    End Function

    Private Function GetQualifikationenFilter(dgvQualifikationenFilter As DataGridView) As QualifikationFilter
        Dim filter As New QualifikationFilter() With {
           .Vorhanden = New List(Of Integer),
           .NichtVorhanden = New List(Of Integer)
        }

        For Each row As DataGridViewRow In dgvQualifikationenFilter.Rows
            Dim qualifikationId As Integer = row.Cells.Item(0).Value

            ' Vorhanden angeklickt 
            If row.Cells.Item(2).Selected Then

                If IsNothing(row.Cells.Item(2).Value) OrElse row.Cells.Item(2).Value = False Then
                    row.Cells.Item(2).Value = True ' Vorhanden checken
                    row.Cells.Item(3).Value = False ' Nicht Vorhanden unchecken
                    filter.Vorhanden.Add(qualifikationId)
                ElseIf row.Cells.Item(2).Value = True Then ' wenn schon checked
                    row.Cells.Item(2).Value = False ' Vorhanden wieder unchecken
                End If

                row.Cells.Item(2).Selected = False
            Else
                If row.Cells.Item(2).Value = True AndAlso Not row.Cells.Item(3).Selected Then
                    ' Nicht mehr selektiert aber noch checked und es wurde nicht 'Nicht Vorhanden' angeklickt
                    filter.Vorhanden.Add(qualifikationId)
                End If
            End If

            If row.Cells.Item(3).Selected Then

                If IsNothing(row.Cells.Item(3).Value) OrElse row.Cells.Item(3).Value = False Then
                    row.Cells.Item(3).Value = True ' Nicht Vorhanden checken
                    row.Cells.Item(2).Value = False ' Vorhanden unchecken
                    filter.NichtVorhanden.Add(qualifikationId)
                ElseIf row.Cells.Item(3).Value = True Then ' Wenn schon checked
                    row.Cells.Item(3).Value = False ' Nicht Vorhanden wieder unchecken
                End If

                row.Cells.Item(3).Selected = False
            Else
                If row.Cells.Item(3).Value = True Then
                    ' Nicht mehr selektiert aber noch checked
                    filter.NichtVorhanden.Add(qualifikationId)
                End If
            End If
        Next

        Return filter
    End Function

    Private Function getMitarbeiterQulifikationen(mitarbeiter As List(Of Mitarbeiter)) As Dictionary(Of Integer, List(Of Integer))
        Dim qualifikationenListe As New List(Of MitarbeiterQualifikation)
        Dim query As String = "Select * from  mitarbeiterqualifikationen"

        Using mysql As New mysql_verbindung.clsMySQL
            mysql.ExecuteWithParametersAsDatareader(query)

            While mysql.DataReader.Read()
                Dim qualifikation As New MitarbeiterQualifikation() With {
                    .ID = mysql.DataReader("ID"),
                    .MitarbeiterID = mysql.DataReader("MitarbeiterID"),
                    .QualifikationID = mysql.DataReader("QualifikationenID")
                }

                If IsNothing(mitarbeiter.FirstOrDefault(Function(f) f.ID = qualifikation.MitarbeiterID)) Then
                    ' Mitarbeiter wurde schon aus Liste gefiltert (Arbeitsbereich stimmt nicht überein)
                    Continue While
                End If

                qualifikationenListe.Add(qualifikation)
            End While
        End Using

        Dim d As New Dictionary(Of Integer, List(Of Integer))
        For Each q As MitarbeiterQualifikation In qualifikationenListe
            If d.ContainsKey(q.MitarbeiterID) Then
                d(q.MitarbeiterID).Add(q.QualifikationID)
            Else
                d(q.MitarbeiterID) = New List(Of Integer) From {q.QualifikationID}
            End If
        Next

        For Each m As Mitarbeiter In mitarbeiter
            If Not d.ContainsKey(m.ID) Then
                ' Noch nicht hinzugefügte Mitarbeiter ohne Qualifikationen hinzufügen
                d.Add(m.ID, New List(Of Integer))
            End If
        Next

        Return d
    End Function

    Public Sub ResetFilter(cbArbeitsbereich As ComboBox, dgvQualifikationenFilter As DataGridView)
        cbArbeitsbereich.Text = ""

        ' Alle Filter Checkboxen leeren
        For Each row As DataGridViewRow In dgvQualifikationenFilter.Rows
            row.Cells.Item(2).Value = False
            row.Cells.Item(3).Value = False
        Next
    End Sub

End Class
