﻿Public Class MitarbeiterLoeschService

    ' Mitarbeiter anhand von ID aus allen Tabellen löschen.
    Public Function DeleteMitarbeiter(mitarbeiterID As Integer) As Boolean
        Dim q1 As String = "DELETE d 
                            FROM dokumente d
                            INNER JOIN mitarbeiterdokumente
                            ON d.ID = mitarbeiterdokumente.ID
                            WHERE mitarbeiterdokumente.MitarbeiterID = " & mitarbeiterID

        Dim q2 As String = "DELETE FROM mitarbeiterdokumente WHERE MitarbeiterID = " & mitarbeiterID

        Dim q3 As String = "DELETE FROM mitarbeiterqualifikationen WHERE MitarbeiterID = " & mitarbeiterID

        Dim q4 As String = "DELETE FROM urlaub WHERE MitarbeiterID = " & mitarbeiterID

        Dim q5 As String = "DELETE FROM mitarbeiter WHERE ID = " & mitarbeiterID

        Try
            ExecSql(q1)
            ExecSql(q2)
            ExecSql(q3)
            ExecSql(q4)
            ExecSql(q5)
        Catch ex As Exception
            Return False
        End Try

        Return True
    End Function

    Private Sub ExecSql(query As String)
        Using mysql As New mysql_verbindung.clsMySQL
            mysql.ExecuteWithParametersAsDatareader(query)
        End Using
    End Sub

End Class
