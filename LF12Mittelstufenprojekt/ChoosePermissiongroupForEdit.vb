﻿Public Class ChoosePermissiongroupForEdit
    Public tempArray() As String
    Public dgvCount As Integer
    Public dgv As New DataGridView
    Public dgv2 As New Object
    Private Sub ChoosePermissiongroupForEdit_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ComboBox1.Items.Clear()

        For i = 0 To dgvCount - 1
            If tempArray(i) <> "Admin" Then
                ComboBox1.Items.Add(tempArray(i))
            End If
        Next
        ComboBox1.SelectedIndex = 0
    End Sub

    Private Sub ButtonAbort_Click(sender As Object, e As EventArgs) Handles ButtonAbort.Click
        EditPermissiongroup.result = DialogResult.Cancel
        Close()
    End Sub

    Private Sub ButtonOK_Click(sender As Object, e As EventArgs) Handles ButtonOK.Click
        'EditArbeitsbereich = ComboBox1.SelectedIndex + 1
        Dim rows As Integer
        Dim myIndex As Integer
        'EditPermissiongroup.Close()
        'Dim newForm As New EditPermissiongroup
        'newForm.Show()
        myIndex = ComboBox1.SelectedIndex + 1
        'rows = MainForm.editPerm.DGVPermissiongroup.Rows.Count
        'MainForm.editPerm.DGVPermissiongroup.ClearSelection()
        'MainForm.editPerm.DGVPermissiongroup.Rows(myIndex).Selected = True 'rows = dgv.Rows.Count
        'rows = MainForm.editPerm.DGVPermissiongroup.CurrentRow.Index
        'Me.Visible = False
        'MainForm.editPerm.LoadPermissionEditor(New DataGridViewCellEventArgs(1, myIndex))

        MainForm.editPerm.LoadPermissionEditor(myIndex, True)
        Close()
    End Sub
End Class