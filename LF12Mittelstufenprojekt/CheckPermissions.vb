﻿Public Class CheckPermissions
    Public myBerechtigungsgruppe As New BerechtigungsGruppe()
    ''' <summary>
    ''' Dient der Ermittlung der berechtigungsgruppe für die Überprüfung der Rechte.
    ''' </summary>
    ''' <param name="WindowsUser">Der User der sich an diesem Programm angemeldet hat. Sollte aus loggedUser aus der Mainform gelesen werden.</param>
    ''' <returns>Berechtigungsgruppe</returns>
    Public Function WhatsMyPermissiongroup(WindowsUser As String) As BerechtigungsGruppe
        Dim query As String
        Dim mitarbeiter As Mitarbeiter = Nothing
        Dim parameter As New Dictionary(Of String, Object)
        query = "Select BerechtigungsGruppe from mitarbeiter Where WindowsBenutzer = @WinUser"
        parameter.Add("WinUser", WindowsUser)
        ' Benutzt die mysql_verbindung um eine Verbinden mit der Datenbank herzustellen und Daten auszulesen.
        Using mysql As New mysql_verbindung.clsMySQL
            mysql.ExecuteWithParametersAsDatareader(query, parameter)

            ' Liest alle Daten die benötigt werden aus der Mitarbeitertabelle aus der datenbank aus.
            While mysql.DataReader.Read()
                ' Ort und Personalnummer hier hinzufügen
                mitarbeiter = New Mitarbeiter() With {
                    .BerechtigungsGruppeID = mysql.DataReader("BerechtigungsGruppe")
                    }
            End While
        End Using
        If mitarbeiter Is Nothing Then
            MessageBox.Show("WindowsBenutzer '" + MainForm.loggedUser + "' wurde in der Datenbank nicht gefunden. Bitte mit einem anderen Benutzer anmelden.", "Anmeldung", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End
        End If

        Return CheckMyPermission(mitarbeiter.BerechtigungsGruppeID)
    End Function

    ''' <summary>
    ''' Prüft welche Berechtigungen der User mit dieser Berechtigungsgruppe besitzt
    ''' </summary>
    ''' <param name="permissiongroupID">ID übergeben die die Berechtigungsgruppe hat, wird automatisch übergeben</param>
    ''' <returns></returns>
    Public Function CheckMyPermission(permissiongroupID As Integer)
        Dim parameter As New Dictionary(Of String, Object)
        Dim query As String = "SELECT * FROM  berechtigungsgruppen WHERE ID = @ID"
        parameter.Add("@ID", permissiongroupID)
        Dim berechtigungsListe As New List(Of BerechtigungsGruppe)


        Using mysql As New mysql_verbindung.clsMySQL
            mysql.ExecuteWithParametersAsDatareader(query, parameter)

            If mysql.DataReader.Read() Then
                myBerechtigungsgruppe = New BerechtigungsGruppe With {
                    .ID = mysql.DataReader("ID"),
                    .Bezeichnung = mysql.DataReader("Bezeichnung"),
                    .EditEmployee = mysql.DataReader("MitarbeiterVerwalten"),
                    .EditFilter = mysql.DataReader("FilterVerwalten"),
                    .EditQualifications = mysql.DataReader("QualifikationenVerwalten"),
                    .EditWorkspaces = mysql.DataReader("ArbeitsbereicheVerwalten"),
                    .ExportEmployeeData = mysql.DataReader("MitarbeiterdatenExportieren"),
                    .ShowAllWorkspaces = mysql.DataReader("AlleArbeitsbereicheAnzeigen"),
                    .ShowAllEmployee = mysql.DataReader("AlleMitarbeiterAnzeigen"),
                    .ShowAllQualification = mysql.DataReader("AlleQualifikationenAnzeigen"),
                    .EditPermissiongroup = mysql.DataReader("BerechtigungenVerwalten"),
                    .EditHolidays = mysql.DataReader("UrlaubeVerwalten"),
                    .EditDocuments = mysql.DataReader("DokumenteVerwalten"),
                    .CreateEmployee = mysql.DataReader("MitarbeiterErstellen"),
                    .EditHolidayConfig = mysql.DataReader("UrlaubKonfigurieren"),
                    .ChangeAdminPW = mysql.DataReader("AdminPasswortAendern")
                }
            End If
        End Using

        Return myBerechtigungsgruppe
    End Function
End Class
