﻿Public Class EditQualifikation
    Private Sub AddFilter_Load() Handles MyBase.Load
        dgvQualifikationen.Rows.Clear()
        Dim query As String = "SELECT * FROM qualifikationen"
        Using mysql As New mysql_verbindung.clsMySQL
            mysql.ExecuteWithParametersAsDatareader(query)
            While mysql.DataReader.Read()
                dgvQualifikationen.Rows.Add(mysql.DataReader("ID"), mysql.DataReader("Bezeichnung"), "bearbeiten", "löschen")
            End While
        End Using
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Dim AddQualifikation As New AddQualifikation
        AddQualifikation.ShowDialog()
        Me.AddFilter_Load()
    End Sub

    Private Sub btnAbbrechen_Click(sender As Object, e As EventArgs) Handles btnAbbrechen.Click
        Me.Close()
    End Sub

    Private Sub dgvQualifikationen_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvQualifikationen.CellContentClick
        Select Case dgvQualifikationen.Columns(e.ColumnIndex).Name
            Case "bearbeiten"
                Dim EditQuali As New AddQualifikation
                EditQuali.QualifikationsID = dgvQualifikationen.Rows(e.RowIndex).Cells("ID").Value
                EditQuali.Text = "Qualifikation bearbeiten"
                EditQuali.ShowDialog()
                Me.AddFilter_Load()
            Case "löschen"
                Dim query As String = "Delete from qualifikationen Where ID = " & dgvQualifikationen.Rows(e.RowIndex).Cells("ID").Value
                Using mysql As New mysql_verbindung.clsMySQL
                    mysql.ExecuteWithParametersWithoutDatareader(query)
                End Using
                Me.AddFilter_Load()
                MsgBox("Erfolgreich gelöscht")
        End Select
    End Sub
End Class