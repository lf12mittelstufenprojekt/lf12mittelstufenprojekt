﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EditQualifikation
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dgvQualifikationen = New System.Windows.Forms.DataGridView()
        Me.ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Bezeichnung = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bearbeiten = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.löschen = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.btnAbbrechen = New System.Windows.Forms.Button()
        CType(Me.dgvQualifikationen, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvQualifikationen
        '
        Me.dgvQualifikationen.AllowUserToAddRows = False
        Me.dgvQualifikationen.AllowUserToDeleteRows = False
        Me.dgvQualifikationen.AllowUserToResizeRows = False
        Me.dgvQualifikationen.BackgroundColor = System.Drawing.Color.LightSkyBlue
        Me.dgvQualifikationen.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvQualifikationen.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ID, Me.Bezeichnung, Me.bearbeiten, Me.löschen})
        Me.dgvQualifikationen.Location = New System.Drawing.Point(12, 12)
        Me.dgvQualifikationen.Name = "dgvQualifikationen"
        Me.dgvQualifikationen.ShowCellErrors = False
        Me.dgvQualifikationen.ShowCellToolTips = False
        Me.dgvQualifikationen.ShowEditingIcon = False
        Me.dgvQualifikationen.ShowRowErrors = False
        Me.dgvQualifikationen.Size = New System.Drawing.Size(445, 354)
        Me.dgvQualifikationen.TabIndex = 0
        '
        'ID
        '
        Me.ID.HeaderText = "ID"
        Me.ID.Name = "ID"
        Me.ID.Visible = False
        '
        'Bezeichnung
        '
        Me.Bezeichnung.HeaderText = "Bezeichnung"
        Me.Bezeichnung.Name = "Bezeichnung"
        Me.Bezeichnung.Width = 200
        '
        'bearbeiten
        '
        Me.bearbeiten.HeaderText = "bearbeiten"
        Me.bearbeiten.Name = "bearbeiten"
        '
        'löschen
        '
        Me.löschen.HeaderText = "löschen"
        Me.löschen.Name = "löschen"
        '
        'btnAdd
        '
        Me.btnAdd.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.Blue
        Me.btnAdd.Location = New System.Drawing.Point(463, 12)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(95, 103)
        Me.btnAdd.TabIndex = 1
        Me.btnAdd.Text = "Hinzufügen"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'btnAbbrechen
        '
        Me.btnAbbrechen.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAbbrechen.ForeColor = System.Drawing.Color.Blue
        Me.btnAbbrechen.Location = New System.Drawing.Point(463, 325)
        Me.btnAbbrechen.Name = "btnAbbrechen"
        Me.btnAbbrechen.Size = New System.Drawing.Size(95, 41)
        Me.btnAbbrechen.TabIndex = 2
        Me.btnAbbrechen.Text = "Abbrechen"
        Me.btnAbbrechen.UseVisualStyleBackColor = True
        '
        'EditQualifikation
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(565, 371)
        Me.Controls.Add(Me.btnAbbrechen)
        Me.Controls.Add(Me.btnAdd)
        Me.Controls.Add(Me.dgvQualifikationen)
        Me.Name = "EditQualifikation"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Mitarbeiterqualifikationen hinzufügen"
        CType(Me.dgvQualifikationen, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents dgvQualifikationen As DataGridView
    Friend WithEvents ID As DataGridViewTextBoxColumn
    Friend WithEvents Bezeichnung As DataGridViewTextBoxColumn
    Friend WithEvents bearbeiten As DataGridViewButtonColumn
    Friend WithEvents löschen As DataGridViewButtonColumn
    Friend WithEvents btnAdd As Button
    Friend WithEvents btnAbbrechen As Button
End Class
