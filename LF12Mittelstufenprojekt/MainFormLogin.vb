﻿

Public Class MainFormLogin

    Private _loginService As LoginService
    Private fehlVersuche As Integer = 0

    Public Sub New(loginService As LoginService)
        InitializeComponent()
        _logInService = loginService
    End Sub

    Private Sub AnmeldeButton_Click(sender As Object, e As EventArgs) Handles BtnLogin.Click
        If _loginService.IstPasswortValid(TxtPassword.Text) Then
            MainFormFunctions.adminLoggedIn = True
            Close()
            MainForm.Show()
        Else
            fehlVersuche += 1
            If fehlVersuche <> 3 Then
                MessageBox.Show("Fehler, eingegebene Login Daten sind nicht korrekt!", "Login", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                MessageBox.Show("Das Passwort wurde mehrmals falsch eingegeben! Login wird für 5sek deaktiviert.", "Login", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ' Nach 3 Passwort Fehleingaben Login Button für 5sec deaktivieren.
                fehlVersuche = 0
                BtnLogin.Enabled = False
                Timer1.Start()
            End If
        End If
    End Sub

    Private Sub BtnCancel_Click(sender As Object, e As EventArgs) Handles BtnCancel.Click
        Close()
    End Sub

    Private Sub ChkShowPW_CheckedChanged(sender As Object, e As EventArgs) Handles ChkShowPW.CheckedChanged

        If ChkShowPW.CheckState = CheckState.Checked Then
            TxtPassword.UseSystemPasswordChar = False
        Else
            TxtPassword.UseSystemPasswordChar = True
        End If
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Timer1.Stop()
        ' Nach timeout von 5sec Login Button wieder aktivieren
        BtnLogin.Enabled = True
    End Sub

    Private Sub Login_Closing(sender As Object, e As FormClosingEventArgs) Handles Me.Closing
        If _loginService.IsLoggedIn() Then
            Return
        End If

        If MessageBox.Show("Programm wirklich beenden", "Beenden?", MessageBoxButtons.YesNo, MessageBoxIcon.Information) = Windows.Forms.DialogResult.No Then
            e.Cancel = True
            Return
        End If

        MainForm.Close()
    End Sub
End Class