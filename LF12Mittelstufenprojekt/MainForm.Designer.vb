﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
        Me.DGVMitarbeiter = New System.Windows.Forms.DataGridView()
        Me.GbxFilter = New System.Windows.Forms.GroupBox()
        Me.BtnResetFilter = New System.Windows.Forms.Button()
        Me.DGVQualifikationenFilter = New System.Windows.Forms.DataGridView()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbArbeitsbereich = New System.Windows.Forms.ComboBox()
        Me.MyMenuStrip = New System.Windows.Forms.MenuStrip()
        Me.MyStatusStrip = New System.Windows.Forms.StatusStrip()
        Me.lblSystemTime = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblDateData = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblUsername = New System.Windows.Forms.ToolStripStatusLabel()
        Me.PanelFunctions = New System.Windows.Forms.Panel()
        Me.PanelOptions = New System.Windows.Forms.Panel()
        Me.ButtonChangeAdminPW = New System.Windows.Forms.Button()
        Me.ButtonConfigHolidays = New System.Windows.Forms.Button()
        Me.ButtonOptions = New System.Windows.Forms.Button()
        Me.ButtonExportEmployees = New System.Windows.Forms.Button()
        Me.ButtonExit = New System.Windows.Forms.Button()
        Me.PanelPermissions = New System.Windows.Forms.Panel()
        Me.ButtonEditPermissions = New System.Windows.Forms.Button()
        Me.ButtonAddPermission = New System.Windows.Forms.Button()
        Me.ButtonShowPermissions = New System.Windows.Forms.Button()
        Me.ButtonPermissions = New System.Windows.Forms.Button()
        Me.PanelWorkspaces = New System.Windows.Forms.Panel()
        Me.ButtonAddWorkspace = New System.Windows.Forms.Button()
        Me.ButtonWorkspaces = New System.Windows.Forms.Button()
        Me.PanelQualification = New System.Windows.Forms.Panel()
        Me.ButtonAddQualification = New System.Windows.Forms.Button()
        Me.ButtonQualification = New System.Windows.Forms.Button()
        Me.PanelEmployee = New System.Windows.Forms.Panel()
        Me.ButtonEditEmployee = New System.Windows.Forms.Button()
        Me.ButtonAddEmployee = New System.Windows.Forms.Button()
        Me.ButtonEmployee = New System.Windows.Forms.Button()
        Me.PanelProgramIcon = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.MyToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.MyTimer = New System.Windows.Forms.Timer(Me.components)
        CType(Me.DGVMitarbeiter, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GbxFilter.SuspendLayout()
        CType(Me.DGVQualifikationenFilter, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MyStatusStrip.SuspendLayout()
        Me.PanelFunctions.SuspendLayout()
        Me.PanelOptions.SuspendLayout()
        Me.PanelPermissions.SuspendLayout()
        Me.PanelWorkspaces.SuspendLayout()
        Me.PanelQualification.SuspendLayout()
        Me.PanelEmployee.SuspendLayout()
        Me.PanelProgramIcon.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DGVMitarbeiter
        '
        Me.DGVMitarbeiter.AllowUserToAddRows = False
        Me.DGVMitarbeiter.AllowUserToDeleteRows = False
        Me.DGVMitarbeiter.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DGVMitarbeiter.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader
        Me.DGVMitarbeiter.BackgroundColor = System.Drawing.Color.LightSkyBlue
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGVMitarbeiter.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.DGVMitarbeiter.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVMitarbeiter.Location = New System.Drawing.Point(206, 316)
        Me.DGVMitarbeiter.MultiSelect = False
        Me.DGVMitarbeiter.Name = "DGVMitarbeiter"
        Me.DGVMitarbeiter.ReadOnly = True
        Me.DGVMitarbeiter.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGVMitarbeiter.Size = New System.Drawing.Size(1074, 352)
        Me.DGVMitarbeiter.TabIndex = 1
        Me.MyToolTip.SetToolTip(Me.DGVMitarbeiter, "Übersicht der Mitarbeiter" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Zum Bearbeiten von Mitarbeiterdaten, doppelklick auf" &
        " eine Zeile" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10))
        '
        'GbxFilter
        '
        Me.GbxFilter.Controls.Add(Me.BtnResetFilter)
        Me.GbxFilter.Controls.Add(Me.DGVQualifikationenFilter)
        Me.GbxFilter.Controls.Add(Me.Label1)
        Me.GbxFilter.Controls.Add(Me.cbArbeitsbereich)
        Me.GbxFilter.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GbxFilter.Location = New System.Drawing.Point(205, 31)
        Me.GbxFilter.Name = "GbxFilter"
        Me.GbxFilter.Size = New System.Drawing.Size(542, 237)
        Me.GbxFilter.TabIndex = 2
        Me.GbxFilter.TabStop = False
        Me.GbxFilter.Text = "Filterung"
        '
        'BtnResetFilter
        '
        Me.BtnResetFilter.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnResetFilter.ForeColor = System.Drawing.Color.Blue
        Me.BtnResetFilter.Location = New System.Drawing.Point(189, 52)
        Me.BtnResetFilter.Name = "BtnResetFilter"
        Me.BtnResetFilter.Size = New System.Drawing.Size(75, 23)
        Me.BtnResetFilter.TabIndex = 7
        Me.BtnResetFilter.Text = "Reset"
        Me.BtnResetFilter.UseVisualStyleBackColor = True
        '
        'DGVQualifikationenFilter
        '
        Me.DGVQualifikationenFilter.AllowUserToAddRows = False
        Me.DGVQualifikationenFilter.AllowUserToDeleteRows = False
        Me.DGVQualifikationenFilter.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DGVQualifikationenFilter.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DGVQualifikationenFilter.BackgroundColor = System.Drawing.Color.LightSkyBlue
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGVQualifikationenFilter.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.DGVQualifikationenFilter.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVQualifikationenFilter.Location = New System.Drawing.Point(6, 81)
        Me.DGVQualifikationenFilter.MultiSelect = False
        Me.DGVQualifikationenFilter.Name = "DGVQualifikationenFilter"
        Me.DGVQualifikationenFilter.ReadOnly = True
        Me.DGVQualifikationenFilter.Size = New System.Drawing.Size(530, 150)
        Me.DGVQualifikationenFilter.TabIndex = 0
        Me.MyToolTip.SetToolTip(Me.DGVQualifikationenFilter, "Filterung nach Qualifikationen")
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 34)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(111, 15)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Arbeitsbereiche:"
        '
        'cbArbeitsbereich
        '
        Me.cbArbeitsbereich.BackColor = System.Drawing.Color.CornflowerBlue
        Me.cbArbeitsbereich.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbArbeitsbereich.ForeColor = System.Drawing.Color.White
        Me.cbArbeitsbereich.FormattingEnabled = True
        Me.cbArbeitsbereich.Location = New System.Drawing.Point(6, 52)
        Me.cbArbeitsbereich.Name = "cbArbeitsbereich"
        Me.cbArbeitsbereich.Size = New System.Drawing.Size(165, 23)
        Me.cbArbeitsbereich.TabIndex = 4
        Me.MyToolTip.SetToolTip(Me.cbArbeitsbereich, "Filterung nach Arbeitsbereiche")
        '
        'MyMenuStrip
        '
        Me.MyMenuStrip.BackColor = System.Drawing.Color.CornflowerBlue
        Me.MyMenuStrip.Location = New System.Drawing.Point(0, 0)
        Me.MyMenuStrip.Name = "MyMenuStrip"
        Me.MyMenuStrip.Size = New System.Drawing.Size(1292, 24)
        Me.MyMenuStrip.TabIndex = 10
        Me.MyMenuStrip.Text = "MenuStrip1"
        '
        'MyStatusStrip
        '
        Me.MyStatusStrip.BackColor = System.Drawing.Color.CornflowerBlue
        Me.MyStatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.lblSystemTime, Me.lblDateData, Me.lblUsername})
        Me.MyStatusStrip.Location = New System.Drawing.Point(0, 671)
        Me.MyStatusStrip.Name = "MyStatusStrip"
        Me.MyStatusStrip.Size = New System.Drawing.Size(1292, 24)
        Me.MyStatusStrip.TabIndex = 11
        Me.MyStatusStrip.Text = "StatusStrip1"
        '
        'lblSystemTime
        '
        Me.lblSystemTime.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSystemTime.Name = "lblSystemTime"
        Me.lblSystemTime.Size = New System.Drawing.Size(49, 19)
        Me.lblSystemTime.Text = "Uhrzeit"
        Me.lblSystemTime.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        '
        'lblDateData
        '
        Me.lblDateData.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right
        Me.lblDateData.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDateData.Name = "lblDateData"
        Me.lblDateData.Size = New System.Drawing.Size(49, 19)
        Me.lblDateData.Text = "Datum"
        '
        'lblUsername
        '
        Me.lblUsername.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUsername.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblUsername.Name = "lblUsername"
        Me.lblUsername.Size = New System.Drawing.Size(1179, 19)
        Me.lblUsername.Spring = True
        Me.lblUsername.Text = "User:"
        Me.lblUsername.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'PanelFunctions
        '
        Me.PanelFunctions.BackColor = System.Drawing.Color.CornflowerBlue
        Me.PanelFunctions.Controls.Add(Me.PanelOptions)
        Me.PanelFunctions.Controls.Add(Me.ButtonOptions)
        Me.PanelFunctions.Controls.Add(Me.ButtonExportEmployees)
        Me.PanelFunctions.Controls.Add(Me.ButtonExit)
        Me.PanelFunctions.Controls.Add(Me.PanelPermissions)
        Me.PanelFunctions.Controls.Add(Me.ButtonPermissions)
        Me.PanelFunctions.Controls.Add(Me.PanelWorkspaces)
        Me.PanelFunctions.Controls.Add(Me.ButtonWorkspaces)
        Me.PanelFunctions.Controls.Add(Me.PanelQualification)
        Me.PanelFunctions.Controls.Add(Me.ButtonQualification)
        Me.PanelFunctions.Controls.Add(Me.PanelEmployee)
        Me.PanelFunctions.Controls.Add(Me.ButtonEmployee)
        Me.PanelFunctions.Controls.Add(Me.PanelProgramIcon)
        Me.PanelFunctions.Dock = System.Windows.Forms.DockStyle.Left
        Me.PanelFunctions.Location = New System.Drawing.Point(0, 24)
        Me.PanelFunctions.Name = "PanelFunctions"
        Me.PanelFunctions.Size = New System.Drawing.Size(199, 647)
        Me.PanelFunctions.TabIndex = 12
        '
        'PanelOptions
        '
        Me.PanelOptions.BackColor = System.Drawing.Color.LightSteelBlue
        Me.PanelOptions.Controls.Add(Me.ButtonChangeAdminPW)
        Me.PanelOptions.Controls.Add(Me.ButtonConfigHolidays)
        Me.PanelOptions.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanelOptions.Location = New System.Drawing.Point(0, 469)
        Me.PanelOptions.Name = "PanelOptions"
        Me.PanelOptions.Size = New System.Drawing.Size(199, 88)
        Me.PanelOptions.TabIndex = 12
        Me.PanelOptions.Visible = False
        '
        'ButtonChangeAdminPW
        '
        Me.ButtonChangeAdminPW.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.ButtonChangeAdminPW.Dock = System.Windows.Forms.DockStyle.Top
        Me.ButtonChangeAdminPW.FlatAppearance.BorderSize = 0
        Me.ButtonChangeAdminPW.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Blue
        Me.ButtonChangeAdminPW.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cyan
        Me.ButtonChangeAdminPW.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonChangeAdminPW.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonChangeAdminPW.ForeColor = System.Drawing.Color.Black
        Me.ButtonChangeAdminPW.Location = New System.Drawing.Point(0, 40)
        Me.ButtonChangeAdminPW.Name = "ButtonChangeAdminPW"
        Me.ButtonChangeAdminPW.Padding = New System.Windows.Forms.Padding(35, 0, 0, 0)
        Me.ButtonChangeAdminPW.Size = New System.Drawing.Size(199, 40)
        Me.ButtonChangeAdminPW.TabIndex = 4
        Me.ButtonChangeAdminPW.Text = "Adminpasswort ändern"
        Me.ButtonChangeAdminPW.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MyToolTip.SetToolTip(Me.ButtonChangeAdminPW, "Qualifikationen neu Anlegen oder bearbeiten")
        Me.ButtonChangeAdminPW.UseVisualStyleBackColor = False
        '
        'ButtonConfigHolidays
        '
        Me.ButtonConfigHolidays.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.ButtonConfigHolidays.Dock = System.Windows.Forms.DockStyle.Top
        Me.ButtonConfigHolidays.FlatAppearance.BorderSize = 0
        Me.ButtonConfigHolidays.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Blue
        Me.ButtonConfigHolidays.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cyan
        Me.ButtonConfigHolidays.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonConfigHolidays.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonConfigHolidays.ForeColor = System.Drawing.Color.Black
        Me.ButtonConfigHolidays.Location = New System.Drawing.Point(0, 0)
        Me.ButtonConfigHolidays.Name = "ButtonConfigHolidays"
        Me.ButtonConfigHolidays.Padding = New System.Windows.Forms.Padding(35, 0, 0, 0)
        Me.ButtonConfigHolidays.Size = New System.Drawing.Size(199, 40)
        Me.ButtonConfigHolidays.TabIndex = 3
        Me.ButtonConfigHolidays.Text = "Urlaubskonfiguration"
        Me.ButtonConfigHolidays.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MyToolTip.SetToolTip(Me.ButtonConfigHolidays, "Qualifikationen neu Anlegen oder bearbeiten")
        Me.ButtonConfigHolidays.UseVisualStyleBackColor = False
        '
        'ButtonOptions
        '
        Me.ButtonOptions.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.ButtonOptions.FlatAppearance.BorderSize = 0
        Me.ButtonOptions.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Blue
        Me.ButtonOptions.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Blue
        Me.ButtonOptions.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonOptions.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonOptions.ForeColor = System.Drawing.Color.White
        Me.ButtonOptions.Location = New System.Drawing.Point(0, 557)
        Me.ButtonOptions.Name = "ButtonOptions"
        Me.ButtonOptions.Padding = New System.Windows.Forms.Padding(10, 0, 0, 0)
        Me.ButtonOptions.Size = New System.Drawing.Size(199, 45)
        Me.ButtonOptions.TabIndex = 11
        Me.ButtonOptions.Text = "Einstellungen"
        Me.ButtonOptions.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MyToolTip.SetToolTip(Me.ButtonOptions, "Öffnet die Einstellungen")
        Me.ButtonOptions.UseVisualStyleBackColor = True
        '
        'ButtonExportEmployees
        '
        Me.ButtonExportEmployees.Dock = System.Windows.Forms.DockStyle.Top
        Me.ButtonExportEmployees.FlatAppearance.BorderSize = 0
        Me.ButtonExportEmployees.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Blue
        Me.ButtonExportEmployees.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Blue
        Me.ButtonExportEmployees.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonExportEmployees.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonExportEmployees.ForeColor = System.Drawing.Color.White
        Me.ButtonExportEmployees.Location = New System.Drawing.Point(0, 592)
        Me.ButtonExportEmployees.Name = "ButtonExportEmployees"
        Me.ButtonExportEmployees.Padding = New System.Windows.Forms.Padding(10, 0, 0, 0)
        Me.ButtonExportEmployees.Size = New System.Drawing.Size(199, 45)
        Me.ButtonExportEmployees.TabIndex = 10
        Me.ButtonExportEmployees.Text = "Mitarbeiterdaten exportieren"
        Me.ButtonExportEmployees.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MyToolTip.SetToolTip(Me.ButtonExportEmployees, "Exportiert die Daten der ausgewählten Mitarbeiter")
        Me.ButtonExportEmployees.UseVisualStyleBackColor = True
        '
        'ButtonExit
        '
        Me.ButtonExit.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.ButtonExit.FlatAppearance.BorderSize = 0
        Me.ButtonExit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Blue
        Me.ButtonExit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Blue
        Me.ButtonExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonExit.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonExit.ForeColor = System.Drawing.Color.White
        Me.ButtonExit.Location = New System.Drawing.Point(0, 602)
        Me.ButtonExit.Name = "ButtonExit"
        Me.ButtonExit.Padding = New System.Windows.Forms.Padding(10, 0, 0, 0)
        Me.ButtonExit.Size = New System.Drawing.Size(199, 45)
        Me.ButtonExit.TabIndex = 9
        Me.ButtonExit.Text = "Schließen"
        Me.ButtonExit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MyToolTip.SetToolTip(Me.ButtonExit, "Beendet das Programm")
        Me.ButtonExit.UseVisualStyleBackColor = True
        '
        'PanelPermissions
        '
        Me.PanelPermissions.BackColor = System.Drawing.Color.LightSteelBlue
        Me.PanelPermissions.Controls.Add(Me.ButtonEditPermissions)
        Me.PanelPermissions.Controls.Add(Me.ButtonAddPermission)
        Me.PanelPermissions.Controls.Add(Me.ButtonShowPermissions)
        Me.PanelPermissions.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelPermissions.Location = New System.Drawing.Point(0, 464)
        Me.PanelPermissions.Name = "PanelPermissions"
        Me.PanelPermissions.Size = New System.Drawing.Size(199, 128)
        Me.PanelPermissions.TabIndex = 8
        Me.PanelPermissions.Visible = False
        '
        'ButtonEditPermissions
        '
        Me.ButtonEditPermissions.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.ButtonEditPermissions.Dock = System.Windows.Forms.DockStyle.Top
        Me.ButtonEditPermissions.FlatAppearance.BorderSize = 0
        Me.ButtonEditPermissions.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Blue
        Me.ButtonEditPermissions.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cyan
        Me.ButtonEditPermissions.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonEditPermissions.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonEditPermissions.ForeColor = System.Drawing.Color.Black
        Me.ButtonEditPermissions.Location = New System.Drawing.Point(0, 80)
        Me.ButtonEditPermissions.Name = "ButtonEditPermissions"
        Me.ButtonEditPermissions.Padding = New System.Windows.Forms.Padding(35, 0, 0, 0)
        Me.ButtonEditPermissions.Size = New System.Drawing.Size(199, 40)
        Me.ButtonEditPermissions.TabIndex = 8
        Me.ButtonEditPermissions.Text = "Bearbeiten"
        Me.ButtonEditPermissions.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MyToolTip.SetToolTip(Me.ButtonEditPermissions, "Neue Berechtigungsgruppe erstellen")
        Me.ButtonEditPermissions.UseVisualStyleBackColor = False
        '
        'ButtonAddPermission
        '
        Me.ButtonAddPermission.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.ButtonAddPermission.Dock = System.Windows.Forms.DockStyle.Top
        Me.ButtonAddPermission.FlatAppearance.BorderSize = 0
        Me.ButtonAddPermission.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Blue
        Me.ButtonAddPermission.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cyan
        Me.ButtonAddPermission.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonAddPermission.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonAddPermission.ForeColor = System.Drawing.Color.Black
        Me.ButtonAddPermission.Location = New System.Drawing.Point(0, 40)
        Me.ButtonAddPermission.Name = "ButtonAddPermission"
        Me.ButtonAddPermission.Padding = New System.Windows.Forms.Padding(35, 0, 0, 0)
        Me.ButtonAddPermission.Size = New System.Drawing.Size(199, 40)
        Me.ButtonAddPermission.TabIndex = 7
        Me.ButtonAddPermission.Text = "Hinzufügen"
        Me.ButtonAddPermission.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MyToolTip.SetToolTip(Me.ButtonAddPermission, "Neue Berechtigungsgruppe erstellen")
        Me.ButtonAddPermission.UseVisualStyleBackColor = False
        '
        'ButtonShowPermissions
        '
        Me.ButtonShowPermissions.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.ButtonShowPermissions.Dock = System.Windows.Forms.DockStyle.Top
        Me.ButtonShowPermissions.FlatAppearance.BorderSize = 0
        Me.ButtonShowPermissions.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Blue
        Me.ButtonShowPermissions.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cyan
        Me.ButtonShowPermissions.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonShowPermissions.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonShowPermissions.ForeColor = System.Drawing.Color.Black
        Me.ButtonShowPermissions.Location = New System.Drawing.Point(0, 0)
        Me.ButtonShowPermissions.Name = "ButtonShowPermissions"
        Me.ButtonShowPermissions.Padding = New System.Windows.Forms.Padding(35, 0, 0, 0)
        Me.ButtonShowPermissions.Size = New System.Drawing.Size(199, 40)
        Me.ButtonShowPermissions.TabIndex = 5
        Me.ButtonShowPermissions.Text = "Anzeigen"
        Me.ButtonShowPermissions.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MyToolTip.SetToolTip(Me.ButtonShowPermissions, "Berechtigungsgruppen öffnen")
        Me.ButtonShowPermissions.UseVisualStyleBackColor = False
        '
        'ButtonPermissions
        '
        Me.ButtonPermissions.Dock = System.Windows.Forms.DockStyle.Top
        Me.ButtonPermissions.FlatAppearance.BorderSize = 0
        Me.ButtonPermissions.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Blue
        Me.ButtonPermissions.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Blue
        Me.ButtonPermissions.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonPermissions.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonPermissions.ForeColor = System.Drawing.Color.White
        Me.ButtonPermissions.Location = New System.Drawing.Point(0, 419)
        Me.ButtonPermissions.Name = "ButtonPermissions"
        Me.ButtonPermissions.Padding = New System.Windows.Forms.Padding(10, 0, 0, 0)
        Me.ButtonPermissions.Size = New System.Drawing.Size(199, 45)
        Me.ButtonPermissions.TabIndex = 7
        Me.ButtonPermissions.Text = "Berechtigungen"
        Me.ButtonPermissions.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MyToolTip.SetToolTip(Me.ButtonPermissions, "Öffnet das Berechtigungsgruppenmenü")
        Me.ButtonPermissions.UseVisualStyleBackColor = True
        '
        'PanelWorkspaces
        '
        Me.PanelWorkspaces.BackColor = System.Drawing.Color.LightSteelBlue
        Me.PanelWorkspaces.Controls.Add(Me.ButtonAddWorkspace)
        Me.PanelWorkspaces.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelWorkspaces.Location = New System.Drawing.Point(0, 371)
        Me.PanelWorkspaces.Name = "PanelWorkspaces"
        Me.PanelWorkspaces.Size = New System.Drawing.Size(199, 48)
        Me.PanelWorkspaces.TabIndex = 6
        Me.PanelWorkspaces.Visible = False
        '
        'ButtonAddWorkspace
        '
        Me.ButtonAddWorkspace.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.ButtonAddWorkspace.Dock = System.Windows.Forms.DockStyle.Top
        Me.ButtonAddWorkspace.FlatAppearance.BorderSize = 0
        Me.ButtonAddWorkspace.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Blue
        Me.ButtonAddWorkspace.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cyan
        Me.ButtonAddWorkspace.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonAddWorkspace.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonAddWorkspace.ForeColor = System.Drawing.Color.Black
        Me.ButtonAddWorkspace.Location = New System.Drawing.Point(0, 0)
        Me.ButtonAddWorkspace.Name = "ButtonAddWorkspace"
        Me.ButtonAddWorkspace.Padding = New System.Windows.Forms.Padding(35, 0, 0, 0)
        Me.ButtonAddWorkspace.Size = New System.Drawing.Size(199, 40)
        Me.ButtonAddWorkspace.TabIndex = 2
        Me.ButtonAddWorkspace.Text = "Hinzufügen/ Bearbeiten"
        Me.ButtonAddWorkspace.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MyToolTip.SetToolTip(Me.ButtonAddWorkspace, "Arbeitsbereiche neu Anlegen oder bearbeiten")
        Me.ButtonAddWorkspace.UseVisualStyleBackColor = False
        '
        'ButtonWorkspaces
        '
        Me.ButtonWorkspaces.Dock = System.Windows.Forms.DockStyle.Top
        Me.ButtonWorkspaces.FlatAppearance.BorderSize = 0
        Me.ButtonWorkspaces.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Blue
        Me.ButtonWorkspaces.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Blue
        Me.ButtonWorkspaces.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonWorkspaces.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonWorkspaces.ForeColor = System.Drawing.Color.White
        Me.ButtonWorkspaces.Location = New System.Drawing.Point(0, 326)
        Me.ButtonWorkspaces.Name = "ButtonWorkspaces"
        Me.ButtonWorkspaces.Padding = New System.Windows.Forms.Padding(10, 0, 0, 0)
        Me.ButtonWorkspaces.Size = New System.Drawing.Size(199, 45)
        Me.ButtonWorkspaces.TabIndex = 5
        Me.ButtonWorkspaces.Text = "Arbeitsbereiche"
        Me.ButtonWorkspaces.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MyToolTip.SetToolTip(Me.ButtonWorkspaces, "Öffnet das Arbeitsbereichmenü")
        Me.ButtonWorkspaces.UseVisualStyleBackColor = True
        '
        'PanelQualification
        '
        Me.PanelQualification.BackColor = System.Drawing.Color.LightSteelBlue
        Me.PanelQualification.Controls.Add(Me.ButtonAddQualification)
        Me.PanelQualification.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelQualification.Location = New System.Drawing.Point(0, 278)
        Me.PanelQualification.Name = "PanelQualification"
        Me.PanelQualification.Size = New System.Drawing.Size(199, 48)
        Me.PanelQualification.TabIndex = 4
        Me.PanelQualification.Visible = False
        '
        'ButtonAddQualification
        '
        Me.ButtonAddQualification.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.ButtonAddQualification.Dock = System.Windows.Forms.DockStyle.Top
        Me.ButtonAddQualification.FlatAppearance.BorderSize = 0
        Me.ButtonAddQualification.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Blue
        Me.ButtonAddQualification.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cyan
        Me.ButtonAddQualification.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonAddQualification.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonAddQualification.ForeColor = System.Drawing.Color.Black
        Me.ButtonAddQualification.Location = New System.Drawing.Point(0, 0)
        Me.ButtonAddQualification.Name = "ButtonAddQualification"
        Me.ButtonAddQualification.Padding = New System.Windows.Forms.Padding(35, 0, 0, 0)
        Me.ButtonAddQualification.Size = New System.Drawing.Size(199, 40)
        Me.ButtonAddQualification.TabIndex = 2
        Me.ButtonAddQualification.Text = "Hinzufügen/ Bearbeiten"
        Me.ButtonAddQualification.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MyToolTip.SetToolTip(Me.ButtonAddQualification, "Qualifikationen neu Anlegen oder bearbeiten")
        Me.ButtonAddQualification.UseVisualStyleBackColor = False
        '
        'ButtonQualification
        '
        Me.ButtonQualification.Dock = System.Windows.Forms.DockStyle.Top
        Me.ButtonQualification.FlatAppearance.BorderSize = 0
        Me.ButtonQualification.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Blue
        Me.ButtonQualification.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Blue
        Me.ButtonQualification.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonQualification.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonQualification.ForeColor = System.Drawing.Color.White
        Me.ButtonQualification.Location = New System.Drawing.Point(0, 233)
        Me.ButtonQualification.Name = "ButtonQualification"
        Me.ButtonQualification.Padding = New System.Windows.Forms.Padding(10, 0, 0, 0)
        Me.ButtonQualification.Size = New System.Drawing.Size(199, 45)
        Me.ButtonQualification.TabIndex = 3
        Me.ButtonQualification.Text = "Qualifikationen"
        Me.ButtonQualification.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MyToolTip.SetToolTip(Me.ButtonQualification, "Öffnet das Qualifikationsmenü")
        Me.ButtonQualification.UseVisualStyleBackColor = True
        '
        'PanelEmployee
        '
        Me.PanelEmployee.BackColor = System.Drawing.Color.LightSteelBlue
        Me.PanelEmployee.Controls.Add(Me.ButtonEditEmployee)
        Me.PanelEmployee.Controls.Add(Me.ButtonAddEmployee)
        Me.PanelEmployee.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelEmployee.Location = New System.Drawing.Point(0, 145)
        Me.PanelEmployee.Name = "PanelEmployee"
        Me.PanelEmployee.Size = New System.Drawing.Size(199, 88)
        Me.PanelEmployee.TabIndex = 2
        Me.PanelEmployee.Visible = False
        '
        'ButtonEditEmployee
        '
        Me.ButtonEditEmployee.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.ButtonEditEmployee.Dock = System.Windows.Forms.DockStyle.Top
        Me.ButtonEditEmployee.FlatAppearance.BorderSize = 0
        Me.ButtonEditEmployee.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Blue
        Me.ButtonEditEmployee.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cyan
        Me.ButtonEditEmployee.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonEditEmployee.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonEditEmployee.ForeColor = System.Drawing.Color.Black
        Me.ButtonEditEmployee.Location = New System.Drawing.Point(0, 40)
        Me.ButtonEditEmployee.Name = "ButtonEditEmployee"
        Me.ButtonEditEmployee.Padding = New System.Windows.Forms.Padding(35, 0, 0, 0)
        Me.ButtonEditEmployee.Size = New System.Drawing.Size(199, 40)
        Me.ButtonEditEmployee.TabIndex = 1
        Me.ButtonEditEmployee.Text = "Bearbeiten"
        Me.ButtonEditEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MyToolTip.SetToolTip(Me.ButtonEditEmployee, "Vorhandenen Mitarbeiter bearbeiten")
        Me.ButtonEditEmployee.UseVisualStyleBackColor = False
        '
        'ButtonAddEmployee
        '
        Me.ButtonAddEmployee.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.ButtonAddEmployee.Dock = System.Windows.Forms.DockStyle.Top
        Me.ButtonAddEmployee.FlatAppearance.BorderSize = 0
        Me.ButtonAddEmployee.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Blue
        Me.ButtonAddEmployee.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cyan
        Me.ButtonAddEmployee.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonAddEmployee.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonAddEmployee.ForeColor = System.Drawing.Color.Black
        Me.ButtonAddEmployee.Location = New System.Drawing.Point(0, 0)
        Me.ButtonAddEmployee.Name = "ButtonAddEmployee"
        Me.ButtonAddEmployee.Padding = New System.Windows.Forms.Padding(35, 0, 0, 0)
        Me.ButtonAddEmployee.Size = New System.Drawing.Size(199, 40)
        Me.ButtonAddEmployee.TabIndex = 0
        Me.ButtonAddEmployee.Text = "Anlegen"
        Me.ButtonAddEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MyToolTip.SetToolTip(Me.ButtonAddEmployee, "Neuen Mitarbeiter anlegen")
        Me.ButtonAddEmployee.UseVisualStyleBackColor = False
        '
        'ButtonEmployee
        '
        Me.ButtonEmployee.Dock = System.Windows.Forms.DockStyle.Top
        Me.ButtonEmployee.FlatAppearance.BorderSize = 0
        Me.ButtonEmployee.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Blue
        Me.ButtonEmployee.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Blue
        Me.ButtonEmployee.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonEmployee.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonEmployee.ForeColor = System.Drawing.Color.White
        Me.ButtonEmployee.Location = New System.Drawing.Point(0, 100)
        Me.ButtonEmployee.Name = "ButtonEmployee"
        Me.ButtonEmployee.Padding = New System.Windows.Forms.Padding(10, 0, 0, 0)
        Me.ButtonEmployee.Size = New System.Drawing.Size(199, 45)
        Me.ButtonEmployee.TabIndex = 1
        Me.ButtonEmployee.Text = "Mitarbeiter"
        Me.ButtonEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MyToolTip.SetToolTip(Me.ButtonEmployee, "Öffnet das Mitarbeitermenü")
        Me.ButtonEmployee.UseVisualStyleBackColor = True
        '
        'PanelProgramIcon
        '
        Me.PanelProgramIcon.Controls.Add(Me.Label2)
        Me.PanelProgramIcon.Controls.Add(Me.PictureBox1)
        Me.PanelProgramIcon.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelProgramIcon.Location = New System.Drawing.Point(0, 0)
        Me.PanelProgramIcon.Name = "PanelProgramIcon"
        Me.PanelProgramIcon.Size = New System.Drawing.Size(199, 100)
        Me.PanelProgramIcon.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(0, 71)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(199, 29)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "MVS Ultimate"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.CornflowerBlue
        Me.PictureBox1.Dock = System.Windows.Forms.DockStyle.Top
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(199, 68)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(206, 298)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(140, 15)
        Me.Label3.TabIndex = 13
        Me.Label3.Text = "Mitarbeiterübersicht:"
        '
        'MyToolTip
        '
        Me.MyToolTip.AutomaticDelay = 0
        Me.MyToolTip.AutoPopDelay = 5000
        Me.MyToolTip.InitialDelay = 0
        Me.MyToolTip.ReshowDelay = 0
        Me.MyToolTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info
        Me.MyToolTip.ToolTipTitle = "Information:"
        '
        'MyTimer
        '
        Me.MyTimer.Interval = 1000
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1292, 695)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.PanelFunctions)
        Me.Controls.Add(Me.MyStatusStrip)
        Me.Controls.Add(Me.GbxFilter)
        Me.Controls.Add(Me.DGVMitarbeiter)
        Me.Controls.Add(Me.MyMenuStrip)
        Me.MainMenuStrip = Me.MyMenuStrip
        Me.MinimumSize = New System.Drawing.Size(1308, 734)
        Me.Name = "MainForm"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "MVS Ultimate"
        CType(Me.DGVMitarbeiter, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GbxFilter.ResumeLayout(False)
        Me.GbxFilter.PerformLayout()
        CType(Me.DGVQualifikationenFilter, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MyStatusStrip.ResumeLayout(False)
        Me.MyStatusStrip.PerformLayout()
        Me.PanelFunctions.ResumeLayout(False)
        Me.PanelOptions.ResumeLayout(False)
        Me.PanelPermissions.ResumeLayout(False)
        Me.PanelWorkspaces.ResumeLayout(False)
        Me.PanelQualification.ResumeLayout(False)
        Me.PanelEmployee.ResumeLayout(False)
        Me.PanelProgramIcon.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DGVMitarbeiter As DataGridView
    Friend WithEvents GbxFilter As GroupBox
    Friend WithEvents DGVQualifikationenFilter As DataGridView
    Friend WithEvents cbArbeitsbereich As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents MyMenuStrip As MenuStrip
    Friend WithEvents MyStatusStrip As StatusStrip
    Friend WithEvents lblSystemTime As ToolStripStatusLabel
    Friend WithEvents lblUsername As ToolStripStatusLabel
    Friend WithEvents PanelFunctions As Panel
    Friend WithEvents PanelPermissions As Panel
    Friend WithEvents ButtonPermissions As Button
    Friend WithEvents PanelWorkspaces As Panel
    Friend WithEvents ButtonWorkspaces As Button
    Friend WithEvents PanelQualification As Panel
    Friend WithEvents ButtonQualification As Button
    Friend WithEvents PanelEmployee As Panel
    Friend WithEvents ButtonEmployee As Button
    Friend WithEvents PanelProgramIcon As Panel
    Friend WithEvents ButtonAddWorkspace As Button
    Friend WithEvents ButtonAddQualification As Button
    Friend WithEvents ButtonEditEmployee As Button
    Friend WithEvents ButtonAddEmployee As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents ButtonExit As Button
    Friend WithEvents Label3 As Label
    Friend WithEvents ButtonAddPermission As Button
    Friend WithEvents ButtonShowPermissions As Button
    Friend WithEvents MyToolTip As ToolTip
    Friend WithEvents ButtonEditPermissions As Button
    Friend WithEvents BtnResetFilter As Button
    Friend WithEvents ButtonOptions As Button
    Friend WithEvents ButtonExportEmployees As Button
    Friend WithEvents PanelOptions As Panel
    Friend WithEvents ButtonChangeAdminPW As Button
    Friend WithEvents ButtonConfigHolidays As Button
    Friend WithEvents lblDateData As ToolStripStatusLabel
    Friend WithEvents MyTimer As Timer
End Class
