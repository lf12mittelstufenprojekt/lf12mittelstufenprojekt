﻿Imports System.ComponentModel

Public Class AddEmployee
    Public MitarbeiterID
    Public Add As Boolean
    Public id As Integer
    Public mode As Integer
    Dim result As DialogResult
    Dim query As String = ""
    Dim mitarbeiter As Mitarbeiter = Nothing
    Private berechtigungsGruppen As List(Of BerechtigungsGruppe)
    Private arbeitsBereiche As List(Of Arbeitsbereich)
    Private urlaubService As New UrlaubService()
    Private mitarbeiterValidation As New MitarbeiterValidationService()
    Private fehlerBoxen As New List(Of Rectangle)

    Private Sub AddEmployee_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ' Arbeitsbereiche und Berechtigungen müssen vor Employee geladen werden, damit bei Bearbeiten Arbeitsbereich und Berechtigungsgruppe richtig in Combobox gesetzt werden
        LoadArbeitsbereiche()
        LoadBerechtigungsGruppen()

        LoadEmployees(mode)
        LoadQualifications()
        LoadDokumente(mode)
        SetQualifacation()
        LadeUrlaub()
        DGVQualification.Columns("ID").Visible = False
    End Sub

    ''' <summary>
    ''' Lädt die Mitarbeiterdaten des ausgewählten Datensatzes oder stellt ein leeres Formblatt zur verfügung um einen neuen Mitarbeiter zu erstellen.
    ''' </summary>
    ''' <param name="setMode">Modus der anzugeben ist, ob es sich um den Bearbeitungsmodus oder um den Erstellungsmodus handelt. 1=bearbeiten 2=erstellen</param>
    Private Sub LoadEmployees(setMode As Integer)

        If setMode = 1 Then
            query = "SELECT m.*, a.Bezeichnung AS ArbeitsbereichsBezeichnung, b.Bezeichnung AS BerechtigungsBezeichnung FROM mitarbeiter m
                     LEFT JOIN arbeitsbereich a
                     ON m.ArbeitsbereichID = a.ID
                     LEFT JOIN berechtigungsgruppen b
                     ON m.BerechtigungsGruppe = b.ID Where m.ID = " + id.ToString
            ' Benutzt die mysql_verbindung um eine Verbinden mit der Datenbank herzustellen und Daten auszulesen.
            Using mysql As New mysql_verbindung.clsMySQL
                mysql.ExecuteWithParametersAsDatareader(query)

                ' Liest alle Daten die benötigt werden aus der Mitarbeitertabelle aus der datenbank aus.
                While mysql.DataReader.Read()
                    ' Ort und Personalnummer hier hinzufügen
                    mitarbeiter = New Mitarbeiter() With {
                        .ID = mysql.DataReader("ID"),
                        .Vorname = mysql.DataReader("Vorname"),
                        .Nachname = mysql.DataReader("Nachname"),
                        .Adresse = mysql.DataReader("Adresse"),
                        .PLZ = mysql.DataReader("PLZ"),
                        .Telefonnummer = mysql.DataReader("Telefonnummer"),
                        .Geburtsdatum = mysql.DataReader("Geburtsdatum"),
                        .SteuerID = mysql.DataReader("SteuerID"),
                        .IBAN = mysql.DataReader("IBAN"),
                        .ArbeitsEinstieg = mysql.DataReader("ArbeitsEinstieg"),
                        .ArbeitsEnde = mysql.DataReader("ArbeitsEnde"),
                        .WindowsBenutzer = mysql.DataReader("WindowsBenutzer"),
                        .BerechtigungsGruppeID = mysql.DataReader("BerechtigungsGruppe"),
                        .ArbeitsbereichID = mysql.DataReader("ArbeitsbereichID"),
                        .ArbeitsBereich = mysql.DataReader("ArbeitsbereichsBezeichnung"),
                        .BerechtigungsGruppe = mysql.DataReader("BerechtigungsBezeichnung"),
                        .Personalnummer = mysql.DataReader("Personalnummer"),
                        .Ort = mysql.DataReader("Ort")
                    }
                End While

                If mitarbeiter.BerechtigungsGruppe <> "Admin" AndAlso BerechtigungsGruppeCombobox.Items.Contains("Admin") Then
                    ' Wenn ein Mitarbeiter kein Admin ist, kann nicht die Admin Berechtigung auswählen
                    BerechtigungsGruppeCombobox.Items.Remove("Admin")
                End If

                ' Weist die daten die aus der datenbank gelesen wurden den Textboxen zu.
                'TextBoxID.Text = Personalnummer
                TextBoxFirstname.Text = mitarbeiter.Vorname
                TextBoxLastname.Text = mitarbeiter.Nachname
                DTPBirthday.Value = mitarbeiter.Geburtsdatum
                BerechtigungsGruppeCombobox.Text = mitarbeiter.BerechtigungsGruppe
                TextBoxAddress.Text = mitarbeiter.Adresse
                TextBoxLocation.Text = mitarbeiter.Ort
                TextBoxPostcode.Text = mitarbeiter.PLZ
                TextBoxPhone.Text = mitarbeiter.Telefonnummer
                TextBoxTaxID.Text = mitarbeiter.SteuerID
                ArbeitsbereichCombobox.Text = mitarbeiter.ArbeitsBereich
                TextBoxIBAN.Text = mitarbeiter.IBAN
                DTPStartOfWork.Value = mitarbeiter.ArbeitsEinstieg
                DTPEndOfWork.Value = mitarbeiter.ArbeitsEnde
                TextBoxWindowsUser.Text = mitarbeiter.WindowsBenutzer
                TextBoxPersonalnumber.Text = mitarbeiter.Personalnummer

            End Using
        ElseIf setMode = 2 Then
            TextBoxFirstname.Text = ""
            TextBoxLastname.Text = ""
            DTPBirthday.Value = Date.Now
            BerechtigungsGruppeCombobox.Text = ""
            TextBoxAddress.Text = ""
            TextBoxLocation.Text = ""
            TextBoxPostcode.Text = ""
            TextBoxPhone.Text = ""
            TextBoxPersonalnumber.Text = ""
            TextBoxTaxID.Text = ""
            ArbeitsbereichCombobox.Text = ""
            TextBoxIBAN.Text = ""
            DTPStartOfWork.Value = Date.Now
            DTPEndOfWork.Value = Date.Now
            TextBoxWindowsUser.Text = ""
        End If
    End Sub

    Private Sub LoadDokumente(setMode As Integer)

        If setMode = 1 Then
            DGVDocuments.Rows.Clear()
            Dim params As New Dictionary(Of String, Object)
            params.Add("@MitarbeiterID", id)
            Dim query As String = "SELECT * FROM dokumente WHERE ID IN(SELECT DokumentID FROM mitarbeiterdokumente WHERE MitarbeiterID = @MitarbeiterID)"
            Using mysql As New mysql_verbindung.clsMySQL
                mysql.ExecuteWithParametersAsDatareader(query, params)
                While mysql.DataReader.Read()
                    Dim Datum As Date = mysql.DataReader("Datum")
                    DGVDocuments.Rows.Add(mysql.DataReader("ID"), mysql.DataReader("Bezeichnung"), Datum.ToShortDateString, "öffnen", "löschen")
                End While
            End Using

        End If
    End Sub

    Private Sub ButtonAbort_Click(sender As Object, e As EventArgs) Handles ButtonAbort.Click
        result = MessageBox.Show("Beim abbrechen werden nicht gespeicherte Stände verworfen.", "Datamangementserver", MessageBoxButtons.YesNo, MessageBoxIcon.Warning)
        If result = DialogResult.Yes Then
            Close()
        End If
    End Sub

    Private Sub ButtonSave_Click(sender As Object, e As EventArgs) Handles ButtonSave.Click
        result = MessageBox.Show("Sie sind dabei diesen Datensatz zu speichern, fortfahren?", "Daten Speichern", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)

        If result = DialogResult.Yes Then
            Dim hasError As Boolean = CheckDaten()
            'saveClicked = True

            If hasError Then
                Refresh() ' Form Updaten, fehlerhate EIngaben werden ro umrandet
            End If

            If hasError Then
                ' Es gab Fehler, nicht speichern und Meldung zeigen
                MessageBox.Show("Es gibt fehlerhafte Eingaben! Bitte Eingaben korrigieren.", "Datenüberprüfung", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return
            End If
        End If

        SaveDataInDB()
    End Sub

    Private Sub SaveDataInDB()
        Dim query As String = ""
        Dim parameterEmployee As New Dictionary(Of String, Object)

        If mode = 1 Then ' Bearbeiten 
            ' Speichert die Daten der Textbox in die Datenbank
            query = "UPDATE mitarbeiter SET Vorname = @Vorname, Nachname= @Nachname, Adresse = @Adresse, PLZ = @PLZ, Ort = @Ort,Telefonnummer = @Telefonnummer, Geburtsdatum = @Geburtsdatum, SteuerID = @SteuerID, IBAN = @IBAN, ArbeitsEinstieg = @ArbeitsEinstieg, ArbeitsEnde = @ArbeitsEnde, WindowsBenutzer = @WindowsBenutzer, BerechtigungsGruppe = @BerechtigungsGruppe, ArbeitsbereichID = @ArbeitsbereichID, Personalnummer = @Personalnummer WHERE ID = @ID"
        ElseIf mode = 2 Then ' Hinzufügen
            query = "INSERT INTO mitarbeiter (Vorname, Nachname, Adresse, PLZ, Ort, Telefonnummer, Geburtsdatum, SteuerID, IBAN, ArbeitsEinstieg, ArbeitsEnde, WindowsBenutzer, BerechtigungsGruppe, ArbeitsbereichID, Personalnummer) VALUES (@Vorname, @Nachname, @Adresse, @PLZ, @Ort, @Telefonnummer, @Geburtsdatum, @SteuerID, @IBAN, @ArbeitsEinstieg, @ArbeitsEnde, @WindowsBenutzer, @BerechtigungsGruppe, @ArbeitsbereichID, @Personalnummer)"
        End If

        parameterEmployee.Add("ID", id.ToString)
        parameterEmployee.Add("Vorname", TextBoxFirstname.Text)
        parameterEmployee.Add("Nachname", TextBoxLastname.Text)
        parameterEmployee.Add("Adresse", TextBoxAddress.Text)
        parameterEmployee.Add("PLZ", TextBoxPostcode.Text)
        parameterEmployee.Add("Telefonnummer", TextBoxPhone.Text)
        parameterEmployee.Add("Geburtsdatum", DTPBirthday.Value.ToString("yyyy-MM-dd"))
        parameterEmployee.Add("SteuerID", TextBoxTaxID.Text)
        parameterEmployee.Add("IBAN", TextBoxIBAN.Text)
        parameterEmployee.Add("ArbeitsEinstieg", DTPStartOfWork.Value.ToString("yyyy-MM-dd"))
        parameterEmployee.Add("ArbeitsEnde", DTPEndOfWork.Value.ToString("yyyy-MM-dd"))
        parameterEmployee.Add("WindowsBenutzer", TextBoxWindowsUser.Text)
        parameterEmployee.Add("Personalnummer", TextBoxPersonalnumber.Text)
        Dim gruppeID As Integer = berechtigungsGruppen.Find(Function(f) f.Bezeichnung = BerechtigungsGruppeCombobox.Text).ID
        Dim arbeitsbereichID As Integer = arbeitsBereiche.Find(Function(f) f.Bezeichnung = ArbeitsbereichCombobox.Text).ID
        parameterEmployee.Add("BerechtigungsGruppe", gruppeID)
        parameterEmployee.Add("ArbeitsbereichID", arbeitsbereichID)
        parameterEmployee.Add("Ort", TextBoxLocation.Text)

        Using mysql As New mysql_verbindung.clsMySQL
            mysql.ExecuteInsertWithParameter(query, parameterEmployee)
        End Using


        'Speichert die Daten der Qualifikation in die Datenbank
        Dim myState As Boolean

        ' Wenn neuer Mitarbeiter angelegt werden, dann ermittle die letzte ID aus der DB
        If mode = 2 Then
            id = GetMaxEmployeeID()
        End If

        For i = 0 To DGVQualification.Rows.Count - 1
            Dim qualiID As Integer = CInt(DGVQualification.Rows(i).Cells(0).Value) ' Aus unsichtbarer ID-Spalte ID abrufen
            ' Wenn checkbox ist checked, dann Qualifikation anlegen
            If DGVQualification.Item(2, i).Value = True Then
                ' Prüfen ob eintrag bereits existiert
                query = "SELECT * FROM mitarbeiterqualifikationen WHERE MitarbeiterID = @MitarbeiterID AND QualifikationenID = @QualifikationenID"
                Dim parameterSelectQuali As New Dictionary(Of String, Object)
                parameterSelectQuali.Add("MitarbeiterID", id.ToString)
                parameterSelectQuali.Add("QualifikationenID", qualiID)
                Using mysql As New mysql_verbindung.clsMySQL
                    mysql.ExecuteWithParametersAsDatareader(query, parameterSelectQuali)
                    myState = mysql.DataReader.Read()
                End Using

                ' Wenn der eintrag nicht existiert dann erzeugen
                If myState <> True Then
                    Dim parameterInsertQuali As New Dictionary(Of String, Object)
                    query = "INSERT INTO mitarbeiterqualifikationen (MitarbeiterID, QualifikationenID) VALUES(@MitarbeiterID, @QualifikationenID)"
                    parameterInsertQuali.Add("MitarbeiterID", id.ToString)
                    parameterInsertQuali.Add("QualifikationenID", qualiID)
                    Using mysql As New mysql_verbindung.clsMySQL
                        mysql.ExecuteWithParametersAsDatareader(query, parameterInsertQuali)
                    End Using
                End If

            Else ' Wenn checkbox ist unchecked, dann Qualifikation löschen wenn vorhanden
                Dim parameterDeleteQuali As New Dictionary(Of String, Object)
                query = "DELETE FROM mitarbeiterqualifikationen WHERE MitarbeiterID = @MitarbeiterID AND QualifikationenID = @QualifikationenID"
                parameterDeleteQuali.Add("MitarbeiterID", id.ToString)
                parameterDeleteQuali.Add("QualifikationenID", qualiID)
                Using mysql As New mysql_verbindung.clsMySQL
                    mysql.ExecuteWithParametersAsDatareader(query, parameterDeleteQuali)
                End Using
            End If
        Next

        'Speichert den Urlaub aus der Tabelle in die Datenbank
        urlaubService.SpeicherUrlaube(DGVHolidays, id)
        Close()
    End Sub

    Private Sub LoadQualifications()
        Dim qualifikationen As List(Of Qualifikation) = LoadAllQualifications()
        DGVQualification.DataSource = qualifikationen

        Dim vorhanden As New DataGridViewCheckBoxColumn With {
            .HeaderText = "Vorhanden",
            .Width = 100
        }

        Dim nichtVorhanden As New DataGridViewCheckBoxColumn With {
            .HeaderText = "Nicht Vorhanden",
            .Width = 100
        }

        DGVQualification.Columns.Add(vorhanden)
        DGVQualification.Columns.Add(nichtVorhanden)
    End Sub

    Private Function LoadAllQualifications() As List(Of Qualifikation)
        query = "Select * from qualifikationen"
        Dim qualifikationen As New List(Of Qualifikation)

        Using mysql As New mysql_verbindung.clsMySQL
            mysql.ExecuteWithParametersAsDatareader(query)

            While mysql.DataReader.Read()
                Dim qualifikation As New Qualifikation() With {
                    .ID = mysql.DataReader("ID"),
                    .Bezeichnung = mysql.DataReader("Bezeichnung")
                }
                qualifikationen.Add(qualifikation)
            End While
        End Using

        Return qualifikationen
    End Function

    Private Sub SetQualifacation()
        query = "SELECT QualifikationenID FROM mitarbeiterqualifikationen WHERE MitarbeiterID = " + id.ToString
        Dim qualifikationen As New List(Of MitarbeiterQualifikation)

        Using mysql As New mysql_verbindung.clsMySQL
            mysql.ExecuteWithParametersAsDatareader(query)

            While mysql.DataReader.Read()
                Dim employeeQualification As New MitarbeiterQualifikation() With {
                    .QualifikationID = mysql.DataReader("QualifikationenID")
                }

                For i = 0 To DGVQualification.Rows.Count - 1
                    If DGVQualification.Item(0, i).Value = employeeQualification.QualifikationID Then
                        DGVQualification.Item(2, i).Value = True
                    End If

                Next i
            End While

            For i = 0 To DGVQualification.Rows.Count - 1
                If DGVQualification.Item(2, i).Value = False Then
                    DGVQualification.Item(3, i).Value = True
                End If
            Next i
        End Using
    End Sub

    Private Sub DGVQualification_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGVQualification.CellContentClick
        If e.RowIndex = -1 Then Exit Sub
        If e.ColumnIndex = 2 Then
            If DGVQualification.Item(2, e.RowIndex).Value = True And DGVQualification.Item(3, e.RowIndex).Value = False Then
                DGVQualification.Item(2, e.RowIndex).Value = False
            ElseIf DGVQualification.Item(2, e.RowIndex).Value = False And DGVQualification.Item(3, e.RowIndex).Value = False Then
                DGVQualification.Item(2, e.RowIndex).Value = True
            ElseIf DGVQualification.Item(2, e.RowIndex).Value = False And DGVQualification.Item(3, e.RowIndex).Value = True Then
                DGVQualification.Item(2, e.RowIndex).Value = True
                DGVQualification.Item(3, e.RowIndex).Value = False
            End If
        ElseIf e.ColumnIndex = 3 Then
            If DGVQualification.Item(3, e.RowIndex).Value = True And DGVQualification.Item(2, e.RowIndex).Value = False Then
                DGVQualification.Item(3, e.RowIndex).Value = False
            ElseIf DGVQualification.Item(3, e.RowIndex).Value = False And DGVQualification.Item(2, e.RowIndex).Value = False Then
                DGVQualification.Item(3, e.RowIndex).Value = True
            ElseIf DGVQualification.Item(3, e.RowIndex).Value = False And DGVQualification.Item(2, e.RowIndex).Value = True Then
                DGVQualification.Item(3, e.RowIndex).Value = True
                DGVQualification.Item(2, e.RowIndex).Value = False
            End If
        End If
    End Sub

    Private Sub ButtonAddHolidays_Click(sender As Object, e As EventArgs) Handles ButtonAddHolidays.Click
        If MainForm.Perm.EditHolidays <> "Y" Then
            MessageBox.Show("Sie besitzen nicht die benötigten Berechtigungen für diese Aktion.", "Info zu Berechtigungen", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        Dim tage As Integer = urlaubService.AddUrlaub(DGVHolidays, id, DTPHolidaysForm.Value, DTPHolidaysTo.Value)
        Dim vorherigeGesamtTage As Integer = 0
        If TextBoxHolidaysTotalDays.Text <> "" Then
            vorherigeGesamtTage = CInt(TextBoxHolidaysTotalDays.Text)
        End If

        TextBoxHolidaysTotalDays.Text = CStr(vorherigeGesamtTage + tage)
    End Sub

    Private Sub DGVHolidays_RowsRemoved(sender As Object, e As DataGridViewRowsRemovedEventArgs) Handles DGVHolidays.RowsRemoved
        TextBoxHolidaysTotalDays.Text = urlaubService.GetTotalUrlaubstage(DGVHolidays, id)
    End Sub

    Private Sub LadeUrlaub()
        If mode = 1 Then
            ' Nur bei Mitarbeiter bearbeiten Urlaube laden
            Dim urlaube As List(Of Urlaub) = urlaubService.LadeUrlaube(DGVHolidays, id)
            TextBoxHolidaysTotalDays.Text = urlaubService.GetTotalUrlaubstage(urlaube)
        End If
    End Sub

    Private Sub ButtonUrlaubDelete_Click(sender As Object, e As EventArgs) Handles BtnDeleteUrlaub.Click
        If MainForm.Perm.EditHolidays <> "Y" Then
            MessageBox.Show("Sie besitzen nicht die benötigten Berechtigungen für diese Aktion.", "Info zu Berechtigungen", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If
        urlaubService.DeleteUrlaub(DGVHolidays, id)
    End Sub

    Private Sub DGVDocuments_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGVDocuments.CellContentClick
        If e.RowIndex = -1 Then Exit Sub
        Select Case DGVDocuments.Columns(e.ColumnIndex).Name
            Case "öffnen"
                Dokument.DateiOeffnen(DGVDocuments.Rows(e.RowIndex).Cells("DokuID").Value)
                Me.LoadDokumente(mode)
            Case "löschen"
                Dim params As New Dictionary(Of String, Object)
                params.Add("@ID", DGVDocuments.Rows(e.RowIndex).Cells("DokuID").Value)
                Dim query As String = "Delete from dokumente Where ID = @ID;Delete from mitarbeiterdokumente where DokumentID = @ID"
                Using mysql As New mysql_verbindung.clsMySQL
                    mysql.ExecuteWithParametersWithoutDatareader(query, params)
                End Using
                Me.LoadDokumente(mode)
                MsgBox("Erfolgreich gelöscht")
        End Select
    End Sub

    Private Sub btnAddDokument_Click(sender As Object, e As EventArgs) Handles btnAddDokument.Click
        If MainForm.Perm.EditDocuments <> "Y" Then
            MessageBox.Show("Sie besitzen nicht die benötigten Berechtigungen für diese Aktion.", "Info zu Berechtigungen", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If
        Dim DokumentHochladen As New AddDocument
        DokumentHochladen.MitarbeiterID = id
        DokumentHochladen.ShowDialog()
        LoadDokumente(mode)
    End Sub

    Private Sub BtnMADelete_Click(sender As Object, e As EventArgs) Handles BtnMaDelete.Click
        Dim result As DialogResult = MessageBox.Show("Mitarbeiter wirklich löschen?", "Mitarbeiter löschen", MessageBoxButtons.YesNo, MessageBoxIcon.Warning)

        If result = DialogResult.Yes Then
            Dim mitarbeiterLoeschService As New MitarbeiterLoeschService()
            Dim erfolgreich As Boolean = mitarbeiterLoeschService.DeleteMitarbeiter(id)

            If erfolgreich Then
                ' Mitarbeiter wurde erfolgreich aus Datenbank gelöscht
                MessageBox.Show("Mitarbeiter erfolgreich gelöscht.", "Mitarbeiter löschen", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
                'MainForm.ReloadMitarbeiter()
                Close()
            End If
        End If
    End Sub

    Private Sub LoadArbeitsbereiche()
        arbeitsBereiche = New List(Of Arbeitsbereich)

        Dim query As String = "Select ID, Bezeichnung from arbeitsbereich"
        Using mysql As New mysql_verbindung.clsMySQL()
            mysql.ExecuteWithParametersAsDatareader(query)
            While mysql.DataReader.Read
                Dim arbeitsBereich As New Arbeitsbereich() With {
                  .ID = mysql.DataReader("ID"),
                  .Bezeichnung = mysql.DataReader("Bezeichnung")
                }

                arbeitsBereiche.Add(arbeitsBereich)
                ArbeitsbereichCombobox.Items.Add(arbeitsBereich.Bezeichnung)
            End While
        End Using
    End Sub

    Private Sub LoadBerechtigungsGruppen()
        berechtigungsGruppen = New List(Of BerechtigungsGruppe)

        Dim query As String = "Select ID, Bezeichnung from berechtigungsgruppen"
        Using mysql As New mysql_verbindung.clsMySQL()
            mysql.ExecuteWithParametersAsDatareader(query)
            While mysql.DataReader.Read
                Dim berechtigung As New BerechtigungsGruppe() With {
                  .ID = mysql.DataReader("ID"),
                  .Bezeichnung = mysql.DataReader("Bezeichnung")
                }

                berechtigungsGruppen.Add(berechtigung)
                BerechtigungsGruppeCombobox.Items.Add(berechtigung.Bezeichnung)
            End While
        End Using
    End Sub

    Private Function GetMaxEmployeeID() As Integer
        Dim query As String = "SELECT MAX(ID) AS MaxID FROM mitarbeiter"
        Dim myState As Boolean
        Dim MaxID As Integer

        Using mysql As New mysql_verbindung.clsMySQL
            mysql.ExecuteWithParametersAsDatareader(query)
            myState = mysql.DataReader.Read()

            If myState = True Then
                MaxID = mysql.DataReader.GetInt64("MaxID")
                Return MaxID
            End If
        End Using

        Return -1
    End Function

    Private Function CheckDaten() As Boolean
        ' Wenn es in irgendeinem Textfeld einen Fehler gab, Textfeldborder rot markieren und nicht speichern
        ' Datumswerte brauchen wegen Date Picker nicht geprüft werden und sind beschränkt von 01.01.1900 - 31.12.2099

        fehlerBoxen = New List(Of Rectangle)
        If Not mitarbeiterValidation.BasisCheck(TextBoxFirstname.Text) Then fehlerBoxen.Add(GetRectangle(TextBoxFirstname))
        If Not mitarbeiterValidation.BasisCheck(TextBoxLastname.Text) Then fehlerBoxen.Add(GetRectangle(TextBoxLastname))
        If Not mitarbeiterValidation.BasisCheck(TextBoxAddress.Text) Then fehlerBoxen.Add(GetRectangle(TextBoxAddress))
        If Not mitarbeiterValidation.CheckPLZ(TextBoxPostcode.Text) Then fehlerBoxen.Add(GetRectangle(TextBoxPostcode))
        If Not mitarbeiterValidation.BasisCheck(TextBoxLocation.Text) Then fehlerBoxen.Add(GetRectangle(TextBoxLocation))
        If Not mitarbeiterValidation.BasisCheck(TextBoxPhone.Text) Then fehlerBoxen.Add(GetRectangle(TextBoxPhone))
        If Not mitarbeiterValidation.BasisCheck(TextBoxTaxID.Text) Then fehlerBoxen.Add(GetRectangle(TextBoxTaxID))
        If Not mitarbeiterValidation.BasisCheck(TextBoxIBAN.Text) Then fehlerBoxen.Add(GetRectangle(TextBoxIBAN))
        If Not mitarbeiterValidation.BasisCheck(TextBoxWindowsUser.Text) Then fehlerBoxen.Add(GetRectangle(TextBoxWindowsUser))
        If Not mitarbeiterValidation.BasisCheck(BerechtigungsGruppeCombobox.Text) Then fehlerBoxen.Add(GetRectangle(BerechtigungsGruppeCombobox))
        If Not mitarbeiterValidation.BasisCheck(ArbeitsbereichCombobox.Text) Then fehlerBoxen.Add(GetRectangle(ArbeitsbereichCombobox))
        If Not mitarbeiterValidation.CheckPersonalNr(TextBoxPersonalnumber.Text) Then fehlerBoxen.Add(GetRectangle(TextBoxPersonalnumber))

        ' Gibt es Fehler?
        Return fehlerBoxen.Count > 0
    End Function

    Private Function GetRectangle(box As Object) As Rectangle
        If TypeOf box Is TextBox Then
            box = DirectCast(box, TextBox)
        ElseIf TypeOf box Is ComboBox Then
            box = DirectCast(box, ComboBox)
        End If

        Dim rectangle As New Rectangle(box.Location.X - 1, box.Location.Y - 1, box.Width + 1, box.Height + 1)
        Return rectangle
    End Function

    Private Sub markErrors(graphics As Graphics)
        ' Alle Boxen mit fehlerhaftem Inhalt row umranden
        For Each r As Rectangle In fehlerBoxen
            Dim pen As New Pen(Color.Red)
            pen.Width = 1
            graphics.DrawRectangle(pen, r)
        Next
    End Sub

    Private Sub AddEmployee_Paint(sender As Object, e As PaintEventArgs) Handles Me.Paint
        markErrors(e.Graphics)
    End Sub
End Class