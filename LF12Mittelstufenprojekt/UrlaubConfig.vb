﻿
Public Class UrlaubConfig

    Private validationService As New MitarbeiterValidationService()
    Private editedRows As HashSet(Of Integer)

    Private Sub UrlaubCOnfig_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        editedRows = New HashSet(Of Integer)
        LadeFeiertage()
        LadeUrlaubWochentage()
    End Sub

    Private Sub BtnSpeichern_Click(sender As Object, e As EventArgs) Handles BtnSpeichern.Click
        SpeicherAenderungen()
    End Sub

    Private Sub DGVFeiertageOnCellValueChanged(sender As Object, e As DataGridViewCellEventArgs) Handles DGVFeiertage.CellValueChanged
        If e.RowIndex <> -1 Then
            editedRows.Add(e.RowIndex)
        End If
    End Sub

    ' Zum Löschen muss eine Zeile selektiert werden und entf. gedrückt werden.
    ' Erst nach Bestätigung aus Datenbank löschen
    Private Sub DGVFeiertageOnUserDeletingRow(sender As Object, e As DataGridViewRowCancelEventArgs) Handles DGVFeiertage.UserDeletingRow
        Dim result As DialogResult = MessageBox.Show("Feiertag wirklich entfernen?", "UrlaubKonfig", MessageBoxButtons.YesNo, MessageBoxIcon.Warning)

        If Not result = DialogResult.Yes Then
            e.Cancel = True
            Return
        End If

        Dim row As DataGridViewRow = e.Row
        Dim id As Integer = If(row.Cells.Item(2).Value Is Nothing, -1, CInt(row.Cells.Item(2).Value))

        ' Nur wenn Id in Datenbank vorhanden löschen
        If id <> -1 Then
            Dim query As String = "DELETE FROM feiertage WHERE ID = '" & id & "'"
            Using mysql As New mysql_verbindung.clsMySQL
                mysql.ExecuteWithParametersAsDatareader(query)
            End Using
        End If

    End Sub

    Private Sub SpeicherAenderungen()
        Dim rows As DataGridViewRowCollection = DGVFeiertage.Rows
        Dim feiertage As New List(Of Feiertag)
        Dim hasError As Boolean = False

        For Each row As DataGridViewRow In DGVFeiertage.Rows
            Dim bezeichnung As String = row.Cells.Item(0).Value?.ToString()
            Dim datum As String = row.Cells.Item(1).Value?.ToString()
            Dim id As Integer = If(row.Cells.Item(2).Value Is Nothing, -1, CInt(row.Cells.Item(2).Value))

            ' Leere Zeilen ignorieren
            If String.IsNullOrWhiteSpace(datum) Then
                Continue For
            End If

            ' Nur geänderte Zeilen speichern
            If Not editedRows.Contains(row.Index) Then
                Continue For
            End If

            ' Wenn in geänderte Zeile falsches Datum, speichern abbrechen
            If Not validationService.CheckDatum(datum) Then
                hasError = True
                Exit For
            End If

            If String.IsNullOrWhiteSpace(bezeichnung) Then
                bezeichnung = ""
            End If

            If bezeichnung.Length > 100 Then
                bezeichnung = bezeichnung.Substring(0, 100)
            End If

            datum = CDate(datum).ToString("yyyy-MM-dd")
            feiertage.Add(New Feiertag(bezeichnung, datum, id))
        Next

        ' Wenn falsches Datum eingeben wurde nicht speichern
        If Not hasError Then
            Dim query As String

            For Each feiertag In feiertage
                ' Wenn noch keine Id vorhanden, wurde die Zeile neu angelegt
                If feiertag.Id = -1 Then
                    query = "INSERT INTO Feiertage (Bezeichnung, Datum) VALUES ('" & feiertag.Bezeichnung & "', '" & feiertag.Datum & "')"
                Else
                    query = "UPDATE Feiertage SET Bezeichnung = '" & feiertag.Bezeichnung & "', Datum = '" & feiertag.Datum & "' WHERE ID = " & feiertag.Id
                End If

                Using mysql As New mysql_verbindung.clsMySQL
                    mysql.ExecuteWithParametersAsDatareader(query)
                End Using
            Next

            SpeicherCheckboxValues()
            MessageBox.Show("Änderungen erfolgreich gespeichert.", "UrlaubConfig", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
            Close()
        Else
            MessageBox.Show("Bitte gültiges Datum eingeben.", "UrlaubConfig", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
        End If

    End Sub

    Private Sub LadeFeiertage()
        Dim feiertage As New List(Of Feiertag)
        Dim query As String = "SELECT * FROM Feiertage"

        Using mysql As New mysql_verbindung.clsMySQL
            mysql.ExecuteWithParametersAsDatareader(query)

            Dim i As Integer = 0
            While mysql.DataReader.Read()
                Dim bezeichnung As String = mysql.DataReader("Bezeichnung").TrimEnd()
                Dim datum As String = CDate(mysql.DataReader("Datum")).ToString("dd.MM.yyyy")
                Dim id As Integer = mysql.DataReader("ID")

                DGVFeiertage.Rows.Insert(i, bezeichnung, datum, id)
                i += 1
            End While
        End Using

        ' Id Spalte unsichtbar, nur um festzustellen ob Update oder Insert
        DGVFeiertage.Columns(2).Visible = False
    End Sub

    '0: Tag wird nicht von Urlaubstagen abgezogen, 1: Tag wird von Urlaubstagen abgezogen (z.B. Wochenende)
    Private Sub LadeUrlaubWochentage()
        Dim query As String = "SELECT config FROM urlaubconfig"
        Dim config As String = "0000000" ' Alle Tage Arbeitstage

        Using mysql As New mysql_verbindung.clsMySQL
            mysql.ExecuteWithParametersAsDatareader(query)

            If mysql.DataReader.Read() Then
                config = mysql.DataReader("config")
            End If
        End Using

        SetCheckboxValues(config)
    End Sub

    Private Sub SetCheckboxValues(config As String)
        For i As Integer = 0 To 6
            If config.Substring(i, 1) = "0" Then
                ComboboxTage.SetItemCheckState(i, CheckState.Unchecked)
            Else
                ComboboxTage.SetItemCheckState(i, CheckState.Checked)
            End If
        Next
    End Sub

    Private Sub SpeicherCheckboxValues()
        Dim config As String = ""

        For i As Integer = 0 To 6
            If ComboboxTage.GetItemCheckState(i) = CheckState.Checked Then
                config &= "1"
            Else
                config &= "0"
            End If
        Next

        Dim query As String = "SELECT config FROM urlaubconfig"

        Using mysql As New mysql_verbindung.clsMySQL
            mysql.ExecuteWithParametersAsDatareader(query)

            If mysql.DataReader.Read() Then
                query = "UPDATE urlaubconfig SET config = '" & config & "'"
            Else
                query = "INSERT INTO urlaubconfig (config) VALUES ('" & config & "')"
            End If

            mysql.ExecuteWithParametersAsDatareader(query)
        End Using
    End Sub

End Class