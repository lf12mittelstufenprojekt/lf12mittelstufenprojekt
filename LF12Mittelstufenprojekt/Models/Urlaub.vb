﻿Public Class Urlaub

    Public Property ID As Integer
    Public Property MitarbeiterID As Integer
    Public Property UrlaubsBeginn As Date
    Public Property UrlaubsEnde As Date

    Public Property Tage As Integer

    Public Sub New(iD As Integer, mitarbeiterID As Integer, urlaubsBeginn As Date, urlaubsEnde As Date, tage As Integer)
        Me.ID = iD
        Me.MitarbeiterID = mitarbeiterID
        Me.UrlaubsBeginn = urlaubsBeginn
        Me.UrlaubsEnde = urlaubsEnde
        Me.Tage = tage
    End Sub
End Class
