﻿Public Class Feiertag
    Public Property Bezeichnung As String

    Public Property Datum As String

    Public Property Id As Integer

    Sub New(bezeichnung As String, datum As String, id As Integer)
        Me.Bezeichnung = bezeichnung
        Me.Datum = datum
        Me.Id = id
    End Sub
End Class
