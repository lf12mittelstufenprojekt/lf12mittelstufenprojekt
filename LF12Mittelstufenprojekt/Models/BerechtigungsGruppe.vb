﻿Public Class BerechtigungsGruppe
    Public Property ID As Integer
    Public Property Bezeichnung As String
    Public Property EditEmployee As String
    Public Property ShowAllEmployee As String
    Public Property EditQualifications As String
    Public Property ShowAllQualification As String
    Public Property ExportEmployeeData As String
    Public Property EditWorkspaces As String
    Public Property ShowAllWorkspaces As String
    Public Property EditFilter As String
    Public Property EditPermissiongroup As String
    Public Property EditHolidays As String
    Public Property EditDocuments As String
    Public Property CreateEmployee As String
    Public Property EditHolidayConfig As String
    Public Property ChangeAdminPW As String

    Shared Function ReadPermissions()
        Dim query As String = "Select * from  berechtigungsgruppen"
        Dim berechtigungsliste As New List(Of BerechtigungsGruppe)

        Using mysql As New mysql_verbindung.clsMySQL
            mysql.ExecuteWithParametersAsDatareader(query)

            While mysql.DataReader.Read()
                Dim berechtigungsgruppe As New BerechtigungsGruppe() With {
                    .ID = mysql.DataReader("ID"),
                    .Bezeichnung = mysql.DataReader("Bezeichnung"),
                    .EditEmployee = mysql.DataReader("MitarbeiterVerwalten"),
                    .EditFilter = mysql.DataReader("FilterVerwalten"),
                    .EditQualifications = mysql.DataReader("QualifikationenVerwalten"),
                    .EditWorkspaces = mysql.DataReader("ArbeitsbereicheVerwalten"),
                    .ExportEmployeeData = mysql.DataReader("MitarbeiterdatenExportieren"),
                    .ShowAllWorkspaces = mysql.DataReader("AlleArbeitsbereicheAnzeigen"),
                    .ShowAllEmployee = mysql.DataReader("AlleMitarbeiterAnzeigen"),
                    .ShowAllQualification = mysql.DataReader("AlleQualifikationenAnzeigen"),
                    .EditPermissiongroup = mysql.DataReader("BerechtigungenVerwalten"),
                    .EditHolidays = mysql.DataReader("UrlaubeVerwalten"),
                    .EditDocuments = mysql.DataReader("DokumenteVerwalten"),
                    .CreateEmployee = mysql.DataReader("MitarbeiterErstellen"),
                    .EditHolidayConfig = mysql.DataReader("UrlaubKonfigurieren"),
                    .ChangeAdminPW = mysql.DataReader("AdminPasswortAendern")
                }
                berechtigungsliste.Add(berechtigungsgruppe)
            End While
        End Using
        Return berechtigungsliste
    End Function
End Class
