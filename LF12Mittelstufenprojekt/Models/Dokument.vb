﻿Imports System.IO
Imports System.Text
Public Class Dokument

    Public Property ID As Integer
    Public Property Bezeichnung As String
    Public Property Dateninhalt As Byte()
    Public Property DateiEndung As String
    Public Property Datum As Date
    Public Shared Sub SpeichereDateiUnterMitarbeiter(DokumentName As String, MitarbeiterID As Integer, file As String)

        Dim DokuDBID As Integer = SaveFileToDB(DokumentName, file)
        Dim Query As String = "Insert into mitarbeiterdokumente (MitarbeiterID,DokumentID) Values (" & MitarbeiterID & "," & DokuDBID & ")"
        Using mysql As New mysql_verbindung.clsMySQL
            mysql.ExecuteInsertWithParameter(Query)
        End Using

    End Sub
    Public Shared Sub LöscheDateiUnterMitarbeiter(DokuID As Integer)
        Dim query As String = "Delete from dokumente Where ID = " & DokuID & ";Delete from mitarbeiterdokumente where DokumentID = " & DokuID
        Using mysql As New mysql_verbindung.clsMySQL
            mysql.ExecuteWithParametersWithoutDatareader(query)
        End Using
    End Sub

    Public Shared Function SaveFileToDB(DokumentName As String, ByRef DateiPfad As String) As Short

        Dim retID As Short
        Dim data As Byte()
        Dim DateiEndung As String = ""
        If File.Exists(DateiPfad) Then

            Dim fInfo As New FileInfo(DateiPfad)
            Dim numBytes As Long = fInfo.Length
            DateiEndung = IO.Path.GetExtension(DateiPfad)
            Using fStream As New FileStream(DateiPfad, FileMode.Open, FileAccess.Read)

                Dim br As New BinaryReader(fStream)

                data = br.ReadBytes(numBytes)

                fStream.Close()

            End Using

            Using oMySql As New mysql_verbindung.clsMySQL

                Dim query As String = "INSERT INTO Dokumente (Bezeichnung,DateiInhalt,DateiEndung,Datum) VALUES(@Bezeichnung, @DateiInhalt,@DateiEndung ,@Datum);"

                Dim parameter As New Dictionary(Of String, Object)

                parameter.Add("@Bezeichnung", DokumentName)
                parameter.Add("@DateiInhalt", data)
                parameter.Add("@Datum", Now())
                parameter.Add("@DateiEndung", DateiEndung)

                retID = oMySql.ExecuteInsertWithParameter(query, parameter)

            End Using
        End If

        Return retID
    End Function

    Public Shared Function GetFileFromDB(DBID As Integer, Dateipfad As String) As Boolean

        Dim query As String = "SELECT DateiInhalt,DateiEndung FROM dokumente WHERE ID = '" & DBID & "'"

        Using oMySql As New mysql_verbindung.clsMySQL


            oMySql.ExecuteWithParametersAsDatareader(query)

            While oMySql.DataReader.Read

                If Not IsDBNull(oMySql.DataReader("DateiInhalt")) Then
                    Try
                        Dim File As String = Dateipfad

                        Using fStream As New FileStream(File, FileMode.Create, FileAccess.Write)

                            Dim bw As New BinaryWriter(fStream)

                            bw.Write(oMySql.DataReader("DateiInhalt"))

                            fStream.Close()

                        End Using

                        If IO.File.Exists(File) Then
                            Return True
                        Else
                            Return False
                        End If
                    Catch ex As Exception
                        MsgBox("Fehler beim Öffnen der Datei." & vbCrLf & "Ggf. ist diese schon geöffnet", MsgBoxStyle.SystemModal)
                        Return False

                    End Try
                Else
                    Return False
                End If
            End While
        End Using
    End Function
    Public Shared Function DateiOeffnen(DBID As Integer) As Boolean
        Dim Dateipfad As String
        Try
            Dim d As Dokument = Dokument.GetByDBID(DBID)
            If Not Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\MVS") Then
                Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\MVS")
            End If
            Dateipfad = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\MVS\" & d.Bezeichnung & String.Format("{0:ddMMyyyyhhmmss}", Now) & d.DateiEndung

            If GetFileFromDB(DBID, Dateipfad) Then

                Process.Start(Dateipfad)

            End If
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

    Public Shared Function GetByDBID(DBID As Integer) As Dokument
        Dim D As New Dokument
        Dim Query As String = "Select * from dokumente Where ID = " & DBID
        Using mysql As New mysql_verbindung.clsMySQL
            mysql.ExecuteWithParametersAsDatareader(Query)
            While mysql.DataReader.Read()
                D.ID = mysql.DataReader("ID")
                D.Bezeichnung = mysql.DataReader("Bezeichnung")
                D.Dateninhalt = mysql.DataReader("DateiInhalt")
                D.DateiEndung = mysql.DataReader("DateiEndung")
                D.Datum = mysql.DataReader("Datum")
            End While
        End Using
        Return D
    End Function

    Public Shared Function GetDokumenteByMitarbeiterID(MitarbeiterID As Integer) As List(Of Dokument)
        Dim Dokumente As New List(Of Dokument)
        Dim D As New Dokument
        Dim Query As String = "Select * from dokumente Where ID IN(Select DokumentID from mitarbeiterdokumente where MitarbeiterID = " & MitarbeiterID & " )"
        Using mysql As New mysql_verbindung.clsMySQL
            mysql.ExecuteWithParametersAsDatareader(Query)
            While mysql.DataReader.Read()
                D.ID = mysql.DataReader("ID")
                D.Bezeichnung = mysql.DataReader("Bezeichnung")
                D.Dateninhalt = mysql.DataReader("DateiInhalt")
                D.Datum = mysql.DataReader("Datum")
                Dokumente.Add(D)
            End While
        End Using
        Return Dokumente
    End Function
End Class
