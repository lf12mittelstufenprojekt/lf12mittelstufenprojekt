﻿Public Class Mitarbeiter

    Public Property ID As Integer
    Public Property Vorname As String
    Public Property Nachname As String
    Public Property Ort As String
    Public Property Adresse As String
    Public Property PLZ As String
    Public Property Telefonnummer As String
    Public Property Geburtsdatum As Date
    Public Property SteuerID As String
    Public Property IBAN As Integer
    Public Property ArbeitsEinstieg As Date
    Public Property ArbeitsEnde As Date
    Public Property WindowsBenutzer As String
    Public Property BerechtigungsGruppeID As Integer
    Public Property ArbeitsbereichID As Integer
    Public Property BerechtigungsGruppe As String ' Bezeichnung BerechtigungsGruppe
    Public Property ArbeitsBereich As String ' Bezeichnung Arbeitsbereich
    Public Property Personalnummer As Integer

End Class
