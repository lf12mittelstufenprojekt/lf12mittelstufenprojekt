﻿Public Class AddPermissiongroup
    Public dgvCount As Integer
    Public tempArray() As String
    Public mode As Integer = 0
    Public mySelectedRow As Double
    Dim query As String
    Private Sub ButtonSave_Click(sender As Object, e As EventArgs) Handles ButtonSave.Click
        Dim myCheck As New Dictionary(Of String, Object)
        For i = 0 To dgvCount - 1
            If TextBoxPermissiongroupname.Text = tempArray(i) Then
                MessageBox.Show("Der Name der Berechtigungsgruppe existiert bereits.", "Berechtigung erstellen", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
        Next

        If mode = 0 Then
            If TextBoxPermissiongroupname.Text = "" Then
                MessageBox.Show("Bitte geben sie eine gültige Bezeichnung für die neue Berechtigungsgruppe ein.", "Berechtigungsgruppe hinzufügen", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                Using mysql As New mysql_verbindung.clsMySQL()
                    myCheck = CheckCheckboxen()
                    mysql.ExecuteInsertWithParameter(query, myCheck)
                End Using
                MessageBox.Show("Erfolgreich hinzugefügt.", "Berechtigungsgruppe hinzufügen", MessageBoxButtons.OK, MessageBoxIcon.Information)
                MainForm.editPerm.Result = DialogResult.OK
                MainForm.editPerm.mode = 0
                Close()
            End If
        ElseIf mode = 1 Then
            If TextBoxPermissiongroupname.Text = "" Then
                MessageBox.Show("Bitte geben sie eine neue gültige Bezeichnung für die Änderung der Berechtigungsgruppe ein.", "Berechtigungsgruppe ändern", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                Using mysql As New mysql_verbindung.clsMySQL()
                    myCheck = CheckCheckboxen()
                    mysql.ExecuteInsertWithParameter(query, myCheck)
                End Using
                MessageBox.Show("Erfolgreich geändert.", "Berechtigungsgruppe ändern", MessageBoxButtons.OK, MessageBoxIcon.Information)
                MainForm.editPerm.Result = DialogResult.OK
                MainForm.editPerm.mode = 0
                Close()
            End If
        End If
    End Sub

    Private Sub ButtonAbort_Click(sender As Object, e As EventArgs) Handles ButtonAbort.Click
        MainForm.editPerm.Result = DialogResult.Cancel
        Close()
    End Sub

    Private Function CheckCheckboxen()
        Dim parameter As New Dictionary(Of String, Object)
        Dim checkboxCounter As Integer = 0
        Dim Obj As Object
        Dim CkBEditEmp, CkBShowAllEmp, CkBEditQuali, CkBShowAllQuali, CkBExportEmp, CkBShowAllWrksps As String
        Dim CkBEditWrksps, CkBEditFlt, CkBEditPerms, CkBEditHD, CkBEditDocu, CkbCreateNewEmp, CkBEditHolidayConf, CkBChangeAdminPassword As String

        ' Speichert die Daten der Textbox in die Datenbank
        If mode = 0 Then
            query = "INSERT INTO berechtigungsgruppen ( Bezeichnung, MitarbeiterVerwalten, AlleMitarbeiterAnzeigen, QualifikationenVerwalten, AlleQualifikationenAnzeigen, MitarbeiterdatenExportieren, AlleArbeitsbereicheAnzeigen, ArbeitsbereicheVerwalten, FilterVerwalten, BerechtigungenVerwalten, UrlaubeVerwalten, DokumenteVerwalten, MitarbeiterErstellen, UrlaubKonfigurieren) VALUES ( @Bezeichnung, @MitarbeiterVerwalten, @AlleMitarbeiterAnzeigen, @QualifikationenVerwalten, @AlleQualifikationenAnzeigen, @MitarbeiterdatenExportieren, @AlleArbeitsbereicheAnzeigen, @ArbeitsbereicheVerwalten, @FilterVerwalten, @BerechtigungenVerwalten, @UrlaubeVerwalten, @DokumenteVerwalten, @MitarbeiterErstellen, @UrlaubKonfigurieren)"
        ElseIf mode = 1 Then
            query = "UPDATE berechtigungsgruppen SET Bezeichnung = @Bezeichnung, MitarbeiterVerwalten = @MitarbeiterVerwalten, AlleMitarbeiterAnzeigen = @AlleMitarbeiterAnzeigen, QualifikationenVerwalten = @QualifikationenVerwalten, AlleQualifikationenAnzeigen = @AlleQualifikationenAnzeigen, MitarbeiterdatenExportieren = @MitarbeiterdatenExportieren, AlleArbeitsbereicheAnzeigen = @AlleArbeitsbereicheAnzeigen, ArbeitsbereicheVerwalten = @ArbeitsbereicheVerwalten, FilterVerwalten = @FilterVerwalten, BerechtigungenVerwalten = @BerechtigungenVerwalten , UrlaubeVerwalten = @UrlaubeVerwalten, DokumenteVerwalten = @DokumenteVerwalten, MitarbeiterErstellen = @MitarbeiterErstellen, UrlaubKonfigurieren = @UrlaubKonfigurieren WHERE ID = @ID"

        End If

        ' Prüft ob Checkboxen für die Berechtigungen gechecked sind
        For Each Obj In GroupBoxPermissions.Controls
            If TypeOf Obj Is CheckBox Then
                Dim cBox As CheckBox = CType(Obj, CheckBox)
                checkboxCounter += 1
                Select Case cBox.Name

                    Case "CkBEditEmployee"
                        If cBox.CheckState = CheckState.Checked Then
                            CkBEditEmp = "Y"
                        Else
                            CkBEditEmp = "N"
                        End If

                    Case "CkBShowAllEmployee"
                        If cBox.CheckState = CheckState.Checked Then
                            CkBShowAllEmp = "Y"
                        Else
                            CkBShowAllEmp = "N"
                        End If

                    Case "CkBEditQualifikation"
                        If cBox.CheckState = CheckState.Checked Then
                            CkBEditQuali = "Y"
                        Else
                            CkBEditQuali = "N"
                        End If

                    Case "CkBShowAllQualification"
                        If cBox.CheckState = CheckState.Checked Then
                            CkBShowAllQuali = "Y"
                        Else
                            CkBShowAllQuali = "N"
                        End If

                    Case "CkBExportEmployeeData"
                        If cBox.CheckState = CheckState.Checked Then
                            CkBExportEmp = "Y"
                        Else
                            CkBExportEmp = "N"
                        End If

                    Case "CkBEditWorkspaces"
                        If cBox.CheckState = CheckState.Checked Then
                            CkBEditWrksps = "Y"
                        Else
                            CkBEditWrksps = "N"
                        End If

                    Case "CkBShowAllWorkspaces"
                        If cBox.CheckState = CheckState.Checked Then
                            CkBShowAllWrksps = "Y"
                        Else
                            CkBShowAllWrksps = "N"
                        End If

                    Case "CkBEditFilter"
                        If cBox.CheckState = CheckState.Checked Then
                            CkBEditFlt = "Y"
                        Else
                            CkBEditFlt = "N"
                        End If

                    Case "CkBEditPermissiongroups"
                        If cBox.CheckState = CheckState.Checked Then
                            CkBEditPerms = "Y"
                        Else
                            CkBEditPerms = "N"
                        End If

                    Case "CkBEditHolidays"
                        If cBox.CheckState = CheckState.Checked Then
                            CkBEditHD = "Y"
                        Else
                            CkBEditHD = "N"
                        End If

                    Case "CkBEditDocuments"
                        If cBox.CheckState = CheckState.Checked Then
                            CkBEditDocu = "Y"
                        Else
                            CkBEditDocu = "N"
                        End If

                    Case "CkBCreateNewEmployee"
                        If cBox.CheckState = CheckState.Checked Then
                            CkbCreateNewEmp = "Y"
                        Else
                            CkbCreateNewEmp = "N"
                        End If

                    Case "CkBEditHolidayConfig"
                        If cBox.CheckState = CheckState.Checked Then
                            CkBEditHolidayConf = "Y"
                        Else
                            CkBEditHolidayConf = "N"
                        End If

                    Case "CkBChangeAdminPW"
                        If cBox.CheckState = CheckState.Checked Then
                            CkBChangeAdminPassword = "Y"
                        Else
                            CkBChangeAdminPassword = "N"
                        End If
                End Select
            End If
        Next

        ' Bereitet die Parameter für das Insert oder Update für die DB vor
        parameter.Add("Bezeichnung", TextBoxPermissiongroupname.Text)
        parameter.Add("MitarbeiterVerwalten", CkBEditEmp)
        parameter.Add("AlleMitarbeiterAnzeigen", CkBShowAllEmp)
        parameter.Add("QualifikationenVerwalten", CkBEditQuali)
        parameter.Add("AlleQualifikationenAnzeigen", CkBShowAllQuali)
        parameter.Add("MitarbeiterdatenExportieren", CkBExportEmp)
        parameter.Add("AlleArbeitsbereicheAnzeigen", CkBShowAllWrksps)
        parameter.Add("ArbeitsbereicheVerwalten", CkBEditWrksps)
        parameter.Add("FilterVerwalten", CkBEditFlt)
        parameter.Add("BerechtigungenVerwalten", CkBEditPerms)
        parameter.Add("UrlaubeVerwalten", CkBEditHD)
        parameter.Add("DokumenteVerwalten", CkBEditDocu)
        parameter.Add("MitarbeiterErstellen", CkbCreateNewEmp)
        parameter.Add("UrlaubKonfigurieren", CkBEditHolidayConf)
        parameter.Add("AdminPasswortAendern", CkBChangeAdminPassword)

        ' Nimm die ID der ausgewählten Reihe oder die letzte Reihe +1 für das hinzufügen
        If mode = 1 Then
            parameter.Add("ID", mySelectedRow)
            'ElseIf mode = 0 Then
            '    parameter.Add("ID", dgvCount)
        End If
        Return parameter
    End Function

End Class