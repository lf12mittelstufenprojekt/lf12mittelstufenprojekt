﻿
Imports LF12Mittelstufenprojekt

<TestClass()>
Public Class MitarbeiterValidationTest

    Private validationService As New MitarbeiterValidationService()

    <DataTestMethod()>
    <DataRow("32.03.2022")>
    <DataRow("10.00.2022")>
    <DataRow("29.02.2022")>
    <DataRow("29.xx.2022")>
    <DataRow("31.12.1899")>
    <DataRow("01.01.2101")>
    <DataRow("01.01.20")>
    Public Sub IsDatumNotCorrect_ReturnFalse(datum As String)
        Dim result As Boolean = validationService.CheckDatum(datum)
        Assert.AreEqual(result, False)
    End Sub

    <DataTestMethod()>
    <DataRow("31.05.2022")>
    <DataRow("30.06.2022")>
    <DataRow("29.02.2020")>
    <DataRow("28.02.2022")>
    <DataRow("1.03.2022")>
    <DataRow("1.5.2022")>
    <DataRow("11.6.2022")>
    Public Sub IsDatumCorrect_ReturnTrue(datum As String)
        Dim result As Boolean = validationService.CheckDatum(datum)
        Assert.AreEqual(result, True)
    End Sub

    <DataTestMethod()>
    <DataRow("123456")>
    <DataRow("12x45")>
    <DataRow("00000")>
    <DataRow("-1234")>
    Public Sub IsPLZNotCorrect_ReturnFalse(plz As String)
        Dim result As Boolean = validationService.CheckPLZ(plz)
        Assert.AreEqual(result, False)
    End Sub

    <DataTestMethod()>
    <DataRow("12345")>
    <DataRow("99999")>
    Public Sub IsPLZCorrect_ReturnTrue(plz As String)
        Dim result As Boolean = validationService.CheckPLZ(plz)
        Assert.AreEqual(result, True)
    End Sub

End Class