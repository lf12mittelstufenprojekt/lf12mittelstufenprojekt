﻿Imports LF12Mittelstufenprojekt

<TestClass()>
Public Class UrlaubTest

    Private _urlaubService As New UrlaubService()

    <TestMethod()>
    <DataRow("07.06.2022", "04.06.2022")>
    <DataRow("10.06.2022", "09.06.2022")>
    Public Sub WhenAnfangDatumBeforeEndReturnMessage(beginn As String, ende As String)
        Dim beginnD As Date = CDate(beginn)
        Dim endeD As Date = CDate(ende)

        Dim resultMsg As String = _urlaubService.CheckUrlaub(New List(Of Urlaub), beginnD, endeD)
        Assert.AreEqual(resultMsg, UrlaubService.EndeDatumVorAnfangMsg)
    End Sub

    <TestMethod()>
    <DataRow("05.06.2022", "10.06.2022")>
    <DataRow("06.06.2022", "06.06.2022")>
    Public Sub WhenAnfangDatumBeforeEndReturnNoMessage(beginn As String, ende As String)
        Dim beginnD As Date = CDate(beginn)
        Dim endeD As Date = CDate(ende)

        Dim resultMsg As String = _urlaubService.CheckUrlaub(New List(Of Urlaub), beginnD, endeD)
        Assert.AreEqual(resultMsg, String.Empty)
    End Sub

    <TestMethod()>
    <DataRow("01.06.2022", "10.06.2022", "01.06.2022")>
    <DataRow("01.06.2022", "10.06.2022", "10.06.2022")>
    <DataRow("01.06.2022", "10.06.2022", "05.06.2022")>
    Public Sub WhenDatumInZeitraumReturnTrue(beginn As String, ende As String, pruefDatum As String)
        Dim beginnD As Date = CDate(beginn)
        Dim endeD As Date = CDate(ende)
        Dim pruefD As Date = CDate(pruefDatum)

        Dim result As Boolean = _urlaubService.IsDatumInZeitraum(beginnD, endeD, pruefD)
        Assert.AreEqual(result, True)
    End Sub

    <TestMethod()>
    <DataRow("01.06.2022", "10.06.2022", "31.05.2022")>
    <DataRow("01.06.2022", "10.06.2022", "11.06.2022")>
    Public Sub WhenDatumNotInZeitraumReturnFalse(beginn As String, ende As String, pruefDatum As String)
        Dim beginnD As Date = CDate(beginn)
        Dim endeD As Date = CDate(ende)
        Dim pruefD As Date = CDate(pruefDatum)

        Dim result As Boolean = _urlaubService.IsDatumInZeitraum(beginnD, endeD, pruefD)
        Assert.AreEqual(result, False)
    End Sub

    <TestMethod()>
    Public Sub CalcUrlaubstageEqualsExpectedResult()
        _urlaubService.SetKeineUrlaubstage(New List(Of Integer) From {DayOfWeek.Saturday, DayOfWeek.Sunday})
        _urlaubService.SetFeiertage(New List(Of Date) From {CDate("31.10.2022")})
        Dim result As Integer = _urlaubService.CalcUrlaubsTageFuerZeitraum(CDate("24.10.2022"), CDate("02.11.2022"))
        Assert.AreEqual(result, 7)

        _urlaubService.SetKeineUrlaubstage(New List(Of Integer) From {DayOfWeek.Monday})
        _urlaubService.SetFeiertage(New List(Of Date) From {CDate("24.12.2022"), CDate("25.12.2022"), CDate("26.12.2022"), CDate("31.12.2022")})
        result = _urlaubService.CalcUrlaubsTageFuerZeitraum(CDate("16.12.2022"), CDate("31.12.2022"))
        Assert.AreEqual(result, 11)

        _urlaubService.SetKeineUrlaubstage(New List(Of Integer) From {DayOfWeek.Saturday, DayOfWeek.Sunday})
        _urlaubService.SetFeiertage(New List(Of Date))
        result = _urlaubService.CalcUrlaubsTageFuerZeitraum(CDate("10.06.2022"), CDate("23.06.2022"))
        Assert.AreEqual(result, 10)
    End Sub

End Class